var App = function App() {
    var schoolsTableDiv;
    var schoolsTableBody;
    var tableBottomInfo;
    var summaryTableBody;
    var schoolsTableHead;
}

App.prototype.functions = new Functions();

App.prototype.init = function() {
    this.tableDiv = $('#tableDiv');
    this.schoolsTableBody = $('#schoolsTableBody');
    this.tableBottomInfo = $('#tableBottomInfo');
    this.summaryTableBody = $('#summaryTableBody');
    this.schoolsTableHead = $('#schoolsTableHead');

    const did = $('#did').text();

    // // show loaders on both the tables
    // $('#loader1').css('display', 'block');
    // $('#loader2').css('display', 'block');

    this.getProjectSchools("/reports/getProjectSchools?did=" + did);
    this.getProjectDetails("/reports/getProjectDetails?did=" + did);
}

App.prototype.getProjectSchools = function(url) {
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function(error, data) {
        if (error) {
            instance.tableDiv.html(error.error);
        } else {
            $('#loader2').css('display', 'none');
            instance.showSchoolDetailsInTable(data.detailsOfSchools);
        }
    })
}

/**
 * 
 * @param {Object} data
 */
App.prototype.showSchoolDetailsInTable = function(detailsOfSchools) {
	
    const instance = this;
    var length = 0;

    // create headings with values from data object
    const firstObj = detailsOfSchools[Object.keys(detailsOfSchools)[0]];
	var headingRow = $('<tr></tr>');
    for (x in firstObj) {
        var th = $('<th></th>').text(x);
        headingRow.append(th);
    }
    headingRow.append('<th>School Reports</th>')
    this.schoolsTableHead.append(headingRow);

    for (key in detailsOfSchools) {
        if (detailsOfSchools.hasOwnProperty(key)) {
            length++;
            const element = detailsOfSchools[key];
            var row = $('<tr></tr>').attr('id', key);

            for (x in element) {
                var td = this.functions.create_td(element[x]); 
                row.append(td);
            }

            var $tdButton = $(`<td class="col-md-2 col-lg-2">
                            <button id="${key}" onclick=requestSchoolReports(this) class="red-round form-group">
                                Click
                            </button>
                        </td>`);
            row.append($tdButton);

            this.schoolsTableBody.append(row);
        }
    }
    this.tableBottomInfo.html(this.tableBottomInfo.html() + (+length) +
        " out of " + (+length));
}

App.prototype.getProjectDetails = function(url) {
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function(error, data) {
        if (error) {
            console.log(error.error.message);
        } else {
            $('#loader1').css('display', 'none');

            for (key in data.projectDetails) {
                if (data.projectDetails.hasOwnProperty(key)) {
                    const value = data.projectDetails[key];

                    var row = $('<tr></tr>');
                    var tdKey = $('<td></td>');
                    tdKey.attr("align", "center");
                    tdKey.html(`<b>${key}</b>`);

                    var tdValue = instance.functions.create_td(value); //$('<td></td>');
                    tdValue.attr("align", "center");

                    row.append(tdKey)
                    row.append(tdValue);
                    instance.summaryTableBody.append(row);
                }
            }
        }
    });
}

new App().init();


const uid = $('#uid').text();
const userDisplayName = $('#userDisplayName').text();

// navigation
var currentURL = window.location.href;
var navTitles = (currentURL.split("#")[1]) ? ((currentURL.split("#")[1]).replace(/%20/g, " ")).split(',') : [];

this.requestSchoolReports = function (target) {
    var did = $(target).prop('id');
    var nextPageURL = `/reports/schoolPage?u=${uid}&n=${userDisplayName}&did=${did}#`;

    for (var index = 0; index < navTitles.length; index++) {
        nextPageURL += navTitles[index] + ",";
    }
    
    window.location = nextPageURL + ($('title').html().split("-")[1]).trim();
}