$(function() {
    const $email = $('#email');
    const $contactNum = $('#mobileNum');
    const $firstName = $('#fName');
    const $lastName = $('#lName');
    const $designation = $('#designation');
    const $createUserBtn = $('#login-submit');

    $createUserBtn.on('click', function(e) {
        e.preventDefault();

        if ($email.val() === undefined || !$email.val()) {
            alert("Enter email")
            $email.focus();
            return;
        }

        if (!emailRegex.test($email.val())) {
            alert('Invalid Email address!');
            $email.focus();
            return;
        }

        if ($firstName.val() === undefined || !$firstName.val()) {
            alert("Enter First Name")
            $firstName.focus();
            return;
        }

        if ($lastName.val() === undefined || !$lastName.val()) {
            alert("Enter Last Name")
            $lastName.focus();
            return;
        }

        if ($contactNum.val() === undefined ||
            !$contactNum.val() ||
            !contactValidationRegex.test($contactNum.val())) {

            alert("Enter Valid Contact Number");
            $contactNum.focus();
            return;
        }

        if ($($designation.find(':selected')[0]).prop('id') == "0") {
            alert("Select Valid Designation")
            $designation.focus();
            return;
        }

        var userDetails = {
            userEmail: $email.val(),
            firstName: $firstName.val(),
            lastName: $lastName.val(),
            mobileNum: $contactNum.val(),
            designation: $($designation.find(':selected')[0]).prop('id')
        };

        $createUserBtn.prop('disabled', true);

        var req = new XMLHttpRequest();
        req.open("POST", "createUser");
        req.setRequestHeader("Content-type", "application/json");
        req.send(JSON.stringify(userDetails));

        req.addEventListener("load", function(res) {
            $('#login-form').find('input:not([type=button])').val('');
            $($('#login-form').find('select > option')[0]).prop('selected', true);

            $createUserBtn.prop('disabled', false);
            alert(JSON.parse(req.responseText).message);
			location.reload();
        });
    });
});