$(function() {
    const emailInput = $('#email');
    const passwordInput = $('#password');
    const loginBtn = $('#login-submit');
    var loginHandler = new Handler();
    var functions = new Functions();

    // if ($('#loader').css("display") !== "none")
    $('#loader').css("display", "none");

    passwordInput.on('keydown', (e) => {
        if (e.keyCode === 13) {
            loginBtn.disabled = true;
            $('#loader').css("display", "block");
            checkLogin(emailInput.val(), passwordInput.val());
        }
    })

    loginBtn.on('click', function(e) {
        loginBtn.disabled = true;
        $('#loader').css("display", "block");
        checkLogin(emailInput.val(), passwordInput.val());
    });

    var checkLogin = function(email, password) {
            loginHandler.login(email, password, $("#backendInitialRoute").val()).then(user => {
                emailInput.val('');
                passwordInput.val('');
            }).catch(error => {
                $('#loader').css("display", "none");
                loginBtn.disabled = false;
                alert(error.message)
            })
    }
});