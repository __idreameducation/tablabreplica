$(function () {
  $('#sendMessageButton').on('click', (e) => {
    e.preventDefault();
    var $form = $(e.target).closest("form");
    sendMail($form);
  });

  $('#contactForm input,textArea').on('focus', (e) => {
    $(e.currentTarget).css('border-color', '');
  });

  function sendMail(form) {
    // get values from FORM
    var name = $(form).find($("input#name")).val();
    var email = $(form).find($("input#email")).val();
    var phone = $(form).find($("input#phone")).val();
    var message = $(form).find($("textarea#message")).val();
    var firstName = name; // For Success/Failure Message
    // Check for white space in name for Success/Fail message
    if (firstName.indexOf(' ') >= 0) {
      firstName = name.split(' ').slice(0, -1).join(' ');
    }
    
    var $emptyFields = $($(form).find("input,textArea")).filter(function() {
        return !this.value;
    });

    if($emptyFields.length > 0) {
      $emptyFields.css('border-color', 'red');
      return;
    }
    
    var mailOptions = {
      "to": [{
        "email": "p@idreameducation.org"
      }, {
        "email": "rp@idreameducation.org"
      }],
      "subject": `[My TabLab] Contact Enquiry Received from ${name}`,
      "html": `<strong style="display:block;margin-top:20px;">Full Name</strong>${name}
               <hr style="margin:10px 0;border-style:dashed;border-width:1px;border-color:#ccc" />
               <strong style="display:block;margin-top:20px;">Contact Number</strong>${phone}
               <hr style="margin:10px 0;border-style:dashed;border-width:1px;border-color:#ccc" />
               <strong style="display:block;margin-top:20px;">Email</strong>${email}
               <hr style="margin:10px 0;border-style:dashed;border-width:1px;border-color:#ccc" />
               <strong style="display:block;margin-top:20px;">Message</strong>${message}
                <hr style="margin:10px 0;border-style:dashed;border-width:1px;border-color:#ccc" />`
    }

    $this = $("#sendMessageButton");
    $this.prop("disabled", true); // Disable submit button until AJAX call is complete to prevent duplicate messages

    new Functions().sendRequest('reports/admin/mailRequest', 'POST', mailOptions, (response) => {
      $this.prop("disabled", false); // Re-enable submit button when call is complete

      var success = response.error ? false : true;
      var resultMessage = "";
      if (success) {
        // Success message
        resultMessage = "Your message has been sent.";

        // $(btn).html('<i class="fa fa-check"></i>');

        setTimeout(function(){
          // $(btn).html(btnText);
        },2000);

        $(form).slideUp();
        $('#successBox').slideDown();
        setTimeout(function(){
          $(form).slideDown();
          $('#successBox').slideUp();
        },4000);

        // $('#successBox').css("display", "");
        // setTimeout(function(){ $('#successBox').css("display", "none")}, 3000);
      } else {
        // Fail message
        resultMessage = "Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!";

        $(form).slideUp();
        $('#errorBox').slideDown();
        setTimeout(function(){
          $(form).slideDown();
          $('#errorBox').slideUp();
        },4000);


        // $('#errorBox').css("display", "");
        // setTimeout(function(){ $('#errorBox').css("display", "none")}, 3000);
      }

      //clear all fields
      $(form).trigger("reset");
    });
  }

//   $("a[data-toggle=\"tab\"]").click(function (e) {
//   e.preventDefault();
//   $(this).tab("show");
// });
});

/*When clicking on Full hide fail/success boxes */
$('#name').focus(function () {
$('#success').html('');
});