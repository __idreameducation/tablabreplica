$(function() {
    const $email = $('#email');
    const $contactNum = $('#mobileNum');
    const $firstName = $('#fName');
    const $lastName = $('#lName');
    const $createUserBtn = $('#login-submit');
    const schoolID = $('#did').val();
    var loginHandler = new Handler();
    var functions = new Functions();
    var teachers = {};

    functions.sendRequest(`getTeachers?did=${schoolID}`, 'GET', null, (error, data) => {
        teachers = data;
        for(var teacherID in data) {
            var $row = $(`<tr id="${teacherID}"></tr>`);
            var $name = $(`<td>${data[teacherID].name}</td>`);
            var $email = $(`<td>${data[teacherID].email}</td>`);
            var $contactNum = $(`<td>${data[teacherID].phoneNumber}</td>`);
            var $viewClasses = $(`<td>
                                    <button class="blue-round form-group" id="${teacherID}" onclick="showTeacherClasses(this)">
                                        Click
                                    </button>
                                </td>`);

            var $buttonDisable = $('<td><i class="fa fa-trash" aria-hidden="true"></i></td>')
                .on('click', (e) => {
                    e.preventDefault();

                    if (confirm("Remove teacher?\nNote: This will delete the user permanently.")) {
                        var teacherID = $($(e.currentTarget).parent()).prop('id');

                        // get classes of the teacher
                        var teacherClasses = teachers[teacherID].linkedClassrooms;
                        // get total number of classes, teacher is linked to
                        var callbackCounter = Object.keys(teacherClasses).length;

                        // remove the teacher from linkedTeachers of the class
                        for(var linkedClassroom in teacherClasses) {
                            loginHandler.setter(`Tablab/Classes/${schoolID}/${linkedClassroom}/linkedTeachers/${teacherID}`, {}, (error) => {
                                callbackCounter--;
                                if(callbackCounter === 0) { // teacher has been unlinked from all the classes
                                    // remove the teacher record from the records of the site users
                                    functions.sendRequest(`admin/deleteUser`, 'POST', {'uid': teacherID}, (response) => {
                                        if(response.error) {
                                            alert('Error while deleting the user! Cannot complete the process.');
                                            return;
                                        }

                                        // remove the teacher from the 'teachers' object
                                        delete teachers[teacherID];

                                        // remove the teacher from the table
                                        $($(e.currentTarget).parent()).remove();
                                    });            
                                }
                            });
                        }
                    }
                });

            // append td to the row
            $row.append($name);
            $row.append($email);
            $row.append($contactNum);
            $row.append($viewClasses);
            // $row.append($edit);
            $row.append($buttonDisable);
            
            // append row to the table body
            $('#teachersList').append($row);
        }
    });

    $createUserBtn.on('click', function(e) {
        e.preventDefault();
        if ($email.val() === undefined || !$email.val()) {
            alert("Enter email")
            $email.focus();
            return;
        }

        const emailPatt = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (!emailPatt.test($email.val())) {
            alert('Invalid Email address!');
            $email.focus();
            return;
        }

        if ($firstName.val() === undefined || !$firstName.val()) {
            alert("Enter First Name")
            $firstName.focus();
            return;
        }

        if ($lastName.val() === undefined || !$lastName.val()) {
            alert("Enter Last Name")
            $lastName.focus();
            return;
        }

        const contactValidationRegex = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;
        if ($contactNum.val() === undefined ||
            !$contactNum.val() ||
            !contactValidationRegex.test($contactNum.val())) {

            alert("Enter Valid Contact Number");
            $contactNum.focus();
            return;
        }

        const selectedClassSubjects = $('#classSubjectsTeacherLink').find('.className:checked');
        var class_subjects = {};

        for (var i = 0; i < selectedClassSubjects.length; i++) {
            var element = $(selectedClassSubjects[i]).parent().parent();
            const elementClassName = $(element).find('.className');
            const isClassTeacher = $(element).find('.isClassTeacher').is(':checked');
            const elementsSubjectName = $(element).find('.subjectName');
            // const elementsSubjectName = $(element).find('.subjectName:checked');

            var selectedSubjects = [];
            var classTeacherSubjects = [];
            $(elementsSubjectName).each(subjectElement => {
                var subjectName = $(elementsSubjectName[subjectElement]).val()
                if($(elementsSubjectName[subjectElement]).is(':checked')){
                    selectedSubjects.push(subjectName);                    
                }

                if(isClassTeacher) {
                    classTeacherSubjects.push(subjectName)
                }
            });

            if (jQuery.isEmptyObject(selectedSubjects)) {
                alert('Select Subjects under selected class!')
                return;
            }

            class_subjects[$(elementClassName).val()] = {
                'isClassTeacher': isClassTeacher,
                'subjects': selectedSubjects,
                'classTeacherSubjects': classTeacherSubjects
            }
        };

        if (jQuery.isEmptyObject(class_subjects)) {
            alert('Link teacher to at least one class!')
            return;
        }

        var userDetails = {
            userEmail: $email.val(),
            firstName: $firstName.val(),
            lastName: $lastName.val(),
            mobileNum: $contactNum.val(),
            'designation': 'Teacher',
            'designationId': schoolID
        };

        $('#loader').css('display', 'block');
        $createUserBtn.prop('disabled', true);

        var req = new XMLHttpRequest();
        req.open("POST", "/reports/admin/createUser");
        req.setRequestHeader("Content-type", "application/json");
        req.send(JSON.stringify(userDetails));

        req.addEventListener("load", function(res) {
            $('#login-form').find('input:not(:button)').val('');
            $('#login-form').find('input:checked').prop('checked', false);

            const responseData = JSON.parse(req.responseText);

            // add class_subject details under teacher account
            const userid = responseData.userRecord.uid;
            loginHandler.setter(`users/${userid}/linkedClassrooms`, class_subjects, () => {});

            for (var key in class_subjects) {
                if (!class_subjects.hasOwnProperty(key)) continue;

                // add teacher to the linked teachers of the class
                var linkedTeachers = {
                    'isClassTeacher': class_subjects[key].isClassTeacher,
                    'subjects': class_subjects[key].subjects,
                    'name': responseData.userRecord.displayName,
                    'email': responseData.userRecord.email
                }
                loginHandler.setter(`Tablab/Classes/${schoolID}/${key}/linkedTeachers/${userid}`, linkedTeachers, () => {});
            }

            $('#loader').css('display', 'none');

            $createUserBtn.prop('disabled', false);
            alert(responseData.message);
        });
    });

    // Is current teacher a Class teacher?
    var previousClassTeacherSelection;
    $('.isClassTeacher').change((e) => {
        if (previousClassTeacherSelection) {
            $($(previousClassTeacherSelection).parent().parent().find('.subjectName:checked')).prop('checked', false) //, false);
            $($(previousClassTeacherSelection).parent().parent().find('.subjectName')).prop('disabled', false);
        }

        previousClassTeacherSelection = e.target;

        // $($(e.target).parent().parent().find('.subjectName')).prop('disabled', true);
        // $($(e.target).parent().parent().find('.subjectName')).prop('checked', true);
    });

    $('.className').change((e) => {
        if ($(e.target).is(':checked')) {
            $($(e.target).parent().parent().find('input:not(.className)')).prop('disabled', false);
        } else {
            $($(e.target).parent().parent().find('.subjectName:checked')).prop('checked', false);
            $($(e.target).parent().parent().find('.isClassTeacher:checked')).prop('checked', false);
            $($(e.target).parent().parent().find('input:not(.className)')).prop('disabled', true);
        }
    });

    this.showTeacherClasses = function(targetElement) {
        $('#modalDiv').modal('show');

        // refresh modal area - remove old data, if any
        $('#teacherClasses').html('');

        // Get selected teacher's name
        var teacherName = $($($(targetElement).parent().parent()).find('td')[0]).text() ;
        $($('#myModalLabel').find('b')[0]).html(teacherName + "\'s ");

        var teacherID = $(targetElement).prop('id');
        $('#selectedTeacher').val(teacherID);
        var linkedClassrooms = teachers[teacherID].linkedClassrooms;

        for (var classID in linkedClassrooms) {
            var $Class = $(`<td class="col-md-2">${classID}</td>`);
            var $subjects = $(`<td class="col-md-2"></td>`);
            var allSubjects = linkedClassrooms[classID].subjects;
            for (var subjectIndex in allSubjects) {
                $subjects.append(`<input class="form-control" style="margin-bottom:3px;" value="${allSubjects[subjectIndex]}" disabled />`);
            }

            var $role = $(`<td class="col-md-2"></td>`);
            $role.text((linkedClassrooms[classID].isClassTeacher) ? "Class Teacher" : "Subject Teacher");

            var $edit = $(`<td style="backgroung-color:#000"><i class="edit fa fa-pencil" area-hidden="true"></i></td>`)
                .on('click', (e) => {
                    if (($(e.currentTarget).find('.edit')).length > 0) { // Edit
                        $(e.currentTarget).find('.edit').removeClass('edit fa-pencil').addClass('save fa-save');
                        $(e.currentTarget).parent().find('input, select').prop("disabled", false);
                    } else { // Save
                        $(e.currentTarget).find('.save').removeClass('save fa-save').addClass('edit fa-pencil');
                        $(e.currentTarget).parent().find('input, select').prop("disabled", true);

                        // update details in the database
                        updateClaasDetails($(e.currentTarget).parent());
                    }
                });

            var $buttonDisable = $('<td><i class="fa fa-trash" aria-hidden="true"></i></td>')
                .on('click', (e) => {
                    e.preventDefault();

                    if (confirm("Remove class from teacher's classrooms?")) {
                        var teacherID = $('#selectedTeacher').val();
                        var classID = $($(e.currentTarget).parent()).prop('id');
                        
                        loginHandler.setter(`Tablab/Classes/${schoolID}/${classID}/linkedTeachers/${teacherID}`, {}, (error) => {
                            loginHandler.setter(`users/${teacherID}/linkedClassrooms/${classID}`, {}, (error) => {});
                            $($(e.currentTarget).parent()).remove();
                        });
                    }
                });

            // create row
            $row = $(`<tr id="${classID}"></tr>`);

            // append all TD to a row
            $row.append($Class);
            $row.append($subjects);
            $row.append($role);
            $row.append($edit);
            $row.append($buttonDisable);

            // append row to table body
            $('#teacherClasses').append($row);
        }
    }

    function updateClaasDetails(targetElement) {
        var classID = $(targetElement).prop('id');
        var teacherID = $('#selectedTeacher').val();

        var updatedSubjects = [];
        var $subjectInputs = targetElement.find('td input');

        $subjectInputs.each(newSubject => {
            if($($subjectInputs[newSubject]).val()) {
                updatedSubjects.push($($subjectInputs[newSubject]).val());
            } else {
                $($subjectInputs[newSubject]).remove();
            }
        });

        loginHandler.setter(`Tablab/Classes/${schoolID}/${classID}/linkedTeachers/${teacherID}/subjects`, updatedSubjects, error => {
            loginHandler.setter(`users/${teacherID}/linkedClassrooms/${classID}/subjects`, updatedSubjects, error => {});
        });
    }
});