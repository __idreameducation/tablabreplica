/** Code documentation
 * Controls the flow of admin work
 */

var request = require("request");
const AdminAuth = require("models/admin");
const modelAdmin = new AdminAuth();

const ADMIN = "admin",
    iDREAM_COORDINATOR = "idreamcoordinator",
    PARTNER_COORDINATOR = "partnercoordinator";

const admin = {};
admin.constructor = function () {};

admin.home = function (req, res) {
    if (req.session.uid && req.session.designation === ADMIN) {
        res.render("admin/home.ejs", {
            'name': req.query.n,
            'uid': req.session.uid
        });
    } else {
        res.redirect('/reports');
    }
}

admin.createUser = function (req, res) {
    var userDetails = req.body;
    modelAdmin.createUser(userDetails, function (error, userRecord, userPassword) {
        if (error) {
            modelAdmin.deleteUser(userRecord.uid, (deletionError) => {
                if (deletionError) {
                    res.status(200).send(JSON.stringify({
                        'message': 'Error while creating user account and deleting the user.\nDelete newly created user manaully.',
                        'error': error,
                        'deletionError': deletionError
                    }));
                } else {
                    res.status(200).send(JSON.stringify({
                        'message': 'Error while creating user account.\nDeleted newly created user.',
                        'error': error
                    }));
                }
            })
            return;
        }

        var mailOptions = {
            to: [{ "email": userRecord.email, "name": userRecord.displayName}],
            subject: 'TabLab Registration ✔',
            text: 'Hello world ?',
        };

        mailOptions.html = "<B> Hi " +
            userRecord.displayName +
            "</b><br><br>You are registered to iDream community." +
            "</b>To activate your account click on <a href='http://www.mytablab.com/reports/setPassword?user=" + userRecord.uid + "' >Set Password</a>";

        // send email to the newly created user for accoutn activation
        sendMail(mailOptions, (error, info) => {
            if (error) {
                modelAdmin.deleteUser(userRecord.uid, (deletionError) => {
                    if (deletionError) {
                        res.status(200).send(JSON.stringify({
                            'message': 'Error while sending mail to the user and deleting the user.\nDelete newly created user manaully.',
                            'error': error,
                            'deletionError': deletionError
                        }));
                    } else {
                        res.status(200).send(JSON.stringify({
                            'message': 'Error while sending mail to the user.\nDeleted newly created user.',
                            'error': error
                        }));
                    }
                })
            } else {
                res.status(200).send(JSON.stringify({
                    'userRecord': userRecord,
                    'message': 'Successfully created new user.\nMail has been sent to the user for account activation.'
                }))
            }
        });
    })
}

function sendMail(mailOptions, done) {
    var options = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'api-key': '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'    
        },
        method: 'POST',
        url: 'https://api.sendinblue.com/v3/smtp/email',
        body: {
            tags: ['1'],
            sender: {
                name: 'iDream Education',
                email: 'p@idreameducation.org'
            },
            to: mailOptions.to,             //  [{ email: mailOptions.to }]
            htmlContent: mailOptions.html,
            subject: mailOptions.subject,
            replyTo: {
                email: 'p@idreameducation.org',
                name: 'Puneet'
            }
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) done(error);
        else done(null, body);
    });
}

admin.deleteUser = function (req, res) {
    res.send(JSON.stringify({}));
    modelAdmin.deleteUser(req.body.uid, (deletionError) => {
        if (deletionError) {
            res.status(500).send(JSON.stringify({
                'error': deletionError
            }));
        } else {
            res.status(200).send(JSON.stringify({}));
        }
    })
}

admin.mailForProjectAssignment = function (req, res) {
    var mailDetails = req.body;
    var email = mailDetails.email;
    var username = mailDetails.username;
    var projectName = mailDetails.projectName;

    var mailOptions = {
        from: 'idreameducation.tech@zoho.com',
        to: email,
        subject: `Assignment of project '${projectName}'`,
        text: `Dear ${username}`,
    };

    mailOptions.html = "<B> Hi " + username + "</B>";

    sendMail(mailOptions, (error, info) => {
        if (error) {
            res.status(200).send(JSON.stringify({
                'message': error
            }));
            return console.log(error);
        }

        res.status(200).send(JSON.stringify({
            'message': 'Mail Sent Successfully'
        }));
    });
}

admin.mailRequest = function (req, res) {
    var mailOptions = req.body;
    sendMail(mailOptions, (error, info) => {        
        if (error) {
            res.status(400).send(JSON.stringify({
                'error': error
            }));
        } else {
            res.status(200).send(JSON.stringify({
                'message': 'Mail Sent Successfully.\n' + info
            }));
        }
    });
}

admin.serveProjectCreationPage = function (req, res) {
    if (req.session.uid &&
        (req.session.designation === ADMIN ||
            req.session.designation === iDREAM_COORDINATOR ||
            req.session.designation === PARTNER_COORDINATOR)) {

        const uid = req.query.u;
        const userName = req.query.n;
        var responseText = {
            'uid': uid,
            'name': userName,
            'd': req.query.designation,
            'isCoordinator': req.query.isCoordinator
        }

        res.render('admin/ProjectCreationPage', responseText);
    } else {
        res.redirect('/reports');
    }
}

admin.getAssignedProjects = function (req, res) {
    const uid = req.query.u;
    modelAdmin.getter(`users/${uid}/projects`, (projectsSnapshot) => {
        if (projectsSnapshot && projectsSnapshot.val()) {
            res.status(200).send(JSON.stringify(projectsSnapshot.val()));
            return;
        }
        res.status(200).send(JSON.stringify({}));
    });
}

admin.getSchoolsUnderProject = function (req, res) {
    const pid = req.query.pid;
    modelAdmin.getter(`Tablab/Projects/${pid}/Schools`, (schoolsSnapshot) => {
        if (schoolsSnapshot && schoolsSnapshot.val()) {
            res.status(200).send(JSON.stringify(schoolsSnapshot.val()));
            return;
        }
        res.status(200).send(JSON.stringify({}));
    });
}

admin.getUsers = function (req, res) {
    // get users
    if (req.query.type) {
        modelAdmin.getterWithDesignation(`users`, req.query.type, (users) => {
            if (users && users.val())
                res.status(200).send(JSON.stringify(users.val()));
            else
                res.status(200).send(JSON.stringify({
                    "error": "Couldn't get user data"
                }));
        });
    } else {
        modelAdmin.getter(`users`, (users) => {
            if (users && users.val())
                res.status(200).send(JSON.stringify(users.val()));
            else
                res.status(200).send(JSON.stringify({
                    "error": "Couldn't get user data"
                }));
        });
    }
}

// get user details
admin.getUserDetails = function (req, res) {
    modelAdmin.getter(`users/${req.query.uid}`, (userDetails) => {        
        if (userDetails && userDetails.val())
            res.status(200).send(JSON.stringify(userDetails.val()));
        else
            res.status(200).send(JSON.stringify({
                "error": "Couldn't get user data"
            }));
    });
}

admin.serveAddSchoolDetailsPage = function (req, res) {
    if (req.session.uid &&
        (req.session.designation === ADMIN ||
            req.session.designation === iDREAM_COORDINATOR ||
            req.session.designation === PARTNER_COORDINATOR)) {

        var object = {
            'uid': req.query.u,
            'name': req.query.n,
            'pid': req.query.pid,
            'pName': req.query.pName,
        }
        res.status(200).render('admin/AddSchoolDetails', object);
    } else {
        res.redirect('/reports');
    }
}

admin.getProjectDetails = function (req, res) {
    const pid = req.query.pid;
    const path = `Tablab/Projects/${pid}/Project_Details`;
    modelAdmin.getter(path, (snap) => {
        if (!snap || !snap.val()) {
            res.status(404).send({
                'error': 'Error while fetching Project Details!'
            });
            return;
        }
        res.status(200).send(snap.val());
    });
}

admin.getSchoolDetails = function (req, res) {
    const schoolId = req.query.did;
    const path = `Tablab/Schools/${schoolId}`;
    modelAdmin.getter(path, (snap) => {
        if (!snap || !snap.val()) {
            res.status(404).send({
                'error': 'Error while fetching School Details!'
            });
            return;
        }
        res.status(200).send(snap.val());
    });
}

admin.getClassesDetails = function (req, res) {
    const schoolId = req.query.did;
    const path = `Tablab/Classes/${schoolId}`;
    modelAdmin.getter(path, (snap) => {
        if (!snap || !snap.val()) {
            res.status(404).send({
                'error': 'Error while fetching Classes Details!'
            });
            return;
        }
        res.status(200).send(snap.val());
    });
}

admin.editProjectDetailsPage = function (req, res) {
    var pid = req.query.pid;
    res.status(200).render('admin/EditProjectDetailsPage', {
        "uid": req.query.u,
        "name": req.query.n,
        "pid": pid
    });
}

admin.getAssignedCoordinators = function (req, res) {
    const projectId = req.query.pid;
    const typeOfCoordinator = req.query.typeOfCoordinator;
    const path = `Tablab/Projects/${projectId}/Project_Details/${typeOfCoordinator}`;
    modelAdmin.getter(path, (snap) => {
        if (!snap || !snap.val()) {
            res.status(404).send({
                'error': 'Error while fetching the data!'
            });
            return;
        }
        res.status(200).send(snap.val());
    });
}

admin.getSchoolAppIDs = function (req, res) {
    var schoolID = req.query.schoolID;
    modelAdmin.getter(`Tablab/Schools/${schoolID}/AppIDs`, (snap) => {
        if (!snap ) {
            res.status(404).send(JSON.stringify({
                "error": "Error while getting data"
            }));
            return;
        }

        if(!snap.val()) {
            res.status(200).send(JSON.stringify({
                "error": "No app ids!"
            }));
            return;
        }

        res.status(200).send(snap.val());
    });
}

admin.updateSchoolMedia = function (req, res) {
	const schoolId = req.query.did;
	var  schoolMediaContent = req.body;
	
	
	modelAdmin.setter(`/Tablab/Schools/${req.query.did}/media`, schoolMediaContent, (error) => {
        if (error) {
            res.status(404).send(JSON.stringify(error));
            return;
        }

        res.send(JSON.stringify({status:1}));
    });

	 
}

function extractObject(request, callback) {
    var body = "";
    var post = "";
    request.on('data', function (data) {
        body += data;
    });

    request.on('end', function () {
        post = JSON.parse(body);
        callback(post)
    });
}

module.exports = admin;