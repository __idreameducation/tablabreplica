const username = $('#username').val();
const uid = $('#uid').val();

var loginHandler = new Handler();
const functions = new Functions();

var iDreamCoordinators;
var partnerCoordinators;

$(document).ready(function () {
    // request list of the projects under current user 
    functions.sendRequest(`/reports/admin/getAssignedProjects?u=${uid}`, 'GET', null, (error, data) => {
        $('#loader1').css('display', 'none');
        var assignedProjects = data;
        for (var key in assignedProjects) {
            var projectDetails = assignedProjects[key];
            var options = {
                'id': key,
                'uid': uid,
                'username': username,
                // 'projectType': (projectDetails.projectType === 1) ? 'Online' : 'Offline',
                'name': projectDetails.projectName,
                'state': projectDetails.location.state
            }
            var $li = createListsElement(options);
            $('#projectsList').append($li);
        }
    })

    // request for idream coordinators data
    functions.sendRequest('/reports/admin/getUsers?type=idreamcoordinator', 'GET', null, (error, data) => {
        iDreamCoordinators = data;

        for (var key in iDreamCoordinators) {
            var $option1 = $(`<option id=${key}>${iDreamCoordinators[key].name} (${iDreamCoordinators[key].email})</option>`);
            $('#assignIdreamCoordinator').append($option1);

            var $option2 = $(`<option id=${key}>${iDreamCoordinators[key].name} (${iDreamCoordinators[key].email})</option>`);
            $('#edit-form #assignIdreamCoordinator').append($option2);
        }
    })

    // request for partner coordinators data
    functions.sendRequest('/reports/admin/getUsers?type=partnercoordinator', 'GET', null, (error, data) => {
        partnerCoordinators = data;
        for (var key in partnerCoordinators) {
            var $option1 = $(`<option id=${key}>${partnerCoordinators[key].name} (${partnerCoordinators[key].email})</option>`);
            $('#assignPartnerCoordinator').append($option1);

            var $option2 = $(`<option style ="margin:5px;font-size:16px;" id=${key}>${partnerCoordinators[key].name} (${partnerCoordinators[key].email})</option>`);
            $('#edit-form #assignPartnerCoordinator').append($option2);
            // var $option2 = $(`<option id=${key}>${partnerCoordinators[key].name} (${partnerCoordinators[key].email})</option>`);
            // $('#createOnlineProject #assignPartnerCoordinator').append($option2);
        }
    })

    $('[data-toggle="popover"]').popover();

    $('#ip1').click(function () {
        if ($(this).is(':checked')) {
            $('#implementationPartner').css("display", "block");
        }
    });

    $('#ip2').click(function () {
        if ($(this).is(':checked')) {
            $('#implementationPartner').css("display", "none");
        }
    });

    $('#fp1').click(function () {
        if ($(this).is(':checked')) {
            $('#fundingPartner').css("display", "block");
        }
    });

    $('#fp2').click(function () {
        if ($(this).is(':checked')) {
            $('#fundingPartner').css("display", "none");
        }
    });

    $('#callAllUsers').click((e) => {
        $('#usersList').html('');

        // request for the data of all the users
        functions.sendRequest('/reports/admin/getUsers', 'GET', null, (error, data) => {
            $('#loader').css('display', 'none');
            for (var key in data) {
                if (data[key].designation === 'admin') continue;

                const $row = $(`<tr>
                <td>${data[key].name}</td>
                <td>${data[key].email}</td>
                <td>${data[key].designation}</td>
                <td>${data[key].phoneNumber}</td>
                </tr>`);
                $('#usersList').append($row);
            }
        });
    });

    $('#submitProjectDetails').click((event) => {
        event.preventDefault();

        var projectDetails = {};
        var projectID = $('#inputProjectID').val();
        projectDetails.projectName = '';
        // projectDetails.projectType = 0;
        projectDetails.implementationPartnerName = '';
        projectDetails.fundingPartnerName = '';
        projectDetails.commencementDate = '';
        projectDetails.expiryDate = '';
        projectDetails.totalNumberOfSchools = '';
        projectDetails.totalNumberOfTablets = '';
        projectDetails.totalNumberOfTrolleys = '';
        projectDetails.fromGrade = '';
        projectDetails.toGrade = '';
        projectDetails.contentLanguage = '';
        projectDetails.location = {};

        /*  --- [START] Check Conditions ---*/
        // Check if Project name is empty
        if (!$('#inputProjectName').val() || $('#inputProjectName').val() === '') {
            alert('Project Name is necessary!');
            $('#inputProjectName').focus();
            return;
        }
        projectDetails.projectName = $('#inputProjectName').val();

        if ($('#ip1').is(':checked') && (!$('#inputImplementationPartner').val() || $('#inputImplementationPartner').val() === '')) {
            alert('Input Implementation Partner Details');
            $('#inputImplementationPartner').focus();
            return;
        }
        projectDetails.implementationPartnerName = $('#inputImplementationPartner').val();

        if ($('#fp1').is(':checked') && (!$('#inputProjectFunder').val() || $('#inputProjectFunder').val() === '')) {
            alert('Input Project Funder Details');
            $('#inputProjectFunder').focus();
            return;
        }
        projectDetails.fundingPartnerName = $('#inputProjectFunder').val();

        if (!$('#fromGrade').val()) {
            alert('Enter Class!');
            $('#fromGrade').focus();
            return;
        }
        projectDetails.fromGrade = $('#fromGrade').val();

        if (!$('#toGrade').val()) {
            alert('Enter Class!');
            $('#toGrade').focus();
            return;
        }
        projectDetails.toGrade = $('#toGrade').val();

        if (+($('#selectLanguage').find(':selected').attr('id')) <= 0) {
            alert('Select One Language For Content!');
            $('#selectLanguage').focus();
            return;
        }
        projectDetails.contentLanguage = $('#selectLanguage').find(':selected').html();

        if (!$('#pLocation').val()) {
            alert('Enter Project Location!');
            $('#pLocation').focus();
            return;
        }
        projectDetails.location.area = $('#pLocation').val();

        if (!$($('#pLocationState').find(':selected')[0]).prop('id')) {
            alert('Please Select One State!');
            $('#pLocationState').focus();
            return;
        }
        projectDetails.location.state = $($('#pLocationState').find(':selected')[0]).html();

        var iDreamCoordinatorId = $($('#assignIdreamCoordinator').find(':selected')[0]).prop('id');
        if (!iDreamCoordinatorId) {
            alert('Assign Project to a iDream coordinator!');
            $('#assignIdreamCoordinator').focus();
            return;
        }
        projectDetails.iDreamCoordinators = [];
        projectDetails.iDreamCoordinators.push({
            'id': iDreamCoordinatorId
        });

        /*  --- [END] Check Conditions ---*/

        // [START] partner coordinator
        var partnerCoordinatorId = $($('#assignPartnerCoordinator').find(':selected')[0]).prop('id');

        if (partnerCoordinatorId) {
            projectDetails.partnerCoordinators = [];
            projectDetails.partnerCoordinators.push({
                'id': partnerCoordinatorId
            });
        }
        // partner coordinator [END]

        projectDetails.totalNumberOfSchools = $('#tSchools').val() ? +$('#tSchools').val() : 0;
        projectDetails.totalNumberOfTablets = $('#tTablets').val() ? +$('#tTablets').val() : 0;
        projectDetails.totalNumberOfTrolleys = $('#tTrolleys').val() ? +$('#tTrolleys').val() : 0;
        projectDetails.commencementDate = $('#inputCommencementDate').val();
        projectDetails.expiryDate = $('#inputExpiryDate').val();

        loginHandler.saveDetails(`Tablab/counter/projects`, 'Tablab/Projects', 'project', {
            Project_Details: projectDetails
        }, (error, projectID) => {
            if (error) {
                alert('Error while saving data!');
                location.reload();
            }

            // save project under Admin's account
            loginHandler.setter(`users/${uid}/projects/${projectID}`, {
                'projectName': projectDetails.projectName,
                'location': projectDetails.location
            }, (error) => {});

            // save project under assigned iDreamCoordinator's account
            loginHandler.setter(`users/${iDreamCoordinatorId}/projects/${projectID}`, {
                'projectName': projectDetails.projectName,
                'location': projectDetails.location
            }, (error) => {});

            var mailOptions = {
                'from': '',
                'to': [iDreamCoordinators[iDreamCoordinatorId]],   //  .email,
                'subject': 'Project assignment and procurment details',
                'html': `Dear ${iDreamCoordinators[iDreamCoordinatorId].name}
                        <br><br>
                        You have been assigned to a new project. Please find the details below, 
                        <br><br>
                        <b>Project name:</b> ${projectDetails.projectName}
                        <br>
                        <b>Tablets required:</b> ${projectDetails.totalNumberOfTablets}
                        <br>
                        <b>Trolleys required:</b> ${projectDetails.totalNumberOfTrolleys}
                        <br>
                        <b>SD Cards required:</b> ${projectDetails.totalNumberOfTablets}
                        <br>
                        <b>Covers required:</b> ${projectDetails.totalNumberOfTablets}
                        <br>
                        <b>Screen gaurds required:</b> ${projectDetails.totalNumberOfTablets}
                        <br><br>
                        Other arrangements to do:
                        <br>
                        <b>Display Boards</b>
                        <br>
                        <b>Training Manuals</b>
                        <br><br><br>
                        Thanks.
                        <br>
                        Regards<br>
                        iDream Operations`
            }

            // Request browser to send mail to assigned coordinator
            functions.sendRequest('mailRequest', 'POST', mailOptions, (response) => {
                console.log(response);
            });
            // functions.sendRequest('/reports/admin/mailForProjectAssignment', 'POST', { 
            //     'email': iDreamCoordinators[iDreamCoordinatorId].email, 
            //     'username': iDreamCoordinators[iDreamCoordinatorId].name, 
            //     'projectName': projectDetails.projectName 
            // }, (response) => {
            //     console.log(response);
            // });

            // save project under assigned partnerCoordinator's account
            if (partnerCoordinatorId) {
                loginHandler.setter(`users/${partnerCoordinatorId}/projects/${projectID}`, {
                    'projectName': projectDetails.projectName,
                    'location': projectDetails.location
                }, (error) => {});

                mailOptions = {
                    'from': '',
                    'to': [partnerCoordinators[partnerCoordinatorId]], // .email,
                    'subject': 'Project assignment and procurment details',
                    'html': `Dear ${partnerCoordinators[partnerCoordinatorId].name}
                            <br><br>
                            You have been assigned to a new project. Please find the details below, 
                            <br><br>
                            <b>Project name:</b> ${projectDetails.projectName}
                            <br><br><br>
                            Thanks.
                            <br>
                            Regards<br>
                            iDream Operations`
                }

                // Request server to send mail to assigned coordinator
                functions.sendRequest('mailRequest', 'POST', mailOptions, (response) => {
                    console.log(response);
                });

                // functions.sendRequest('/reports/admin/mailForProjectAssignment', 'POST', { 'email': partnerCoordinators[partnerCoordinatorId].email, 'username': partnerCoordinators[partnerCoordinatorId].name, 'projectName': projectDetails.projectName }, (response) => {
                //     console.log(response);
                // });
            }

            // reset form data
            $('#form')[0].reset();

            // append newly created project to Admin's project list
            var options = {
                'id': projectID,
                'uid': uid,
                'username': username,
                // 'projectType': (projectDetails.projectType === 1) ? 'Online' : 'Offline',
                'name': projectDetails.projectName,
                'state': projectDetails.location.state
            }
            var $row = createListsElement(options);
            $('#projectsList').append($row);
        });

        alert('All Details saved. Click \"OK\"');
    });

    function createListsElement(options) {
        var $row = $(`<tr></tr>`);
        var $td1 = $(`<td class="col-lg-4 col-md-4">${options.name}</td>`);
        // var $td2 = $(`<td class="col-lg-2 col-md-2">${options.projectType}</td>`);
        var $td3 = $(`<td class="col-lg-2 col-md-2">${options.state}</td>`);
        var $td4 = $(`<td class="col-lg-2 col-md-2">
        <!--
            <a id="${options.id}" href="addSchoolDetailsPage?u=${options.uid}&n=${options.username}&pid=${options.id}&pName=${options.name}">
            </a>
         -->
         <button id="${options.id}_${options.name}" onclick=requestPojectSchools(this) class="blue-round form-group">
             <!--<i class="fa fa-eye"></i>-->
             Click
         </button>
                    </td>`);
        var $td5 = $(`<td class="col-md-2 col-lg-2">
                        <a href="#">
                            <button class="red-round form-group" onclick="showProjectDetails(this)" id="${options.id}">
                                Click
                            </button>
                        </a>
                    </td>`);

        var $td6 = $(`<td class="col-md-2 col-lg-2">
<!--
<a href="/reports/projectPage?u=${options.uid}&n=${options.username}&did=${options.id}"> 
</a>
-->
                        <button id="${options.id}" class="red-round form-group" onclick=requestProjectReports(this)>
                            <!--<i class="fa fa-eye"></i>-->
                            Click
                        </button>
                    </td>`);

        $row.append($td1);
        // $row.append($td2);
        $row.append($td3);
        $row.append($td4);
        $row.append($td5);
        $row.append($td6);
        return $row;
    }

    // navigation
    var currentURL = window.location.href;
    var navTitles = (currentURL.split("#")[1]) ? ((currentURL.split("#")[1]).replace(/%20/g, " ")).split(',') : [];

    this.requestProjectReports = function (target) {
        var projectID = $(target).prop('id');
        var nextPageURL = `/reports/projectPage?u=${uid}&n=${username}&did=${projectID}#`;
    
        for (var index = 0; index < navTitles.length; index++) {
            nextPageURL += navTitles[index] + ",";
        }
        
        window.location = nextPageURL + ($('title').html().split("-")[1]).trim();
    }
    
    this.requestPojectSchools = function (target) {
        var id = $(target).prop('id').split("_");
        var projectID = id[0];
        var pName = id[1];
        var nextPageURL = `addSchoolDetailsPage?u=${uid}&n=${username}&pid=${projectID}&pName=${pName}#`;
    
        console.log(nextPageURL);
        
        for (var index = 0; index < navTitles.length; index++) {
            nextPageURL += navTitles[index] + ",";
        }
        
        window.location = nextPageURL + ($('title').html().split("-")[1]).trim();
    }
    
    

    // set content language options
    languages.forEach((language, i) => {
        $('#selectLanguage').append($(`<option id="${i+1}">${language}</option>`))
    });

    // set states options
    states.forEach((state, i) => {
        $('#pLocationState').append($(`<option id="${i+1}">${state.name}</option>`))
    });
});

function showProjectDetails(event) {
    const projectID = $(event).prop('id');
    $('#editProjectDetailsModal').modal('show');
    editProjectDetails(projectID);
}