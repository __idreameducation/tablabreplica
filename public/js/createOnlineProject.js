$(document).ready(function() {
    $('#createOnlineProject #ip1').click(function() {
        if ($(this).is(':checked')) {
            $('#createOnlineProject #implementationPartner').css("display", "block");
        }
    });

    $('#createOnlineProject #ip2').click(function() {
        if ($(this).is(':checked')) {
            $('#createOnlineProject #implementationPartner').css("display", "none");
        }
    });

    $('#createOnlineProject #fp1').click(function() {
        if ($(this).is(':checked')) {
            $('#createOnlineProject #fundingPartner').css("display", "block");
        }
    });

    $('#createOnlineProject #fp2').click(function() {
        if ($(this).is(':checked')) {
            $('#createOnlineProject #fundingPartner').css("display", "none");
        }
    });

    $('#createOnlineProject #submitProjectDetails').click((event) => {
        event.preventDefault();

        var projectDetails = {};
        var projectID = $('#createOnlineProject #inputProjectID').val();
        projectDetails.projectName = '';
        projectDetails.projectType = 1;
        projectDetails.implementationPartnerName = '';
        projectDetails.fundingPartnerName = '';
        projectDetails.commencementDate = '';
        projectDetails.expiryDate = '';
        projectDetails.totalNumberOfSchools = '';
        projectDetails.totalNumberOfTablets = '';
        projectDetails.totalNumberOfTrolleys = '';
        projectDetails.fromGrade = '';
        projectDetails.toGrade = '';
        projectDetails.contentLanguage = '';
        projectDetails.location = {};

        /*  --- [START] Check Conditions ---*/
        // Check if Project name is empty
        if (!$('#createOnlineProject #inputProjectName').val() || $('#createOnlineProject #inputProjectName').val() === '') {
            alert('Project Name is necessary!');
            $('#createOnlineProject #inputProjectName').focus();
            return;
        }
        projectDetails.projectName = $('#createOnlineProject #inputProjectName').val();

        if ($('#createOnlineProject #ip1').is(':checked') && (!$('#createOnlineProject #inputImplementationPartner').val() || $('#createOnlineProject #inputImplementationPartner').val() === '')) {
            alert('Input Implementation Partner Details');
            $('#createOnlineProject #inputImplementationPartner').focus();
            return;
        }
        projectDetails.implementationPartnerName = $('#createOnlineProject #inputImplementationPartner').val();

        if ($('#createOnlineProject #fp1').is(':checked') && (!$('#createOnlineProject #inputProjectFunder').val() || $('#createOnlineProject #inputProjectFunder').val() === '')) {
            alert('Input Project Funder Details');
            $('#createOnlineProject #inputProjectFunder').focus();
            return;
        }
        projectDetails.fundingPartnerName = $('#createOnlineProject #inputProjectFunder').val();

        if (!$('#createOnlineProject #fromGrade').val()) {
            alert('Enter Class!');
            $('#createOnlineProject #fromGrade').focus();
            return;
        }
        projectDetails.fromGrade = $('#createOnlineProject #fromGrade').val();

        if (!$('#createOnlineProject #toGrade').val()) {
            alert('Enter Class!');
            $('#createOnlineProject #toGrade').focus();
            return;
        }
        projectDetails.toGrade = $('#createOnlineProject #toGrade').val();

        if (+($('#createOnlineProject #selectLanguage').find(':selected').attr('id')) <= 0) {
            alert('Select One Language For Content!');
            $('#createOnlineProject #selectLanguage').focus();
            return;
        }
        projectDetails.contentLanguage = $('#createOnlineProject #selectLanguage').find(':selected').html();

        if (!$('#createOnlineProject #pLocation').val()) {
            alert('Enter Project Location!');
            $('#createOnlineProject #pLocation').focus();
            return;
        }
        projectDetails.location.area = $('#createOnlineProject #pLocation').val();

        if (!$($('#createOnlineProject #pLocationState').find(':selected')[0]).prop('id')) {
            alert('Please Select One State!');
            $('#createOnlineProject #pLocationState').focus();
            return;
        }
        projectDetails.location.state = $($('#createOnlineProject #pLocationState').find(':selected')[0]).html();

        var iDreamCoordinatorId = $($('#createOnlineProject #assignIdreamCoordinator').find(':selected')[0]).prop('id');
        if (!iDreamCoordinatorId) {
            alert('Assign Project to a iDream coordinator!');
            $('#createOnlineProject #assignIdreamCoordinator').focus();
            return;
        }
        projectDetails.iDreamCoordinators = [];
        projectDetails.iDreamCoordinators.push({ 'id': iDreamCoordinatorId });

        /*  --- [END] Check Conditions ---*/

        // [START] partner coordinator
        var partnerCoordinatorId = $($('#createOnlineProject #assignPartnerCoordinator').find(':selected')[0]).prop('id');

        if (partnerCoordinatorId) {
            projectDetails.partnerCoordinators = [];
            projectDetails.partnerCoordinators.push({ 'id': partnerCoordinatorId });
        }
        // partner coordinator [END]

        projectDetails.totalNumberOfSchools = $('#createOnlineProject #tSchools').val();
        projectDetails.totalNumberOfTablets = $('#createOnlineProject #tTablets').val();
        projectDetails.totalNumberOfTrolleys = $('#createOnlineProject #tTrolleys').val();
        projectDetails.commencementDate = $('#createOnlineProject #inputCommencementDate').val();
        projectDetails.expiryDate = $('#createOnlineProject #inputExpiryDate').val();

        loginHandler.saveDetails(`Tablab/counter/projects`, 'Tablab/Projects', 'project', { Project_Details: projectDetails }, (error, projectID) => {
            if (error) {
                alert('Error while saving data!');
                location.reload();
            }

            // save project under Admin's account
            loginHandler.setter(`users/${uid}/projects/${projectID}`, { 'projectName': projectDetails.projectName, 'location': projectDetails.location, 'projectType': projectDetails.projectType }, (error) => {});

            // save project under assigned iDreamCoordinator's account
            loginHandler.setter(`users/${iDreamCoordinatorId}/projects/${projectID}`, { 'projectName': projectDetails.projectName, 'location': projectDetails.location, 'projectType': projectDetails.projectType }, (error) => {});

            // save project under assigned partnerCoordinator's account
            if (partnerCoordinatorId)
                loginHandler.setter(`users/${partnerCoordinatorId}/projects/${projectID}`, { 'projectName': projectDetails.projectName, 'location': projectDetails.location, 'projectType': projectDetails.projectType }, (error) => {});

            // reset form data
            $('#createOnlineProject #form')[0].reset();

            // append newly created project to Admin's project list
            var options = {
                'id': projectID,
                'uid': uid,
                'username': username,
                'projectType': (projectDetails.projectType === 1) ? 'Online' : 'Offline',
                'name': projectDetails.projectName,
                'state': projectDetails.location.state
            }
            var $row = createListsElement(options);
            $('#projectsList').append($row);

            // Request browser to send mail to assigned coordinator
            functions.sendRequest('/reports/admin/mailForProjectAssignment', 'POST', { 'email': iDreamCoordinators[iDreamCoordinatorId].email, 'username': iDreamCoordinators[iDreamCoordinatorId].name, 'projectName': projectDetails.projectName }, (response) => {
                console.log(response);
            });
        });

        alert('All Details saved. Click \"OK\"');
    })


    function createListsElement(options) {
        var $row = $(`<tr></tr>`);
        var $td1 = $(`<td class="col-lg-4 col-md-4">${options.name}</td>`);
        var $td2 = $(`<td class="col-lg-2 col-md-2">${options.state}</td>`);
        var $td3 = $(`<td class="col-lg-2 col-md-2">
                        <a id="${options.id}" href="addSchoolDetailsPage?u=${options.uid}&n=${options.username}&pid=${options.id}&pName=${options.name}">
                            <button class="blue-round form-group">
                                <!--<i class="fa fa-eye"></i>-->
                                Click
                            </button>
                        </a>
                    </td>`);
        var $td4 = $(`<td class="col-md-2 col-lg-2">
                        <button class="red-round form-group" id="${options.id}" onclick="showProjectDetails(this)" href="#">
                            <!--<i class="fa fa-eye"></i>-->
                            Click
                        </button>
                    </td>`);

        var $td5 = $(`<td class="col-md-2 col-lg-2">
                        <a href="/projectPage?u=${options.uid}&n=${options.username}&did=${options.id}">
                            <button class="red-round form-group">
                                <!--<i class="fa fa-eye"></i>-->
                                Click
                            </button>
                        </a>
                    </td>`);
        $row.append($td1);
        $row.append($td2);
        $row.append($td3);
        $row.append($td4);
        $row.append($td5);
        return $row;
    }

    // set content language options
    languages.forEach((language, i) => {
        $('#createOnlineProject #selectLanguage').append($(`<option id="${i+1}">${language}</option>`))
    });

    // set states options
    states.forEach((state, i) => {
        $('#createOnlineProject #pLocationState').append($(`<option id="${i+1}">${state.name}</option>`))
    });
});

function showProjectDetails(event) {
    const projectID = $(event).prop('id');
    console.log(projectID);

    var $expandedDiv = $(`<div>
        Expanded
    </div>`);
}