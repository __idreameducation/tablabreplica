var express = require("express");
var bodyParser = require("body-parser");
var iPrepController = require('../controller/iPrep');

var router = express.Router();

router.get('/', iPrepController.loginPage);
router.post("/login", bodyParser.json(), iPrepController.loginUser);

router.get('/logout', iPrepController.logoutUser);
router.get('/home', iPrepController.home);
router.get('/admin', iPrepController.adminHome);
router.get('/getUsers', iPrepController.getUsers);
router.get('/getFilterKeys', iPrepController.getFilterKeys);
router.get('/getSubjectwiseMastery', iPrepController.getSubjectwiseMastery);
router.get('/getSubjectwiseTopics', iPrepController.getSubjectwiseTopics);
router.get('/getFacilitatorDetails', iPrepController.getFacilitatorDetails);

// set password
router.get('/setPassword', iPrepController.setPassword);
router.post("/setPassword", iPrepController.activateUser);

router.post('/admin/createUser', iPrepController.createUser);
router.post('/getFacilitatorStudentsWithMastery', iPrepController.getFacilitatorStudentsWithMastery);

module.exports = router;