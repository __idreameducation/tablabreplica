/** Code Documentation
 * Handles all the client related server side work
 * 
 */

// const TYPE_ASSESSMENTS_USAGE = "AssessmentsUsageReports";
const TYPE_ASSESSMENTS_USAGE = "AssessmentUsageReports";
const TYPE_CONTENT_USAGE = "ContentUsageReports";
const analyticsReference = "Tablab/analytics";
const LIMIT_TO_LAST = 200;
const NO_LIMIT_TO_LAST = -1;
const KEY_DELIMITER = '*';

const PROJECT_LEVEL_USER = "projectmanager",
    SCHOOL_LEVEL_USER = "schoolmanager",
    TEACHER = "teacher",
    ADMIN = "admin",
    iDREAM_COORDINATOR = "idreamcoordinator",
    PARTNER_COORDINATOR = "partnercoordinator";


/* send mail */
var request = require("request");
const fs = require('fs');
const url = require('url');
var excel = require('excel4node');

const ModelsClient = require('../models/client');
const modelsClientObj = new ModelsClient();

var Client = {};

Client.tablabHomePage = function (req, res) {
    res.render("index");
}

Client.blogPage = function (req, res) {
    res.render("blog");
}

Client.faqPage = function (req, res) {
    res.render("faqPage");
}

Client.aboutPage = function (req, res) {
    res.render("about");
}

Client.postPage = function (req, res) {
    res.render("post");
}

Client.loginPage = function (req, res) {
    if (!req.session.uid) {
        res.render("login", {
            'name': '',
            'backendInitialRoute': 'reports'
        });
    } else {
        res.redirect(req.session.home);
    }
}

Client.serveMainPage = function (req, res) {
    if (req.session.uid) {
        res.redirect(req.session.home);
    } else {
        req.session.destroy();
        res.redirect('/reports');
    }
}

/**
 * Post Request from loginHandler.js
 * Returns response with url for next page to serve
 * 
 * @param {*} req 
 * @param {*} res 
 * @returns {*} res
 */
Client.loginUser = function (req, res) {
    const userInfo = req.body;
    var responseText = {};

    var designation = userInfo.designation.toLowerCase();
    const did = userInfo.did;
    const uid = userInfo.uid;
    const userName = (userInfo.name.trim()).split(" ")[0];
    var queryString = "";
    console.log(uid)
    var isCoordinator = 0;
    if (designation === iDREAM_COORDINATOR || designation === PARTNER_COORDINATOR) {
        isCoordinator = 1
    }

    queryString = "?u=" + uid + "&n=" + userName + "&d=" + designation + "&did=" + did + "&isCoordinator=" + isCoordinator;
     console.log(queryString)
    const error = null;
    if (error) {
        responseText.error = error;
        responseText.url = '/reports';
    } else {
        var flag = true;

        // if (userInfo.type === ADMIN) {
        //     // goto Admin Home Page
        //     responseText.url = "/admin" + queryString;
        //     designation = ADMIN;
        // } else 
        if (designation === PROJECT_LEVEL_USER) {
            // goto project page
            responseText.url = "/reports/projectPage" + queryString;
        } else if (designation === SCHOOL_LEVEL_USER) {
            // goto school page
            responseText.url = "/reports/schoolPage" + queryString;
        } else if (designation === TEACHER) {
            // goto teacher page
            responseText.url = "/reports/teacherPage" + queryString;
        } else if (designation === iDREAM_COORDINATOR ||
            designation === PARTNER_COORDINATOR ||
            designation === ADMIN) {
            responseText.url = "/reports/admin/projectCreationPage" + queryString;
        } else {
            responseText.url = '/reports';
            flag = false;
        }

        if (flag) {
            req.session.uid = uid;
            req.session.designation = designation;
            req.session.home = responseText.url;
        }
    }

    res.write(JSON.stringify(responseText));
    res.status(200).send();
}

Client.logout = function (req, res) {
    modelsClientObj.setter(`session/${req.session.uid}/download`, {}, (error) => {
        if (error) {
            // internal server error page
            // res.render('500', { "message": "Error in logout. Go back", "url": `` });
            res.status(500).send("Error while logging out!");
        } else {
            req.session.destroy();
            res.redirect('/reports');
        }
    });
}

Client.serveProjectPage = function (req, res) {
    const uid = req.query.u;
    if (req.session.uid &&
        (req.session.designation === PROJECT_LEVEL_USER ||
            req.session.designation === ADMIN ||
            req.session.designation === iDREAM_COORDINATOR ||
            req.session.designation === PARTNER_COORDINATOR)) {
        const userName = req.query.n;
        //    const designation = req.query.d;
        const did = req.query.did;
        // call function that returns data, based on did 
        // ...

        var obj = {
            'uid': uid,
            'name': userName,
            'did': did
        }
        res.render("client/projectPage.ejs", obj);
    } else {
        res.redirect('/reports');
    }
}

Client.serveSchoolPage = function (req, res) {
    if (req.session.uid &&
        (req.session.designation === SCHOOL_LEVEL_USER ||
            req.session.designation === PROJECT_LEVEL_USER ||
            req.session.designation === ADMIN ||
            req.session.designation === iDREAM_COORDINATOR ||
            req.session.designation === PARTNER_COORDINATOR)) {
        const uid = req.query.u;
        const userName = req.query.n;
        // did is the school ID
        const did = req.query.did;

        modelsClientObj.getter(`Tablab/Schools/${did}/appData/settings/tablabType/value`, (snap) => {
            var tablabType = (snap && snap.val()) ? snap.val() : 0;
            res.render("client/schoolPage.ejs", {
                'uid': uid,
                'name': userName,
                'did': did,
                'tablabType': tablabType,
				'userType':req.session.designation
            });
        });
    } else {
        res.redirect('/reports');
    }
}

Client.serveTeacherPage = function (req, res) {
    // if (req.session.uid && (req.session.designation === TEACHER)) {
    const uid = req.query.u;
    const userName = req.query.n;

    // did is the school ID
    const did = req.query.did;
    res.render("client/teacherPage", {
        'uid': uid,
        'name': userName,
        'did': did,
    });

    // } else {
    //     res.redirect('/reports');
    // }
}

Client.getLinkedClasses = function (req, res) {
    // user id
    const uid = req.query.uid;

    // school id
    const did = req.query.did;

    // Request for classes linked with the user of userid -> uid
    modelsClientObj.getter(`users/${uid}/linkedClassrooms`, (snap) => {
        if (snap && snap.val()) {
            var linkedClassrooms = (snap && snap.val()) ? snap.val() : {};
            var countCallbacks = Object.keys(linkedClassrooms).length;
            for (var className in linkedClassrooms) {
                modelsClientObj.getter(`Tablab/Classes/${did}/${className}/Details`, (classDetailsSnap) => {
                    if (classDetailsSnap && classDetailsSnap.val()) {
                        var totalStudents = classDetailsSnap.val().totalStudents;
                        var refClassName = classDetailsSnap.ref.toString().split('/')[6];
                        linkedClassrooms[refClassName].totalStudents = totalStudents;
                    }
                    countCallbacks--;
                    if (countCallbacks == 0) {
                        res.status(200).send(JSON.stringify(linkedClassrooms));
                    }
                });
            }
        } else {
            res.status(500).send(JSON.stringify({
                "error": "No data found!"
            }));
        }
    });
}

Client.getTotalUsageByClass = function (req, res) {
    // school id
    const did = req.query.did;
    var queryClassName = req.query.className;
    var querySubjectName = req.query.subjectName;

    // Request for classes linked with the user of userid -> uid
    modelsClientObj.getter(`Tablab/analytics/${did}/${TYPE_CONTENT_USAGE}/appUsers`, (snap) => {
        if (!snap) {
            res.status(500).send(JSON.stringify({
                "error": "Couldn't fetch data!"
            }));
            return;
        }

        if (!snap.val()) {
            res.status(200).send(JSON.stringify({
                "error": "No data found!"
            }));
            return;
        }

        var totalUsage = 0;
        snap.forEach((dateNode) => {
            dateNode.forEach((report) => {
                var usage = report.val();
                const className = usage.className;
                const subjectName = usage.subjectOfContent;

                if (querySubjectName) {
                    if (queryClassName === className && subjectName === querySubjectName) {
                        totalUsage += +usage.durationOfUse;
                    }
                } else {
                    if (queryClassName === className) {
                        totalUsage += +usage.durationOfUse;
                    }
                }
            })
        });
        res.status(200).send(JSON.stringify({
            "className": queryClassName,
            "subjectName": querySubjectName,
            "totalUsage": convertSecondsIntoHours(totalUsage)
        }));
    });
}

Client.serveReportsPage = function (req, res) {
    if (req.session.uid) {
        const uid = req.query.u;
        //    const designation = req.query.d;
        const did = req.query.did;
        const userName = req.query.n;
        const reportType = req.query.reportType

        res.render("client/graphicalReports.ejs", {
            uid: uid,
            did: did,
            reportType: reportType,
            name: userName
        });
    } else {
        res.redirect('/reports');
    }
}

Client.serveTabularReportsPage = function (req, res) {
    if (req.session.uid) {
        const uid = req.query.u;
        const did = req.query.did;
        const userName = req.query.n;

        modelsClientObj.getter(`Tablab/Schools/${did}/Classes`, snap => {
            var grades = {};
            if (snap && snap.val()) {
                grades = snap.val();
            } else {
                console.log('No classes')
            }

            res.render("client/tabularReports.ejs", {
                'uid': uid,
                'did': did,
                'name': userName,
                'grades': grades
            });
        });
    } else {
        res.redirect('/reports');
    }
}

Client.serveAddStudentsPage = function (req, res) {
    if (req.session.uid) {
        const uid = req.query.u;
        const did = req.query.did;
        const userName = req.query.n;
        var responseText = {
            'uid': uid,
            'name': userName,
            'did': did,
            'classLimits': {},
            'error': {}
        };

        modelsClientObj.getter(`Tablab/Schools/${did}/Classes`, (snap) => {
            if (snap && snap.val()) {
                var keys = Object.keys(snap.val());
                responseText.classes = keys
            } else {
                responseText.error = {
                    message: "No classes in records."
                }
            }

            res.render("client/addStudents", responseText);
        })
    } else {
        res.redirect('/reports');
    }
}

Client.serveAddTeachersPage = function (req, res) {
    if (req.session.uid) {
        const uid = req.query.u;
        const did = req.query.did;
        const userName = req.query.n;
        var responseText = {
            'uid': uid,
            'name': userName,
            'did': did,
            'classLimits': {},
            'error': {}
        };

        modelsClientObj.getter(`Tablab/Schools/${did}/Classes`, (snap) => {
            if (snap && snap.val()) {
                var keys = Object.keys(snap.val());
                responseText.class_subjects = {
                    "classes": keys
                }

                modelsClientObj.getter(`Tablab/Schools/${did}/appData/settings/subjects`, (subjects) => {
                    if (subjects && subjects.val()) {
                        var subjectKeys = Object.keys(subjects.val());
                        responseText.class_subjects.subjects = subjectKeys;
                    } else {
                        responseText.class_subjects = '';
                        responseText.error = {
                            message: "No subjects in records."
                        }
                    }
                    res.render("client/addTeachers", responseText);
                });
            } else {
                responseText.error = {
                    message: "No classes in records."
                }
                res.render("client/addTeachers", responseText);
            }
        });
    } else {
        res.redirect('/reports');
    }
}

Client.serveAddClassroomsPage = function (req, res) {
    if (req.session.uid) {
        const uid = req.query.u;
        const did = req.query.did;
        const userName = req.query.n;
        var responseText = {
            'uid': uid,
            'name': userName,
            'did': did,
            'error': {}
        };

        // modelsClientObj.getter(`Tablab/Schools/${did}/Classes`, (snap) => {
        //     if (snap && snap.val()) {
        //         var keys = Object.keys(snap.val());
        //         responseText.classes = keys
        //     } else {
        //         responseText.error = {
        //             message: "No classes in records."
        //         }
        //     }
        res.render("client/addClassrooms", responseText);
        // })
    } else {
        res.redirect('/reports');
    }
}

Client.serveAssessmentsAnalyticsPage = function (req, res) {
    if (req.session.uid) {
        const uid = req.query.u;
        const did = req.query.did;
        const userName = req.query.n;

        var responseText = {
            'uid': uid,
            'name': userName,
            'did': did
        };
        res.render("client/assessmentsAnalytics", responseText);
    } else {
        res.redirect('/reports');
    }
}

Client.serveStudentWiseAssessmentsDetailsPage = function (req, res) {
    if (req.session.uid) {
        const uid = req.query.u;
        const did = req.query.did;
        const userName = req.query.n;

        var responseText = {
            'uid': uid,
            'name': userName,
            'did': did,
            'studentName': req.query.studentName,
            'error': {}
        };
        res.render("client/assessmentPerUser", responseText);
    } else {
        res.redirect('/reports');
    }
}

Client.serveMasterReportsDetailsPage = function(req,res){
	if (req.session.uid) {
        const uid = req.query.u;
        const did = req.query.did;
        const userName = req.query.n;
		const startDate = req.query.start;
		const endDate = req.query.end;
        var responseText = {
            'uid': uid,
            'name': userName,
            'did': did
        };
        res.render("client/masteruserReports", responseText);
    } else {
        res.redirect('/reports');
    }
}

Client.addStudents = function (req, res) {
    const data = req.body;
    const schoolID = data.schoolID;
    const studentID = schoolID + '_' + data.studentInfo.studentClass + '_' + data.studentInfo.username;

    modelsClientObj.setter(`Tablab/Schools/${schoolID}/appUsers/userDetails/${studentID}`, data.studentInfo, (error) => {
        if (error) {
            res.status(404).send(JSON.stringify(error));
            return;
        }

        res.send(JSON.stringify({}));
    });
}

Client.getStudentRecords = function (req, res) {
    const schoolId = req.query.did;
    const pathToStudentsInfo = `Tablab/Schools/${schoolId}/appUsers/userDetails`;

    modelsClientObj.getter(pathToStudentsInfo, (snap) => {
        var responseText = {};
        var status;
        if (snap && snap.val()) {
            status = 200;
            responseText.studentRecords = snap.val();
        } else {
            status = 404;
            responseText.error = {
                'message': 'Error in getting student records!!'
            }
        }

        res.status(status).send(JSON.stringify(responseText));
    })
}

Client.fileUpload = function (req, res) {
    //    const form
}

Client.getProjectDetails = function (req, res) {
    const projectId = req.query.did;
    const pathToProject = `Tablab/Projects/${projectId}`;

    modelsClientObj.getter(pathToProject, (snap) => {
        var responseText = {};
        var status = 0;
        if (!snap || !snap.val()) {
            responseText.error = error;
            status = 404;
        } else {
            const projectDetails = snap.child("Project_Details");
            if (projectDetails && projectDetails.val()) {
                const pd = projectDetails.val();
                responseText.projectDetails = {
                    'Project Name': pd.projectName,
                    'Total Schools Under This Project': pd.totalNumberOfSchools,
                    'Project Funder': pd.fundingPartnerName,
                    'Implementation Partner': pd.implementationPartnerName,
                    'Date Of Commencement': pd.commencementDate,
					'Expiry Date': pd.expiryDate,
                    'Location': pd.location.area + ", " + pd.location.state,
                    'Content Language': pd.contentLanguage
                }
                status = 200;
            }
        }
        res.status(status).send(JSON.stringify(responseText));
    });
}

Client.getProjectSchools = function (req, res) {
    const projectId = req.query.did;
    const pathToProject = `Tablab/Projects/${projectId}`;

    modelsClientObj.getter(pathToProject, (snap) => {
        var responseText = {};
        var status = 0;
        if (!snap || !snap.val()) {
            responseText.error = 'No Projects at the current path!';
            status = 404;
            res.status(status).send(JSON.stringify(responseText));
        } else {
            const schoolIDs = snap.child("Schools").val();
            if (!schoolIDs) {
                responseText.error = 'No schools available!';
                res.status(404).send(JSON.stringify(responseText));
                return;
            }

            var detailsOfSchools = {};

            // for async database queries
			var countCallbacks = 0;
			
            const totalSchoolIds = Object.keys(schoolIDs).length;
            var keyArray = [];

			for (key in schoolIDs) 
			{
                keyArray.push(key);
				const pathToSchoolDetails = `Tablab/Schools/${key}/School_Details`;
				const pathToSchoolUsageDetails = `Tablab/analytics/${key}/computed_data`;
				
                modelsClientObj.getter(pathToSchoolDetails, (schoolDetails) => {
					modelsClientObj.getter(pathToSchoolUsageDetails,(school_usage_details)=>{
						let school_usage_details_val = school_usage_details.val()
						if (schoolDetails && schoolDetails.val()) {
							const sd = schoolDetails.val();
							if(school_usage_details_val)
							{
								detailsOfSchools[keyArray[countCallbacks]] = {
									'School Name': sd.schoolName,
									'Location': sd.location.area + ", " + sd.location.state,
									'Classes Covered': sd['Classes Covered'],
									'Number Of Tabs Installed': sd.totalNumberOfTablets,
									'Total Hours Usage' : Math.round(school_usage_details_val.usage_hours/36)/100,
									'Unique Days Usage' : school_usage_details_val.unique_days,
								} 
							}
							else
							{
								detailsOfSchools[keyArray[countCallbacks]] = {
									'School Name': sd.schoolName,
									'Location': sd.location.area + ", " + sd.location.state,
									'Classes Covered': sd['Classes Covered'],
									'Number Of Tabs Installed': sd.totalNumberOfTablets,
									'Total Hours Usage' : 0,
									'Unique Days Usage' : 0,
								} 
							}
							
						} else {
							console.log('error >>> Error Occured');
						}
						countCallbacks++;
						
						if (countCallbacks === totalSchoolIds) {
							status = 200;
							responseText.detailsOfSchools = detailsOfSchools;
							res.status(status).send(JSON.stringify(responseText));
						}
					})
                    
                })
            }
        }
    });
}

Client.getMonthlyUsageData = function ( req,res ) {
	const schoolId = req.query.did;
	pathToMonthlyUsageDetails = `Tablab/analytics/${schoolId}/ContentUsageReports/appUsers`;
	modelsClientObj.getter(pathToMonthlyUsageDetails,(monthly_usage_detials)=>{
		var responseText = {};
		var status = 0;
		if (!monthly_usage_detials || !monthly_usage_detials.val()) {
            status = 404;
            responseText.error = 'Error while retrieving school details!';
        } else {
			
			let monthly_usage_detials_val = monthly_usage_detials.val()
			let monthly_usage_detials_obj = {};
			for(let key in monthly_usage_detials_val)
			{
				let month_n = new Date(key).toLocaleString('en-us', { month: 'long' });
				let month_year = month_n + new Date(key).getFullYear()
				if(monthly_usage_detials_obj[month_year])
				{
					monthly_usage_detials_obj[month_year].unique_days_usage += 1; 
					for(let inner_obj_key in monthly_usage_detials_val[key])
					{
						monthly_usage_detials_obj[month_year].hours_usege += monthly_usage_detials_val[key][inner_obj_key].durationOfUse;
					}
				}
				else
				{
					monthly_usage_detials_obj[month_year] = {
						hours_usege  : 0,
						unique_days_usage : 0
					};
					monthly_usage_detials_obj[month_year].unique_days_usage += 1; 
					for(let inner_obj_key in monthly_usage_detials_val[key])
					{
						monthly_usage_detials_obj[month_year].hours_usege += monthly_usage_detials_val[key][inner_obj_key].durationOfUse;
					}
				}
			}
			let monthly_usage_detials_table_object = {};
				
			for(let key in monthly_usage_detials_obj)
			{
				monthly_usage_detials_table_object[key] = {
					"months" : key,
					"hours_of_usage" : Math.round(monthly_usage_detials_obj[key].hours_usege/36)/100,
					"unique_days_of_use" : monthly_usage_detials_obj[key].unique_days_usage
				}
				
			}
			status = 200;
			responseText.monthly_usage_detials_table_object = monthly_usage_detials_table_object;
			res.status(status).send(JSON.stringify(responseText));
			
		}
	})
}

Client.getSchoolDetails = function (req, res) {
    const schoolId = req.query.did;
    const pathToSchoolDetails = `Tablab/Schools/${schoolId}/School_Details`;
    modelsClientObj.getter(pathToSchoolDetails, (schoolDetails) => {
        var responseText = {};
        var status = 0;

        // check if schoolDetail is null
        if (!schoolDetails || !schoolDetails.val()) {
            status = 404;
            responseText.error = 'Error while retrieving school details!';
        } else {
            status = 200;

            const classesCovered = 'Classes Covered';
            const sd = schoolDetails.val();
            responseText.schoolDetails = {
                'School Name': sd.schoolName,
                'Location': sd.location.area + ", " + sd.location.state,
                'Project Funder': sd.fundingPartnerName,
                'Commencement Date': sd.commencementDate,
				'Expiry Date': sd.expiryDate,
                'Implementation Partner': sd.implementationPartnerName,
                'Number Of Students': sd.numberOfStudents,
                'Classes Covered': sd[classesCovered],
                'Number Of Teachers': sd.numberOfTeachers,
                'Number Of Tabs Installed': sd.totalNumberOfTablets
            }
        }
        res.status(status).send(JSON.stringify(responseText));
    });
}

Client.schoolMediaDetails = function (req, res) {
    const schoolId = req.query.did;
    const pathToSchoolDetails = `Tablab/Schools/${schoolId}/media`;
    modelsClientObj.getter(pathToSchoolDetails, (schoolDetails) => {
        var responseText = {};
        var status = 0;

        // check if schoolDetail is null
        if (!schoolDetails || !schoolDetails.val()) {
            status = 404;
            responseText.error = 'Error while retrieving school details!';
        } else {
            status = 200;

            const classesCovered = 'Classes Covered';
            const sd = schoolDetails.val();
			
            //console.log(sd);
			res.status(status).send(schoolDetails.val());
        }
        
    });
}

Client.getSchoolGrades = function (req, res) {
    const schoolId = req.query.did;
    const path = `Tablab/Classes/${schoolId}`;
    modelsClientObj.getter(path, (grades) => {
        var responseText = {};
        var status = 0;

        // check if there is any details
        if (!grades || !grades.val()) {
            status = 404;
            responseText.error = {
                'message': "Error In Getting Details..."
            };
        } else {
            status = 200;
            responseText.grades = grades.val();
        }
        res.status(status).send(JSON.stringify(responseText));
    });
}

Client.getGradeDetails = function (req, res) {
    const schoolId = req.query.did;
    const gradeId = req.query.gradeId;
    const path = `Tablab/Classes/${schoolId}/${gradeId}`;
    modelsClientObj.getter(path, (gradeDetails) => {
        var responseText = {};
        var status = 0;

        // check if there is any details
        if (!gradeDetails || !gradeDetails.val()) {
            status = 404;
            responseText.error = {
                'message': "Error In Getting Details..."
            };
        } else {
            status = 200;
            const d = gradeDetails.val();
            responseText.gradeDetails = {
                'Number Of Students': (d.Details.totalStudents) ? d.Details.totalStudents : 0,
                'Number Of Active Students': (d.Details.numberOfActiveStudents) ? d.Details.numberOfActiveStudents : 0
            }

            responseText.linkedTeachers = d.linkedTeachers;
        }
        res.status(status).send(JSON.stringify(responseText));
    });
}

Client.getTeachers = function (req, res) {
    var schoolID = req.query.did;
    modelsClientObj.getOrderedByChild(`users`, "did", schoolID, (snap) => {
        if (!snap || !snap.val()) {
            res.status(404).send(JSON.stringify({}));
            return;
        }
        var responseText = {};
        snap.forEach((userSnap) => {
            var user = userSnap.val();
            if (user.designation.toLowerCase() === TEACHER) {
                responseText[userSnap.key] = user;
            }
        });
        res.status(200).send(JSON.stringify(responseText));
    });
}

Client.downloadSampleCSVForStudentInfo = function (req, res) {
    var csvFileData = "Student First Name,Student Last Name,Student Class,E-mail,Contact Number";
    const file = `${__dirname}/../files/StudentInfoSample.csv`;

    // create file with fs.write
    fs.writeFile(file, csvFileData, (err) => {
        if (err) {
            res.status(500).send(err);
        }
        res.download(file);
    });
}

/**
 * For Graphical reports
 * 
 * @param {HTTPRequest} req
 * @param {HTTPResponse} res
 * 
 */
Client.getDataForGraphicalReports = function (req, res) {
    const schoolId = req.query.did;
    const reportType = req.query.reportType;
    const forWhat = req.query.forWhat;
    const selectedData = req.query.selectedData;
    const selectedDataType = req.query.selectedDataType;
    const startDate = req.query.start;
    const endDate = req.query.end;

    //    const path = `Tablab/analytics/${reportType}/${schoolId}/rawAnalytics`;
    const path = `${analyticsReference}/${schoolId}/${reportType}/appUsers`;
    // modelsClientObj.getter(path, function(reports) {
    modelsClientObj.getterWithQuery(path, startDate, endDate, NO_LIMIT_TO_LAST, function (reports) {
        var statusCode;
        var message = {};
        if (!reports) {
            statusCode = 404;
            message = {
                'error': 'Some Error Occured'
            };
        } else if (!reports.val()) {
            statusCode = 200;
            message = {
                'error': 'No data to show!'
            };
        } else {
            var nameList = [],
                timeList = [];

            var graphData = {};
			//console.log("SelectedData",reports);
            if (selectedData) {
				
                // Drill Down In Graph
                graphData = createGraphDataForSelectedData(reports, req.query);
                // graphData = createGraphDataForSelectedData(reports, forWhat, selectedData, selectedDataType)
            } else {
                graphData = createGraphData(reports, req.query);
                // graphData = createGraphData(reports, forWhat);
            }
			statusCode = 200;
			if(req.query.forWhat === "monthYear")
			{
				message = {
					graphData
				} 
			}
			else
			{
				convertToArray(graphData, nameList, timeList);
				message = {
					nameList,
					timeList
				};
			}
            
        }
        res.writeHeader(statusCode)
        res.write(JSON.stringify(message));
        res.end();
    })
}

// ---------------------------------- [START] Graphical Reports ---------------------------------- //

/** ************************* Drill Down In Graph *************************
 * for a given value of forWhat, it calculates the cumulative data
 * in terms of usage time of the content with respect to the value of 'forWhat' param
 * @selectedData param represents the single value of type @selectedDataType
 * (Eg. Videos in Category, 8 in Classes etc.)
 * 
 * @param {Object} rawDataSnapForGraphs 
 * @param {String} forWhat 
 * @param {String} selectedData  
 * @param {String} selectedDataType
 * @returns {Object} cumulativeAnalyticsObject
 */

function createGraphDataForSelectedData(rawDataSnapForGraphs, queryParameters) {
   	let { forWhat, selectedData, selectedDataType } = queryParameters;
   console.log(rawDataSnapForGraphs.numChildren());
	const obj = {};
	
	if(forWhat === "monthYear")
	{
		rawDataSnapForGraphs.forEach(function (userIdNode) { // User Id node
			userIdNode.forEach((report) => {
				var usage = report.val();
				var durationOfUse = convertSecondsIntoHours(usage.durationOfUse);
				var property = usage[forWhat];
				var category = usage['contentCategory'];
				var subject = usage['subjectOfContent'];
				var className = usage.className;
				var subjectName = usage.subjectOfContent;
				if (selectedData === usage[selectedDataType]) {	
					if (queryClassName && querySubjectName && queryClassName !== 'undefined') 
					{
						if (queryClassName === className && querySubjectName === subjectName) 
						{
							if (obj.hasOwnProperty(property)) 
							{
								obj[property] += +durationOfUse;
							} 
							else 
							{
								obj[property] = +durationOfUse;
							}
							////////////////////////////  extra start 111111 category wise wise /////////////////
							if(category_wise.hasOwnProperty(category))
							{
								if(category_wise[category].hasOwnProperty(property))
								{
									
									category_wise[category][property] += +durationOfUse;
								}
								else
								{
									category_wise[category][property] = +durationOfUse
									
								}
							}
							else 
							{	
								category_wise[category] = {
									[property] : +durationOfUse
								}
							}

							if(subject_wise.hasOwnProperty(subject))
							{
								if(subject_wise[subject].hasOwnProperty(property))
								{
									
									subject_wise[subject][property] += +durationOfUse;
								}
								else
								{
									subject_wise[subject][property] = +durationOfUse
									
								}
							}
							else 
							{	
								subject_wise[subject] = {
									[property] : +durationOfUse
								}
							}
							//////////// extra end 111111 //////////////////

							////////////////////////////  extra start 111111 subject wise wise /////////////////
							if(category_wise.hasOwnProperty(category))
							{
								if(category_wise[category].hasOwnProperty(property))
								{
									
									category_wise[category][property] += +durationOfUse;
								}
								else
								{
									category_wise[category][property] = +durationOfUse
									
								}
							}
							else 
							{	
								category_wise[category] = {
									[property] : +durationOfUse
								}
							}

							if(subject_wise.hasOwnProperty(subject))
							{
								if(subject_wise[subject].hasOwnProperty(property))
								{
									
									subject_wise[subject][property] += +durationOfUse;
								}
								else
								{
									subject_wise[subject][property] = +durationOfUse
									
								}
							}
							else 
							{	
								subject_wise[subject] = {
									[property] : +durationOfUse
								}
							}
							//////////// extra end 111111 //////////////////
						}
					} 
					else 
					{
						if (obj.hasOwnProperty(property)) 
						{
							obj[property] += +durationOfUse;
						} 
						else 
						{
							obj[property] = +durationOfUse;
						}
						////////////////////////////  extra start 222222 /////////////////
						
						if(category_wise.hasOwnProperty(category))
						{
							if(category_wise[category].hasOwnProperty(property))
							{
								
								category_wise[category][property] += +durationOfUse;
							}
							else
							{
								category_wise[category][property] = +durationOfUse
								
							}
						}
						else 
						{	
							category_wise[category] = {
								[property] : +durationOfUse
							}
						}

						if(subject_wise.hasOwnProperty(subject))
						{
							if(subject_wise[subject].hasOwnProperty(property))
							{
								
								subject_wise[subject][property] += +durationOfUse;
							}
							else
							{
								subject_wise[subject][property] = +durationOfUse
								
							}
						}
						else 
						{	
							subject_wise[subject] = {
								[property] : +durationOfUse
							}
						}
						////////////////////////////  extra end 2222222 /////////////////
					}
				}
			});
		});
		return {cumlative:obj,category_wise,subject_wise}
	}
	else	
	{
		rawDataSnapForGraphs.forEach(function (userIdNode) { // User Id node
			userIdNode.forEach((report) => {
				var usage = report.val();
				var durationOfUse = convertSecondsIntoHours(usage.durationOfUse);
				var property = usage[forWhat];
				if (selectedData === usage[selectedDataType]) {
					if (obj.hasOwnProperty(property)) {
						obj[property] += +durationOfUse;
					} else {
						obj[property] = +durationOfUse;
					}
				}
			});
		});
		return obj;
	}
    
}

/**
 * for a given value of forWhat, it calculates the cumulative data
 * in terms of usage time of the content with respect to the value of 'forWhat' param
 * 
 * @param {Object} rawDataSnapForGraphs 
 * @param {String} forWhat 
 * @returns {Object} cumulativeAnalyticsObject
 */
// ********* For the structure (date:{ "userID*randomKey": {} }) **********
function createGraphData(rawDataSnapForGraphs, queryParameters) {
    var forWhat = queryParameters.forWhat;
    var queryClassName = queryParameters.grade;
	var querySubjectName = queryParameters.subjectName;
	const obj = {};
	const category_wise = {};
 	const subject_wise = {};
	console.log(rawDataSnapForGraphs.numChildren());
	
	if(forWhat === "monthYear")
	{
		
		rawDataSnapForGraphs.forEach(function (userIdNode) { // User Id node
			userIdNode.forEach((report) => {
				var usage = report.val();
				var durationOfUse = convertSecondsIntoHours(usage.durationOfUse);
				var property = usage[forWhat];
				var category = usage['contentCategory'];
				var subject = usage['subjectOfContent'];
				var className = usage.className;
				var subjectName = usage.subjectOfContent;
				if (queryClassName && querySubjectName && queryClassName !== 'undefined') 
				{
					if (queryClassName === className && querySubjectName === subjectName) 
					{
						if (obj.hasOwnProperty(property)) 
						{
							obj[property] += +durationOfUse;
						} 
						else 
						{
							obj[property] = +durationOfUse;
						}
						////////////////////////////  extra start 111111 category wise wise /////////////////
						if(category_wise.hasOwnProperty(category))
						{
							if(category_wise[category].hasOwnProperty(property))
							{
								
								category_wise[category][property] += +durationOfUse;
							}
							else
							{
								category_wise[category][property] = +durationOfUse
								
							}
						}
						else 
						{	
							category_wise[category] = {
								[property] : +durationOfUse
							}
						}

						if(subject_wise.hasOwnProperty(subject))
						{
							
							if(subject_wise[subject].hasOwnProperty(property))
							{
								
								subject_wise[subject][property] += +durationOfUse;
							}
							else
							{
								subject_wise[subject][property] = +durationOfUse
								
							}
						}
						else 
						{	
							subject_wise[subject] = {
								[property] : +durationOfUse
							}
						}
						//////////// extra end 111111 //////////////////

						////////////////////////////  extra start 111111 subject wise wise /////////////////
						if(category_wise.hasOwnProperty(category))
						{
							if(category_wise[category].hasOwnProperty(property))
							{
								
								category_wise[category][property] += +durationOfUse;
							}
							else
							{
								category_wise[category][property] = +durationOfUse
								
							}
						}
						else 
						{	
							category_wise[category] = {
								[property] : +durationOfUse
							}
						}

						if(subject_wise.hasOwnProperty(subject))
						{
							if(subject_wise[subject].hasOwnProperty(property))
							{
								
								subject_wise[subject][property] += +durationOfUse;
							}
							else
							{
								subject_wise[subject][property] = +durationOfUse
								
							}
						}
						else 
						{	
							subject_wise[subject] = {
								[property] : +durationOfUse
							}
						}
						//////////// extra end 111111 //////////////////
					}
				} 
				else 
				{
					if (obj.hasOwnProperty(property)) 
					{
						obj[property] += +durationOfUse;
					} 
					else 
					{
						obj[property] = +durationOfUse;
					}
					////////////////////////////  extra start 222222 /////////////////
					
					if(category_wise.hasOwnProperty(category))
					{
						if(category_wise[category].hasOwnProperty(property))
						{
							
							category_wise[category][property] += +durationOfUse;
						}
						else
						{
							category_wise[category][property] = +durationOfUse
							
						}
					}
					else 
					{	
						category_wise[category] = {
							[property] : +durationOfUse
						}
					}

					if(subject_wise.hasOwnProperty(subject))
					{
						if(subject === "")
						{
							//console.log(userIdNode.val())
						}
						if(subject_wise[subject].hasOwnProperty(property))
						{
							
							subject_wise[subject][property] += +durationOfUse;
						}
						else
						{
							subject_wise[subject][property] = +durationOfUse
							
						}
					}
					else 
					{	
						if(subject === "")
						{
							//console.log(userIdNode.val())
						}
						subject_wise[subject] = {
							[property] : +durationOfUse
						}
					}
					////////////////////////////  extra end 2222222 /////////////////
				}
			});
		});
		return {cumlative:obj,category_wise,subject_wise}
		
	}
	else
	{
		//console.log("Main Key",rawDataSnapForGraphs.key);
        console.log("Total number of results",rawDataSnapForGraphs.numChildren());
				
		
		rawDataSnapForGraphs.forEach(function (userIdNode) { // User Id node
         console.log("Date ",userIdNode.key);
		console.log("results in date",userIdNode.numChildren());
		
			userIdNode.forEach((report) => {
				//console.log("Key",report.);
				var usage = report.val();
				var durationOfUse = convertSecondsIntoHours(usage.durationOfUse);
				var property = usage[forWhat];
				var className = usage.className;
				var subjectName = usage.subjectOfContent;
				if (queryClassName && querySubjectName && queryClassName !== 'undefined') {
					if (queryClassName === className && querySubjectName === subjectName) {
						if (obj.hasOwnProperty(property)) {
							obj[property] += +durationOfUse;
						} else {
							obj[property] = +durationOfUse;
						}
					}
				} else {
					if (obj.hasOwnProperty(property)) {
						obj[property] += +durationOfUse;
					} else {
						obj[property] = +durationOfUse;
					}
				}
			});
		});
		console.log(obj);
		return obj;
	}
    

    
	//console.log(obj,">>>>>>>>>>>")
	
	
}

// ---------------------------------- [END] Graphical Reports ---------------------------------- //


// ---------------------------------- [START] Tabular Reports ---------------------------------- //
/**
 * Calculates the usage time of each category for each class/grade
 * of the school referred by schoolId (req.query.did)
 * And responses back a json strigified object which contains either error message
 * or calculted cumulative analytics depending upon the usageReports returned from database
 * 
 * @param {HTTPRequest} req
 * @param {HTTPResponse} res
 */
Client.getClassWiseCategoryUsageData = function (req, res) {

    var schoolId = req.query.did;
    const grade = req.query.grade;
    const startDate = req.query.start;
    const endDate = req.query.end;

    const path = `${analyticsReference}/${schoolId}/${TYPE_CONTENT_USAGE}/appUsers`;
    modelsClientObj.getterWithQuery(path, `${startDate}`, `${endDate}`, NO_LIMIT_TO_LAST, (snap) => {
        var responseText = {}
        if (!snap || !snap.val()) {
            responseText.error = "No Usage found";
        } else {
            responseText.cumulativeUsage = contentTabularReports(snap, req.query);
        }
        res.send(JSON.stringify(responseText));
    })
}

Client.getMonthlyUsageReports = function (req,res) {

}

// Returns Classwise category usage data
function contentTabularReports(snap, queryParameters) {
    var cumulativeUsage = {};
    const grade = queryParameters.grade;
    const querySubjectName = queryParameters.subjectName;
    const startDate = queryParameters.start;
    const endDate = queryParameters.end;

    // For tabular reports of usage by students of the given grade
    if (grade) {
        snap.forEach((userIdNode) => { // User Id node
            userIdNode.forEach((report) => {
                var usage = report.val();
                const className = usage.className;
                const subjectName = usage.subjectOfContent;
                const studentName = usage.userName;
                const contentCategory = usage.contentCategory.toLowerCase();
                const durationOfUse = convertSecondsIntoHours(usage.durationOfUse);

                if (querySubjectName && querySubjectName !== "undefined") {
                    if (grade === className && subjectName === querySubjectName) {
                        if (cumulativeUsage.hasOwnProperty(studentName)) {
                            if (cumulativeUsage[studentName].hasOwnProperty(contentCategory)) {
                                cumulativeUsage[studentName][contentCategory] += +durationOfUse;
                                cumulativeUsage[studentName].total += +durationOfUse;
                            } else {
                                cumulativeUsage[studentName][contentCategory] = +durationOfUse;
                                cumulativeUsage[studentName].total += +durationOfUse;
                            }
                        } else {
                            cumulativeUsage[studentName] = {
                                "total": +durationOfUse
                            };
                            cumulativeUsage[studentName][contentCategory] = +durationOfUse;
                        }
                    }
                } else {
                    if (grade === className) {
                        if (cumulativeUsage.hasOwnProperty(studentName)) {
                            if (cumulativeUsage[studentName].hasOwnProperty(contentCategory)) {
                                cumulativeUsage[studentName][contentCategory] += +durationOfUse;
                                cumulativeUsage[studentName].total += +durationOfUse;
                            } else {
                                cumulativeUsage[studentName][contentCategory] = +durationOfUse;
                                cumulativeUsage[studentName].total += +durationOfUse;
                            }
                        } else {
                            cumulativeUsage[studentName] = {
                                "total": +durationOfUse
                            };
                            cumulativeUsage[studentName][contentCategory] = +durationOfUse;
                        }
                    }
                }
            });
        });
    } else { // For tabular reports of collective usage by all the grades of that school
        snap.forEach((userIdNode) => { // User Id node
            userIdNode.forEach((report) => {
                var usage = report.val();
                const className = usage.className;
                const contentCategory = usage.contentCategory.toLowerCase();
                const durationOfUse = convertSecondsIntoHours(usage.durationOfUse);
                const dateOfAccess = usage.dateOfAccess;

                if (cumulativeUsage.hasOwnProperty(className)) {
                    if (cumulativeUsage[className].hasOwnProperty(contentCategory)) {
                        cumulativeUsage[className][contentCategory] += +durationOfUse;
                    } else {
                        cumulativeUsage[className][contentCategory] = +durationOfUse;
                    }
                } else {
                    cumulativeUsage[className] = {};
                    cumulativeUsage[className][contentCategory] = +durationOfUse;
                }
            })
        })
    }
    return cumulativeUsage;
}

//  ----------------------------- [START] AssessmentsUsageData ---------------------------------- //

Client.getAssessmentsUsage = function (req, res) {
    var schoolId = req.query.did;
    const startDate = req.query.start;
    const endDate = req.query.end;
    const grade = req.query.grade;

    const path = `${analyticsReference}/${schoolId}/${TYPE_ASSESSMENTS_USAGE}/appUsers`;
    // modelsClientObj.getter(path, (snap) => {

    if (grade) {
        // modelsClientObj.getterWithQuery(path, (`${startDate}*${schoolId}_${grade}`), (`${endDate}*${schoolId}_${grade}`), NO_LIMIT_TO_LAST, (snap) => {
        modelsClientObj.getterWithQuery(path, (`${startDate}`), (`${endDate}`), NO_LIMIT_TO_LAST, (snap) => {
            var responseText = {}
            if (!snap || !snap.val()) {
                responseText.error = "No Usage found";
            } else {
                responseText.cumulativeUsage = assessmentsTabularReportsPerClass(snap, req.query);
            }
            res.send(JSON.stringify(responseText));
        })
    } else {
        // modelsClientObj.getterWithQuery(path, (`${startDate}*${schoolId}_${grade}`), (`${endDate}*${schoolId}_${grade}`), LIMIT_TO_LAST, (snap) => {
        modelsClientObj.getterWithQuery(path, (`${startDate}`), (`${endDate}`), LIMIT_TO_LAST, (snap) => {
            var responseText = {}
            if (!snap || !snap.val()) {
                responseText.error = "No Usage found";
            } else {
                // responseText.cumulativeUsage = snap.val(); 
                responseText.cumulativeUsage = assessmentsRawReports(snap, req.query);
            }
            res.send(JSON.stringify(responseText));
        })
    }
}

Client.usermasterReportsDetails = function (req, res) {


	var schoolId = req.query.did;
    const startDate = req.query.start;
	const endDate = req.query.end;

	const path = `/Tablab/analytics/${schoolId}/ContentUsageReports/appUsers`;
	
	modelsClientObj.getterWithQuery(path, (`${startDate}`), (`${endDate}`), NO_LIMIT_TO_LAST, (snap) => {
		var responseText = {}
		if (!snap || !snap.val()) {
			responseText.error = "No Usage found";
			
		} else {
			
			responseText.cumulativeUsage = cumulativeUsage(snap.val());
		}
		res.send(JSON.stringify(responseText));
	})
}

function cumulativeUsage(snap) {
	let cumulative_usage = {};
	for(key in snap)
	{
		for(nested_key in snap[key])
		{
			cumulative_usage[nested_key] = snap[key][nested_key];
		}
	}
	return cumulative_usage;

}

function assessmentsRawReports(snap, queryParameters) {
    var usageObject = {};
    snap.forEach((userIdNode) => { // userId node
        userIdNode.forEach((report) => {
            var usage = report.val();
            usageObject[report.key] = usage;
        });
    });
    return usageObject;
}

Client.getAssessmentsReportsPerUser = function (req, res) {
    var queryParameters = req.query;
    const studentID = queryParameters.studentID;
    const startDate = queryParameters.start;
    const endDate = queryParameters.end;
    const schoolId = queryParameters.did;
    const path = `${analyticsReference}/${schoolId}/${TYPE_ASSESSMENTS_USAGE}/appUsers`;
    modelsClientObj.getterWithQuery(path, `${startDate}`, `${endDate}`, NO_LIMIT_TO_LAST, (snap) => {
        var responseText = {}
        if (!snap || !snap.val()) {
            responseText.error = "No Usage found";
        } else {
            var userObject = {};
            snap.forEach((childSnap) => { // Date node
                childSnap.forEach((report) => {
                    var usage = report.val();
                    const currentStudentID = (report.key).split(KEY_DELIMITER)[0];
                    const assessmentType = usage.assessmentType;
                    const totalScore = +(usage.totalScore);
                    const totalQuestions = +(usage.totalQuestions);

                    if (currentStudentID === studentID) {
                        if (userObject.hasOwnProperty(assessmentType)) { // Repeatitive current assessmentType
                            userObject[assessmentType].count += 1;
                            userObject[assessmentType].totalScore += totalScore;
                            userObject[assessmentType].totalQuestions += totalQuestions;
                        } else { // First time for current assessmentType
                            userObject[assessmentType] = {
                                "count": 1,
                                "totalScore": totalScore,
                                "totalQuestions": totalQuestions
                            }
                        }
                    }
                })

                //     }
            })
            //console.log('------ userObject ------', userObject);

            responseText.cumulativeUsage = userObject;
        }
        res.send(JSON.stringify(responseText));
    })
}

Client.getAttemptedAssessmentsReportsPerUser = function (req, res) {
    var queryParameters = req.query;
    const studentID = queryParameters.studentID;
    const startDate = queryParameters.start;
    const endDate = queryParameters.end;
    const schoolId = queryParameters.did;
    const queryAssessmentType = queryParameters.type;

    // const path = `${analyticsReference}/${schoolId}/${TYPE_ASSESSMENTS_USAGE}/appUsers/${studentID}`;
    // modelsClientObj.getter(path, (snap) => {
    const path = `${analyticsReference}/${schoolId}/${TYPE_ASSESSMENTS_USAGE}/appUsers`;
    // modelsClientObj.getterWithQuery(path, `${startDate}*${studentID}`, `${endDate}*${studentID}`, NO_LIMIT_TO_LAST, (snap) => {
    modelsClientObj.getterWithQuery(path, `${startDate}`, `${endDate}`, NO_LIMIT_TO_LAST, (snap) => {
        var responseText = {}
        if (!snap || !snap.val()) {
            responseText.error = "No Usage found";
        } else {
            var userObject = {};
            snap.forEach((childSnap) => { // Date node
                //     var dateKey = dateNode.key;
                //     if (dateKey >= startDate && dateKey <= endDate) {
                childSnap.forEach((report) => {
                    var usage = report.val();
                    const currentStudentID = (report.key).split(KEY_DELIMITER)[0];
                    const assessmentName = usage.assessmentName;
                    const assessmentType = usage.assessmentType;

                    if (currentStudentID === studentID) {
                        if (queryAssessmentType) {
                            if (queryAssessmentType === assessmentType) {
                                if (userObject.hasOwnProperty(assessmentName)) { // Repeatitive current assessmentName
                                    userObject[assessmentName].count += 1;
                                    userObject[assessmentName].totalScore += +(usage.totalScore);
                                    userObject[assessmentName].totalQuestions += +(usage.totalQuestions);

                                    if (!userObject[assessmentName].hasOwnProperty('assessmentType'))
                                        userObject[assessmentName].assessmentType = usage.assessmentType;
                                } else { // First time for current assessmentName
                                    userObject[assessmentName] = {
                                        "count": 1,
                                        "totalScore": +(usage.totalScore),
                                        "totalQuestions": +(usage.totalQuestions),
                                        "assessmentType": usage.assessmentType
                                    }
                                }
                            }
                        } else {
                            if (userObject.hasOwnProperty(assessmentName)) { // Repeatitive current assessmentName
                                userObject[assessmentName].count += 1;
                                userObject[assessmentName].totalScore += +(usage.totalScore);
                                userObject[assessmentName].totalQuestions += +(usage.totalQuestions);

                                if (!userObject[assessmentName].hasOwnProperty('assessmentType'))
                                    userObject[assessmentName].assessmentType = usage.assessmentType;
                            } else { // First time for current assessmentName
                                userObject[assessmentName] = {
                                    "count": 1,
                                    "totalScore": +(usage.totalScore),
                                    "totalQuestions": +(usage.totalQuestions),
                                    "assessmentType": usage.assessmentType
                                }
                            }
                        }
                    }
                })

                //     }
            })
            responseText.cumulativeUsage = userObject;
        }
        res.send(JSON.stringify(responseText));
    })
}

function assessmentsTabularReportsPerClass(snap, queryParameters) {
    var cumulativeUsage = {};
    const grade = queryParameters.grade;
    const querySubjectName = queryParameters.subjectName;
    const userID = queryParameters.auid; // appUserID
    const startDate = queryParameters.start;
    const endDate = queryParameters.end;

    snap.forEach((userIdNode) => { // User Id node
        userIdNode.forEach((report) => {
            var usage = report.val();
            // var userId = report.key.split(KEY_DELIMITER)[1];
            var userId = report.key.split(KEY_DELIMITER)[0];
            const assessmentType = usage.assessmentType;
            const totalScore = +(usage.totalScore);
            const totalQuestions = +(usage.totalQuestions);
            const userName = usage.userName;
            const userClass = usage.className;
            const subjectName = usage.subject;

            if (querySubjectName && querySubjectName !== 'undefined') { // If subject is also a filter
                if (userClass === grade && querySubjectName === subjectName) {
                    if (cumulativeUsage.hasOwnProperty(userId)) {
                        if (cumulativeUsage[userId].usage.hasOwnProperty(assessmentType)) { // Repeatitive current assessmentType
                            cumulativeUsage[userId].usage[assessmentType].count += 1;
                            cumulativeUsage[userId].usage[assessmentType].totalScore += totalScore;
                            cumulativeUsage[userId].usage[assessmentType].totalQuestions += totalQuestions;
                        } else {
                            cumulativeUsage[userId].usage[assessmentType] = {
                                "count": 1,
                                "totalScore": totalScore,
                                "totalQuestions": totalQuestions
                            }
                        }
                    } else {
                        cumulativeUsage[userId] = {
                            'userName': userName,
                            'usage': {}
                        }

                        cumulativeUsage[userId].usage[assessmentType] = {
                            "count": 1,
                            "totalScore": totalScore,
                            "totalQuestions": totalQuestions
                        }
                    }
                }
            } else { // If only student class is a filter
                if (userClass === grade) {
                    if (cumulativeUsage.hasOwnProperty(userId)) {
                        if (cumulativeUsage[userId].usage.hasOwnProperty(assessmentType)) { // Repeatitive current assessmentType
                            cumulativeUsage[userId].usage[assessmentType].count += 1;
                            cumulativeUsage[userId].usage[assessmentType].totalScore += totalScore;
                            cumulativeUsage[userId].usage[assessmentType].totalQuestions += totalQuestions;
                        } else {
                            cumulativeUsage[userId].usage[assessmentType] = {
                                "count": 1,
                                "totalScore": totalScore,
                                "totalQuestions": totalQuestions
                            }
                        }
                    } else {
                        cumulativeUsage[userId] = {
                            'userName': userName,
                            'usage': {}
                        }

                        cumulativeUsage[userId].usage[assessmentType] = {
                            "count": 1,
                            "totalScore": totalScore,
                            "totalQuestions": totalQuestions
                        }
                    }
                }
            }
        })
    })
    return cumulativeUsage;
}

// ------------------------------ [END] AssessmentsUsageData ------------------------------ //

// ------------------------------ [START] DOWNLOAD ------------------------------ //

// Download reports for category wise usage according to users
Client.downloadCategorywiseUsageByClasses = function (req, res) {
    // Create a new instance of a Workbook class
    var workbook = new excel.Workbook();

    // Add Worksheets to the workbook
    var worksheet = workbook.addWorksheet('Content Reports');

    // Create a reusable style
    var style = workbook.createStyle({
        font: {
            color: '#303133',
            size: 12
        }
    });

    // Create a reusable heading style
    var headingStyle = workbook.createStyle({
        font: {
            color: '#303133',
            size: 14
        }
    });


    const did = req.query.schoolID;
    const path = `${analyticsReference}/${did}/${req.query.type}/appUsers`;

    var startAt = '',
        endAt = '',
        forWhichKey = '';

    startAt = `${req.query.start}`;
    endAt = `${req.query.end}`;
    forWhichKey = 'className';

    modelsClientObj.getterWithQuery(path, startAt, endAt, -1, (snap) => {
        // var csvFileData = `Student Class,Apps,Assessments,Books,Multimedia,Videos\r`;

        worksheet.cell(1, 1).string('Student Class').style(headingStyle);
        worksheet.cell(1, 2).string('Apps').style(headingStyle);
        worksheet.cell(1, 3).string('Assessments').style(headingStyle);
        worksheet.cell(1, 4).string('Books').style(headingStyle);
        worksheet.cell(1, 5).string('Multimedia').style(headingStyle);
        worksheet.cell(1, 6).string('Videos').style(headingStyle);

        var cumulativeUsage = {};
        if (snap && snap.val()) {
            snap.forEach((childSnap) => {
                childSnap.forEach((report) => {
                    var usage = report.val();
                    const forWhat = usage[forWhichKey];
                    const contentCategory = usage.contentCategory.toLowerCase();
                    const durationOfUse = convertSecondsIntoHours(usage.durationOfUse);

                    if (cumulativeUsage.hasOwnProperty(forWhat)) {
                        if (cumulativeUsage[forWhat].hasOwnProperty(contentCategory)) {
                            cumulativeUsage[forWhat][contentCategory] += +durationOfUse;
                        } else {
                            cumulativeUsage[forWhat][contentCategory] = +durationOfUse;
                        }
                    } else {
                        cumulativeUsage[forWhat] = {};
                        cumulativeUsage[forWhat][contentCategory] = +durationOfUse;
                    }
                });
            });

            var i = 2;
            for (var key in cumulativeUsage) {
                worksheet.cell(i, 1).string(key).style(style);
                worksheet.cell(i, 2).number(((cumulativeUsage[key].apps) ? +cumulativeUsage[key].apps : 0)).style(style);
                worksheet.cell(i, 3).number(((cumulativeUsage[key].assessments) ? +cumulativeUsage[key].assessments : 0)).style(style);
                worksheet.cell(i, 4).number(((cumulativeUsage[key].books) ? +cumulativeUsage[key].books : 0)).style(style);
                worksheet.cell(i, 5).number(((cumulativeUsage[key].multimedia) ? +cumulativeUsage[key].multimedia : 0)).style(style);
                worksheet.cell(i, 6).number(((cumulativeUsage[key].videos) ? +cumulativeUsage[key].videos : 0)).style(style);
                i++;
            }
        }

        // Get current date
        var date = new Date();
        var today = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();

        // Directory to save reports file temporarily
        var reportsFileDir = `${__dirname}/../temp`;

        // Fetch school name from the database
        const pathToSchoolDetails = `Tablab/Schools/${did}/School_Details/schoolName`;
        modelsClientObj.getter(pathToSchoolDetails, (schoolName) => {
            var file = reportsFileDir + `/Reports_${schoolName.val()}_${today}.xlsx`;
            try {
                // create directory to save reports file, if doesn't exists
                if (!fs.existsSync(reportsFileDir)) {
                    fs.mkdirSync(reportsFileDir);
                }

                // write data into the file
                workbook.write(file, (error, stats) => {
                    if (error) {
                        res.status(500).send(error);
                    }

                    res.download(file, (fileServeError) => {
                        // delete the newly created downloadable
                        if (fs.existsSync(file)) {
                            fs.unlinkSync(file);
                        }
                    });
                });
            } catch (dirDeletionError) {
                res.status(500).send(dirDeletionError);
            }
        });
    });
}

Client.downloadSampleCSVForStudentInfo = function (req, res) {
    var csvFileData = "Student First Name,Student Last Name,Student Class,E-mail,Contact Number";
    const file = `${__dirname}/../files/StudentInfoSample.csv`;

    // create file with fs.write
    fs.writeFile(file, csvFileData, (err) => {
        if (err) {
            res.status(500).send(err);
        }
        res.download(file);
    });
}

Client.downloadCategorywiseUsageByStudents = function (req, res) {
    // const path = `session/${req.session.uid}/download/${req.query.type}`;
    const did = req.query.schoolID;
    const grade = req.query.grade;
    const path = `${analyticsReference}/${did}/${req.query.type}/appUsers`;

    var startAt = '',
        endAt = '',
        forWhichKey = '';

    startAt = `${req.query.start}`; // *${did}_${grade}`;
    endAt = `${req.query.end}`; // *${did}_${grade}`;
    forWhichKey = 'userName';

    modelsClientObj.getterWithQuery(path, startAt, endAt, -1, (snap) => {
        var csvFileData = `Student Name,Apps,Assessments,Books,Multimedia,Videos\r`;
        var cumulativeUsage = {};
        if (snap && snap.val()) {
            snap.forEach((childSnap) => {
                childSnap.forEach((report) => {
                    var usage = report.val();
                    const forWhat = usage[forWhichKey];
                    const contentCategory = usage.contentCategory.toLowerCase();
                    const durationOfUse = convertSecondsIntoHours(usage.durationOfUse);

                    if (grade) {
                        if (cumulativeUsage.hasOwnProperty(forWhat)) {
                            if (cumulativeUsage[forWhat].hasOwnProperty(contentCategory)) {
                                cumulativeUsage[forWhat][contentCategory] += +durationOfUse;
                            } else {
                                cumulativeUsage[forWhat][contentCategory] = +durationOfUse;
                            }
                        } else {
                            cumulativeUsage[forWhat] = {};
                            cumulativeUsage[forWhat][contentCategory] = +durationOfUse;
                        }
                    }
                });
            });

            for (var key in cumulativeUsage) {
                csvFileData += key + ",";
                var count = 0;
                csvFileData += ((cumulativeUsage[key].apps) ? cumulativeUsage[key].apps : "0") + ",";
                csvFileData += ((cumulativeUsage[key].assessments) ? cumulativeUsage[key].assessments : "0") + ",";
                csvFileData += ((cumulativeUsage[key].books) ? cumulativeUsage[key].books : "0") + ",";
                csvFileData += ((cumulativeUsage[key].multimedia) ? cumulativeUsage[key].multimedia : "0") + ",";
                csvFileData += ((cumulativeUsage[key].videos) ? cumulativeUsage[key].videos : "0") + "\r";
            }
        }

        const file = `${__dirname}/../tmp/report.csv`;

        // delete old file if created ever
        try {
            if (fs.existsSync(file)) {
                fs.unlinkSync(file);
            }
        } catch (err) {
            console.log('Error in file deletion = ' + err);
        }

        // create file with fs.write
        fs.writeFile(file, csvFileData, (err) => {
            if (err) {
                res.status(500).send(err);
            }
            res.download(file);
        });
    });
}

/**
 * Reports in excel format with two excel sheets one for each of the following
 * 1. Student wise usage of the LMS content
 * 2. Student wise usage of the Tablab assessments
 */
Client.downloadUsageReportsByStudents = function (req, res) {
    // Create a new instance of a Workbook class
    var workbook = new excel.Workbook();

    // Add Worksheets to the workbook
    var contentReportsWorksheet = workbook.addWorksheet('Content Reports');
    var assessmentsReportsWorksheet = workbook.addWorksheet('Assessment Reports');

    // Create a reusable style
    var style = workbook.createStyle({
        font: {
            color: '#303133',
            size: 12
        }
    });

    // Create a reusable heading style
    var headingStyle = workbook.createStyle({
        font: {
            color: '#303133',
            size: 14
        }
    });

    const did = req.query.schoolID;
    const grade = req.query.grade;
    const path = `${analyticsReference}/${did}/${req.query.type}/appUsers`;

    var startAt = '',
        endAt = '',
        forWhichKey = '';

    startAt = `${req.query.start}`;
    endAt = `${req.query.end}`;

    // Prepare sheet 1 - content reports
    modelsClientObj.getterWithQuery(path, startAt, endAt, -1, (snap) => {
        contentReportsWorksheet.cell(1, 1).string('Student Name').style(headingStyle);
        contentReportsWorksheet.cell(1, 2).string('Apps').style(headingStyle);
        contentReportsWorksheet.cell(1, 3).string('Assessments').style(headingStyle);
        contentReportsWorksheet.cell(1, 4).string('Books').style(headingStyle);
        contentReportsWorksheet.cell(1, 5).string('Multimedia').style(headingStyle);
        contentReportsWorksheet.cell(1, 6).string('Videos').style(headingStyle);

        var cumulativeUsage = {};
        if (snap && snap.val()) {
            snap.forEach((childSnap) => {
                childSnap.forEach((report) => {
                    var usage = report.val();
                    const userName = usage.userName;
                    const userClass = usage.className;
                    const contentCategory = usage.contentCategory.toLowerCase();
                    const durationOfUse = convertSecondsIntoHours(usage.durationOfUse);

                    if (userClass === grade) {
                        if (cumulativeUsage.hasOwnProperty(userName)) {
                            if (cumulativeUsage[userName].hasOwnProperty(contentCategory)) {
                                cumulativeUsage[userName][contentCategory] += +durationOfUse;
                            } else {
                                cumulativeUsage[userName][contentCategory] = +durationOfUse;
                            }
                        } else {
                            cumulativeUsage[userName] = {};
                            cumulativeUsage[userName][contentCategory] = +durationOfUse;
                        }
                    }
                });
            });

            var i = 2;
            for (var key in cumulativeUsage) {
                contentReportsWorksheet.cell(i, 1).string(key).style(style);
                contentReportsWorksheet.cell(i, 2).number(((cumulativeUsage[key].apps) ? +cumulativeUsage[key].apps : 0)).style(style);
                contentReportsWorksheet.cell(i, 3).number(((cumulativeUsage[key].assessments) ? +cumulativeUsage[key].assessments : 0)).style(style);
                contentReportsWorksheet.cell(i, 4).number(((cumulativeUsage[key].books) ? +cumulativeUsage[key].books : 0)).style(style);
                contentReportsWorksheet.cell(i, 5).number(((cumulativeUsage[key].multimedia) ? +cumulativeUsage[key].multimedia : 0)).style(style);
                contentReportsWorksheet.cell(i, 6).number(((cumulativeUsage[key].videos) ? +cumulativeUsage[key].videos : 0)).style(style);
                i++;
            }
        }

        // Prepare sheet 2 - assessments reports
        const path2 = `${analyticsReference}/${did}/${TYPE_ASSESSMENTS_USAGE}/appUsers`;
        modelsClientObj.getterWithQuery(path2, startAt, endAt, -1, (snap) => {
            assessmentsReportsWorksheet.cell(1, 1, 2, 1, true).string('Student Name').style(headingStyle);
            assessmentsReportsWorksheet.cell(1, 2, 1, 3, true).string('Practice Assessment').style(headingStyle);
            assessmentsReportsWorksheet.cell(1, 4, 1, 5, true).string('Locked Term Assessment').style(headingStyle);
            assessmentsReportsWorksheet.cell(1, 6, 1, 7, true).string('Unlocked Term Assessment').style(headingStyle);
            assessmentsReportsWorksheet.cell(2, 2).string('Number').style(headingStyle);
            assessmentsReportsWorksheet.cell(2, 3).string('Average').style(headingStyle);
            assessmentsReportsWorksheet.cell(2, 4).string('Number').style(headingStyle);
            assessmentsReportsWorksheet.cell(2, 5).string('Average').style(headingStyle);
            assessmentsReportsWorksheet.cell(2, 6).string('Number').style(headingStyle);
            assessmentsReportsWorksheet.cell(2, 7).string('Average').style(headingStyle);

            var cumulativeUsage2 = {};
            if (snap && snap.val()) {
                snap.forEach((childSnap) => {
                    childSnap.forEach((report) => {
                        var usageReport = report.val();
                        var userId = report.key.split(KEY_DELIMITER)[0];

                        const assessmentType = usageReport.assessmentType;
                        const totalScore = +(usageReport.totalScore);
                        const totalQuestions = +(usageReport.totalQuestions);
                        const userName = usageReport.userName;
                        const userClass = usageReport.className;

                        if (userClass === grade) {
                            if (cumulativeUsage2.hasOwnProperty(userId)) {
                                if (cumulativeUsage2[userId].usage.hasOwnProperty(assessmentType)) { // Repeatitive current assessmentType
                                    cumulativeUsage2[userId].usage[assessmentType].count += 1;
                                    cumulativeUsage2[userId].usage[assessmentType].totalScore += totalScore;
                                    cumulativeUsage2[userId].usage[assessmentType].totalQuestions += totalQuestions;
                                } else {
                                    cumulativeUsage2[userId].usage[assessmentType] = {
                                        "count": 1,
                                        "totalScore": totalScore,
                                        "totalQuestions": totalQuestions
                                    }
                                }
                            } else {
                                cumulativeUsage2[userId] = {
                                    'userName': userName,
                                    'usage': {}
                                }

                                cumulativeUsage2[userId].usage[assessmentType] = {
                                    "count": 1,
                                    "totalScore": totalScore,
                                    "totalQuestions": totalQuestions
                                }
                            }
                        }
                    });
                });

                var j = 3;
                for (var key in cumulativeUsage2) {
                    var k = 2;
                    var object = cumulativeUsage2[key];
                    const username = object.userName;
                    assessmentsReportsWorksheet.cell(j, 1).string(username).style(style);

                    const o1 = object.usage["Practice"];
                    const o2 = object.usage["LockedTerm"];
                    const o3 = object.usage["UnlockedTerm"];

                    const avg1 = (o1) ? (100 * ((+o1.totalScore) / (+o1.totalQuestions))) : 0,
                        avg2 = (o2) ? (100 * ((+o2.totalScore) / (+o2.totalQuestions))) : 0,
                        avg3 = (o3) ? (100 * ((+o3.totalScore) / (+o3.totalQuestions))) : 0;

                    assessmentsReportsWorksheet.cell(j, 2).number((o1) ? o1.count : 0).style(style);
                    assessmentsReportsWorksheet.cell(j, 3).number(avg1).style(style);
                    assessmentsReportsWorksheet.cell(j, 4).number((o2) ? o2.count : 0).style(style);
                    assessmentsReportsWorksheet.cell(j, 5).number(avg2).style(style);
                    assessmentsReportsWorksheet.cell(j, 6).number((o3) ? o3.count : 0).style(style);
                    assessmentsReportsWorksheet.cell(j, 7).number(avg3).style(style);
                    j++;
                }
            }

            // Get current date
            var date = new Date();
            var today = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();

            // Directory to save reports file temporarily
            var reportsFileDir = `${__dirname}/../temp`;

            // Fetch school name from the database
            const pathToSchoolDetails = `Tablab/Schools/${did}/School_Details/schoolName`;
            modelsClientObj.getter(pathToSchoolDetails, (schoolName) => {
                var file = reportsFileDir + `/Reports_${schoolName.val()}_${today}.xlsx`;
                try {
                    // create directory to save reports file, if doesn't exists
                    if (!fs.existsSync(reportsFileDir)) {
                        fs.mkdirSync(reportsFileDir);
                    }

                    // write data into the file
                    workbook.write(file, (error, stats) => {
                        if (error) {
                            res.status(500).send(error);
                        }

                        res.download(file, (fileServeError) => {
                            // delete the newly created downloadable
                            if (fs.existsSync(file)) {
                                fs.unlinkSync(file);
                            }
                        });
                    });
                } catch (dirDeletionError) {
                    res.status(500).send(dirDeletionError);
                }
            });
        });
    });
}


Client.downloadUserMasterReports = function(req,res) {
	var workbook = new excel.Workbook();
	var userMasterReportsWorksheet = workbook.addWorksheet('User Master Reports');
	var style = workbook.createStyle({
        font: {
            color: '#303133',
            size: 12
		}
	});
	var headingStyle = workbook.createStyle({
        font: {
            color: '#303133',
            size: 14
        }
	});
	var startAt = '',
        endAt = '',

    startAt = `${req.query.start}`;
    endAt = `${req.query.end}`;
	const schoolId = req.query.did;
	console.log(startAt,endAt)
	let path = `/Tablab/analytics/${schoolId}/ContentUsageReports/appUsers`
	modelsClientObj.getterWithQuery(path, startAt, endAt, -1,(snap)=>{
		userMasterReportsWorksheet.cell(1, 1).string('Student Name').style(headingStyle);
        userMasterReportsWorksheet.cell(1, 2).string('Student Class').style(headingStyle);
        userMasterReportsWorksheet.cell(1, 3).string('Date Of Access').style(headingStyle);
        userMasterReportsWorksheet.cell(1, 4).string('Time Of Access').style(headingStyle);
		userMasterReportsWorksheet.cell(1, 6).string('Subject').style(headingStyle);
		userMasterReportsWorksheet.cell(1, 5).string('Content Category').style(headingStyle);
        userMasterReportsWorksheet.cell(1, 7).string('Duration of use').style(headingStyle);
        userMasterReportsWorksheet.cell(1, 8).string('Topic Name').style(headingStyle);
        userMasterReportsWorksheet.cell(1, 9).string('Sub Topic Name').style(headingStyle);
		
		if (snap && snap.val()) {
            var i = 2;
            snap.forEach((childSnap) => {
                childSnap.forEach((report) => {
                    userMasterReportsWorksheet.cell(i, 1).string(report.val().userName).style(style);
                    userMasterReportsWorksheet.cell(i, 2).string(report.val().className).style(style);
					userMasterReportsWorksheet.cell(i, 3).string(report.val().dateOfAccess).style(style);
					userMasterReportsWorksheet.cell(i, 4).string(report.val().timeOfAccess).style(style);
                    userMasterReportsWorksheet.cell(i, 5).string(report.val().subjectOfContent).style(style);
		
					userMasterReportsWorksheet.cell(i, 6).string(report.val().contentCategory).style(style);
                    const durationOfUse = getTimeFromSeconds(report.val().durationOfUse);
                    userMasterReportsWorksheet.cell(i, 7).string(durationOfUse).style(style);

                    const score = `${report.val().totalScore}/${report.val().totalQuestions}`;
                    userMasterReportsWorksheet.cell(i, 8).string(report.val().resourceName).style(style);

                    userMasterReportsWorksheet.cell(i, 9).string("N/A").style(style);
                    i++;
                });
			});
			
		}
		// Get current date
        var date = new Date();
        var today = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();

        // Directory to save reports file temporarily
        var reportsFileDir = `${__dirname}/../temp`;

        // Fetch school name from the database
        const pathToSchoolDetails = `Tablab/Schools/${schoolId}/School_Details/schoolName`;
        modelsClientObj.getter(pathToSchoolDetails, (schoolName) => {
            var file = reportsFileDir + `/User_Master_Reports_${schoolName.val()}_${today}.xlsx`;
            try {
                // create directory to save reports file, if doesn't exists
                if (!fs.existsSync(reportsFileDir)) {
                    fs.mkdirSync(reportsFileDir);
                }

                // write data into the file
                workbook.write(file, (error, stats) => {
                    if (error) {
                        res.status(500).send(error);
                    }

                    res.download(file, (fileServeError) => {
                        // delete the newly created downloadable
                        if (fs.existsSync(file)) {
                            fs.unlinkSync(file);
                        }
                    });
                });
            } catch (dirDeletionError) {
                res.status(500).send(dirDeletionError);
            }
        });
	
	});
}

Client.downloadAssessmentsRawReports = function (req, res) {
    // Create a new instance of a Workbook class
    var workbook = new excel.Workbook();

    // Add Worksheets to the workbook
    var assessmentsReportsWorksheet = workbook.addWorksheet('Assessment Reports');

    // Create a reusable style
    var style = workbook.createStyle({
        font: {
            color: '#303133',
            size: 12
        }
    });

    // Create a reusable heading style
    var headingStyle = workbook.createStyle({
        font: {
            color: '#303133',
            size: 14
        }
    });

    const did = req.query.schoolID;
    const path = `${analyticsReference}/${did}/${req.query.type}/appUsers`;

    var startAt = '',
        endAt = '';

    startAt = `${req.query.start}`;
    endAt = `${req.query.end}`;

    modelsClientObj.getterWithQuery(path, startAt, endAt, NO_LIMIT_TO_LAST, (snap) => {
        assessmentsReportsWorksheet.cell(1, 1).string('Student Name').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 2).string('Student Class').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 3).string('Date Of Access').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 4).string('Time Of Access').style(headingStyle);
		assessmentsReportsWorksheet.cell(1, 5).string('Subject').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 6).string('Type').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 7).string('Duration of use').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 8).string('Score').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 9).string('Completion Status').style(headingStyle);

        var cumulativeUsage = {};
        if (snap && snap.val()) {
            var i = 2;
            snap.forEach((childSnap) => {
                childSnap.forEach((report) => {
                    assessmentsReportsWorksheet.cell(i, 1).string(report.val().userName).style(style);
                    assessmentsReportsWorksheet.cell(i, 2).string(report.val().className).style(style);
					assessmentsReportsWorksheet.cell(i, 3).string(report.val().dateOfAssessmentTaken).style(style);
					assessmentsReportsWorksheet.cell(i, 4).string(report.val().timeOfAssessmentTaken).style(style);
                    assessmentsReportsWorksheet.cell(i, 5).string(report.val().subject).style(style);
                    assessmentsReportsWorksheet.cell(i, 6).string(report.val().assessmentType).style(style);

                    const durationOfUse = getTimeFromSeconds(report.val().timeTakenToComplete);
                    assessmentsReportsWorksheet.cell(i, 7).string(durationOfUse).style(style);

                    const score = `${report.val().totalScore}/${report.val().totalQuestions}`;
                    assessmentsReportsWorksheet.cell(i, 8).string(score).style(style);

                    assessmentsReportsWorksheet.cell(i, 9).number(+report.val().completionStatus).style(style);
                    i++;
                });
            });
        }

        // Get current date
        var date = new Date();
        var today = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();

        // Directory to save reports file temporarily
        var reportsFileDir = `${__dirname}/../temp`;

        // Fetch school name from the database
        const pathToSchoolDetails = `Tablab/Schools/${did}/School_Details/schoolName`;
        modelsClientObj.getter(pathToSchoolDetails, (schoolName) => {
            var file = reportsFileDir + `/Reports_${schoolName.val()}_${today}.xlsx`;
            try {
                // create directory to save reports file, if doesn't exists
                if (!fs.existsSync(reportsFileDir)) {
                    fs.mkdirSync(reportsFileDir);
                }

                // write data into the file
                workbook.write(file, (error, stats) => {
                    if (error) {
                        res.status(500).send(error);
                    }

                    res.download(file, (fileServeError) => {
                        // delete the newly created downloadable
                        if (fs.existsSync(file)) {
                            fs.unlinkSync(file);
                        }
                    });
                });
            } catch (dirDeletionError) {
                res.status(500).send(dirDeletionError);
            }
        });
    });
}

Client.downloadAssessmentReportsPerStudent = function (req, res) {
    var queryParameters = req.query;
    const studentID = queryParameters.studentID;
    const startDate = queryParameters.start;
    const endDate = queryParameters.end;
    const schoolId = queryParameters.did;
    const queryAssessmentType = queryParameters.type;


    // Create a new instance of a Workbook class
    var workbook = new excel.Workbook();

    // Add Worksheets to the workbook
    var assessmentsReportsWorksheet = workbook.addWorksheet(`${studentID.split('_').pop()}'s Assessment Reports`);

    // Create a reusable style
    var style = workbook.createStyle({
        font: {
            color: '#303133',
            size: 12
        }
    });

    // Create a reusable heading style
    var headingStyle = workbook.createStyle({
        font: {
            color: '#303133',
            size: 14
        }
    });

    const path = `${analyticsReference}/${schoolId}/${TYPE_ASSESSMENTS_USAGE}/appUsers`;
    modelsClientObj.getterWithQuery(path, `${startDate}`, `${endDate}`, NO_LIMIT_TO_LAST, (snap) => {
        assessmentsReportsWorksheet.cell(1, 1).string('Assessment Name').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 2).string('Assessment Topic').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 3).string('Type').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 4).string('Attempts').style(headingStyle);
        assessmentsReportsWorksheet.cell(1, 5).string('Average (%)').style(headingStyle);

        var cumulativeUsage = {};
        if (snap && snap.val()) {
            snap.forEach((childSnap) => {
                childSnap.forEach((report) => {
                    var usage = report.val();
                    const currentStudentID = (report.key).split(KEY_DELIMITER)[0];
                    const assessmentName = usage.assessmentName;
                    const assessmentType = usage.assessmentType;

                    if (currentStudentID === studentID) {
                        if (queryAssessmentType) {
                            if (queryAssessmentType === assessmentType) {
                                if (cumulativeUsage.hasOwnProperty(assessmentName)) { // Repeatitive current assessmentName
                                    cumulativeUsage[assessmentName].count += 1;
                                    cumulativeUsage[assessmentName].totalScore += +(usage.totalScore);
                                    cumulativeUsage[assessmentName].totalQuestions += +(usage.totalQuestions);

                                    if (!cumulativeUsage[assessmentName].hasOwnProperty('assessmentType'))
                                        cumulativeUsage[assessmentName].assessmentType = usage.assessmentType;
                                } else if (assessmentName) { // First time for current assessmentName
                                    cumulativeUsage[assessmentName] = {
                                        "count": 1,
                                        "totalScore": +(usage.totalScore),
                                        "totalQuestions": +(usage.totalQuestions),
                                        "assessmentType": usage.assessmentType
                                    }
                                }
                            }
                        } else {
                            if (cumulativeUsage.hasOwnProperty(assessmentName)) { // Repeatitive current assessmentName
                                cumulativeUsage[assessmentName].count += 1;
                                cumulativeUsage[assessmentName].totalScore += +(usage.totalScore);
                                cumulativeUsage[assessmentName].totalQuestions += +(usage.totalQuestions);

                                if (!cumulativeUsage[assessmentName].hasOwnProperty('assessmentType'))
                                    cumulativeUsage[assessmentName].assessmentType = usage.assessmentType;
                            } else if (assessmentName) { // First time for current assessmentName
                                cumulativeUsage[assessmentName] = {
                                    "count": 1,
                                    "totalScore": +(usage.totalScore),
                                    "totalQuestions": +(usage.totalQuestions),
                                    "assessmentType": usage.assessmentType
                                }
                            }
                        }
                    }
                })
            })

            // Create excel sheet
            var j = 2;
            for (var keyAssessmentName in cumulativeUsage) {
                if (cumulativeUsage.hasOwnProperty(keyAssessmentName)) {
                    const assesmentNameArray = keyAssessmentName.split('_');
                    const assessmentTopic = assesmentNameArray[2];
                    const assessmentName = assesmentNameArray[3];

                    assessmentsReportsWorksheet.cell(j, 1).string(assessmentName).style(style);
                    assessmentsReportsWorksheet.cell(j, 2).string(assessmentTopic).style(style);
                    assessmentsReportsWorksheet.cell(j, 3).string(cumulativeUsage[keyAssessmentName].assessmentType).style(style);
                    assessmentsReportsWorksheet.cell(j, 4).number(cumulativeUsage[keyAssessmentName].count).style(style);

                    const avg = 100 * (cumulativeUsage[keyAssessmentName].totalScore / cumulativeUsage[keyAssessmentName].totalQuestions);
                    assessmentsReportsWorksheet.cell(j, 5).number(avg).style(style);

                    j++;
                }
            }
        }

        // Get current date
        var date = new Date();
        var today = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();

        // Directory to save reports file temporarily
        var reportsFileDir = `${__dirname}/../temp`;

        // Fetch school name from the database
        const pathToSchoolDetails = `Tablab/Schools/${schoolId}/School_Details/schoolName`;
        modelsClientObj.getter(pathToSchoolDetails, (schoolName) => {
            var file = reportsFileDir + `/Reports_${schoolName.val()}_${today}.xlsx`;
            try {
                // create directory to save reports file, if doesn't exists
                if (!fs.existsSync(reportsFileDir)) {
                    fs.mkdirSync(reportsFileDir);
                }

                // write data into the file
                workbook.write(file, (error, stats) => {
                    if (error) {
                        res.status(500).send(error);
                    }

                    res.download(file, (fileServeError) => {
                        // delete the newly created downloadable
                        if (fs.existsSync(file)) {
                            fs.unlinkSync(file);
                        }
                    });
                });
            } catch (dirDeletionError) {
                res.status(500).send(dirDeletionError);
            }
        });
    })
}
// ------------------------------ DOWNLOAD [END] ------------------------------ //

Client.setPassword = function (req, res) {
    var urlParts = url.parse(req.url, true);
    var uid = urlParts.query.user;

    if (uid) {
        modelsClientObj.checkRegisterStatus(uid, function (email) {
            if (email)
                res.render("setPassword.ejs", {
                    email: email,
                    uid: uid,
                    redirect: "/reports"
                });
            else
                res.send("Link Expired");
        })
    } else {
        res.send("Link Expired");
    }
}

/** 
 * for updating user password.
 * send confirm mail to the user
 */

Client.activateUser = function (request, response) {
    var user = request.body;    
    if (user.uid && user.email) {
        modelsClientObj.sendVerification(user.uid, user.password, function (error, data) {
            console.log('error', error);
            if (error) {
                response.status(400).send(JSON.stringify({
                    'error': error
                }));
                return;
            }

            var mailOptions = {
                // from: 'idreameducation.tech@zoho.com',
                to: [{
                    "email": user.email
                }],
                subject: 'Password Successfully Set ✔',
                text: ''
            };
            mailOptions.html = setPasswordMailBodyFor(user.email);
            sendMail(mailOptions, (error, info) => {
                if (error) {
                    response.status(400);
                    response.send(JSON.stringify({
                        'error': error
                    }));
                    return console.log(error);
                }
                response.status(200);
                response.send();
            });
        })
    } else {
        response.status(400).send(JSON.stringify({
            'error': 'Details missing'
        }));
    }
}

/**
 * send mail to the user
 */
function sendMail(mailOptions, done) {
    var options = {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'api-key': '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'    
        },
        method: 'POST',
        url: 'https://api.sendinblue.com/v3/smtp/email',
        body: {
            tags: ['1'],
            sender: {
                name: 'iDream Education',
                email: 'p@idreameducation.org'
            },
            to: mailOptions.to,             //  [{ email: mailOptions.to }]
            htmlContent: mailOptions.html,
            subject: mailOptions.subject,
            replyTo: {
                email: 'p@idreameducation.org',
                name: 'Puneet'
            }
        },
        json: true
    };

    request(options, function (error, response, body) {
        if (error) {
            done(error);
        } else {
            done(null, body);
        }
    });
}

// function sendMail(mailOptions, done) {
//     let transporter = nodeMailer.createTransport({
//         service: 'Zoho', // 'gmail',
//         auth: {
//             user: "idreameducation.tech@zoho.com",
//             pass: "idream12345"
//         }
//     });

//     // send mail with defined transport object
//     transporter.sendMail(mailOptions, (error, info) => {
//         done(error, info);
//     });
// }

/**
 * make email body for user. 
 */
function setPasswordMailBodyFor(username) {
    var bodyContent = "Hi " + username + "<br>";
    bodyContent += "Your password has been successfully set. Visit <br/> <a href = 'www.mytablab.com/reports'>iDream Web Backend</a>";
    return bodyContent;
}

/**
 * 
 * @param {string} seconds
 * @returns {string} hours
 */
function convertSecondsIntoHours(seconds) {
    return ((+seconds) / 3600).toFixed(2);
}

function getTimeFromSeconds(seconds) {
    seconds = Number(seconds);
    const ss = Math.floor(seconds % 60);
    const mm = Math.floor((seconds / 60) % 60);
    const hh = Math.floor(seconds / (60 * 60));
    return (("00" + hh.toString()).slice(-2) + ':' + ("00" + mm.toString()).slice(-2) + ':' + ("00" + ss.toString()).slice(-2));
}

/**
 * Extracts keys and values 
 * 
 * @param {Object} JSONObject (cumulatice analytics object) 
 * @param {Strng[]} nameList 
 * @param {Strng[]} timeList
 */
function convertToArray(JSONObject, nameList, timeList) {
    for (x in JSONObject) {
        nameList.push(x);
        timeList.push((JSONObject[x].toFixed(2)));
    }
}

/**
 * 
 * @param {String} str 
 * @returns {String} str
 */
function toTitleCase(str) {
    return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
}

module.exports = Client;