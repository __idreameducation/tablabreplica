function editProjectDetails(pid) {  // pid is the ID of the selected project

    $('#edit-form')[0].reset();

    // var loginHandler = new Handler();
    // const functions = new Functions();
    // const uid = $('#uid').val();

    // var iDreamCoordinators;
    // var partnerCoordinators;
    var projectDetails;
    
    // // request for idream coordinators data
    // functions.sendRequest('/reports/admin/getUsers?type=idreamcoordinator', 'GET', null, (error, data) => {
    //     iDreamCoordinators = data;

    //     // request for partner coordinators data    
    //     functions.sendRequest('/reports/admin/getUsers?type=partnercoordinator', 'GET', null, (error, data) => {
    //         partnerCoordinators = data;

            functions.sendRequest(`getProjectDetails?pid=${pid}`, 'GET', null, (error, data) => {
                projectDetails = data;

                // // Set iDreamCoordinators as options
                // for (var key in iDreamCoordinators) {
                //     var $option1 = $(`<option id=${key}>${iDreamCoordinators[key].name} (${iDreamCoordinators[key].email})</option>`);
                //     $('#assignIdreamCoordinator').append($option1);
                //     // var $option2 = $(`<option id=${key}>${iDreamCoordinators[key].name} (${iDreamCoordinators[key].email})</option>`);
                //     // $('#createOnlineProject #assignIdreamCoordinator').append($option2);
                // }
            
                // // Set partnerCoordinators as options
                // for (var key in partnerCoordinators) {
                //     var $option1 = $(`<option id=${key}>${partnerCoordinators[key].name} (${partnerCoordinators[key].email})</option>`);
                //     $('#assignPartnerCoordinator').append($option1);
                // }

                $('#edit-form #inputProjectName').val(data.projectName);
                $('#edit-form #pLocation').val(data.location.area);
        
                $('#edit-form #inputImplementationPartner').val(data.implementationPartnerName?data.implementationPartnerName:'N/A');
                $('#edit-form #inputProjectFunder').val(data.fundingPartnerName?data.fundingPartnerName:'N/A');
                $('#edit-form #inputCommencementDate').val(data.commencementDate);
                $('#edit-form #inputExpiryDate').val(data.expiryDate);
                $('#edit-form #tSchools').val(data.totalNumberOfSchools);
                $('#edit-form #fromGrade').val(data.fromGrade);
                $('#edit-form #toGrade').val(data.toGrade);
                $('#edit-form #tTablets').val(data.totalNumberOfTablets);
                $('#edit-form #tTrolleys').val(data.totalNumberOfTrolleys);
        
                $('#edit-form #pLocationState').val(data.location.state);
                $('#edit-form #selectLanguage').val(data.contentLanguage);
                
               /*  var iDreamCoordinatorID = (data.iDreamCoordinators) ? data.iDreamCoordinators["0"].id : "";
                $(`#edit-form #assignIdreamCoordinator option[id="${iDreamCoordinatorID}"]`).prop('selected', true);
                    console.log(data.partnerCoordinators) */
              
				
				for (var i= 0; i<data.partnerCoordinators.length; i++){
					  var partnerCoordinatorID = (data.partnerCoordinators) ? data.partnerCoordinators[i].id : "";
                $(`#edit-form #assignPartnerCoordinator option[id="${partnerCoordinatorID}"]`).prop('selected', true);
				}
				
				
				
				
            });
    //     });
    // });

    // set states options
    states.forEach((state, i) => {
        $('#edit-form #pLocationState').append($(`<option id="${i+1}">${state.name}</option>`))
    });

    // set content language options
    languages.forEach((language, i) => {
        $('#edit-form #selectLanguage').append($(`<option id="${i+1}">${language}</option>`));
    });
	
// select and deselect 	multiple input without ctrl
$("#assignPartnerCoordinator option").mousedown(function(e){
																		e.preventDefault();
																			var select = this;
																			var scroll = select.scrollTop;
																			
																			e.target.selected = !e.target.selected;
																			
																			setTimeout(function(){select.scrollTop = scroll;}, 0);
																			
																			$(select).focus();
					}).mousemove(function(e){e.preventDefault()});
    
    // Submit Button click listener
    $('#submit-editProjectDetails').on('click', (e) => {
        e.preventDefault();
        
        projectDetails.projectName = $('#edit-form #inputProjectName').val();
        projectDetails.location.area = $('#edit-form #pLocation').val();
        projectDetails.location.state = $('#edit-form #pLocationState').val();
        projectDetails.implementationPartnerName = ($('#edit-form #inputImplementationPartner').val() !== "N/A")? $('#edit-form #inputImplementationPartner').val() : "";
        projectDetails.fundingPartnerName = ($('#edit-form #inputProjectFunder').val() !== "N/A")? $('#edit-form #inputProjectFunder').val() : "";

        var newCommencementDate = Date.parse($('#edit-form #inputCommencementDate').val());
        var newExpiryDate = Date.parse($('#edit-form #inputExpiryDate').val());
        if (newCommencementDate < newExpiryDate) {
            projectDetails.commencementDate = $('#edit-form #inputCommencementDate').val();
            projectDetails.expiryDate = $('#edit-form #inputExpiryDate').val();
        } else {
            alert("Commencement Date should be smaller than the expiry date!");
            return;
        }

        if($('#edit-form #tSchools').val() < projectDetails.totalNumberOfSchools) {
            alert("New number for total number of schools cannot be less than the old!");
            return;
        }
        projectDetails.totalNumberOfSchools = $('#edit-form #tSchools').val();

        // Content Language
        projectDetails.contentLanguage = $('#edit-form #selectLanguage').val();

        // Grades/Classes information
        if (!$('#edit-form #fromGrade').val()) {
            alert('Enter Class!');
            $('#edit-form #fromGrade').focus();
            return;
        }
        const fromGrade = $('#edit-form #fromGrade').val();

        if (!$('#edit-form #toGrade').val()) {
            alert('Enter Class!');
            $('#edit-form #toGrade').focus();
            return;
        }
        const toGrade = $('#edit-form #toGrade').val();

        if (+fromGrade < 1 || +fromGrade > 11) {
            alert('First entry in classes should be greater than or equal 1 and smaller than or equal to 11.');
            $('#edit-form #fromGrade').focus();
            return;
        }
        if (+toGrade < 2 || +toGrade > 12) {
            alert('Second entry in classes should be greater than or equal to 2 and smaller than or equal to 12');
            $('#edit-form #toGrade').focus();
            return;
        }
        if (+fromGrade >= +toGrade) {
            alert('First entry in classes should be smaller than the second entry!');
            $('#edit-form #fromGrade').focus();
            return;
        }
        projectDetails.fromGrade = fromGrade;
        projectDetails.toGrade = toGrade;

        if($('#edit-form #tTablets').val() < projectDetails.totalNumberOfTablets) {
            alert('New value cannot be smaller than the old value!');
            $('#edit-form #tTablets').focus();
            return;
        }
        projectDetails.totalNumberOfTablets = $('#edit-form #tTablets').val();

        if($('#edit-form #tTrolleys').val() < projectDetails.totalNumberOfTrolleys) {
            alert('New value cannot be smaller than the old value!');
            $('#edit-form #tTrolleys').focus();
            return;
        }
        projectDetails.totalNumberOfTrolleys = $('#edit-form #tTrolleys').val();

        var oldIdreamCoordinatorId = projectDetails.iDreamCoordinators["0"].id;
        var newIdreamCoordinatorId = $($('#edit-form #assignIdreamCoordinator').find(':selected')[0]).prop('id');
        projectDetails.iDreamCoordinators["0"].id = (newIdreamCoordinatorId) ? newIdreamCoordinatorId : "";
        
        var oldPartnerCoordinatorId = (projectDetails.partnerCoordinators) ? projectDetails.partnerCoordinators["0"].id : "";
        var newPartnerCoordinatorId = $($('#edit-form #assignPartnerCoordinator').find(':selected')[0]).prop('id');
		 
		  var partnerCoordinatorIds = [];
		  var newPartnerCoordinators =[];
		  $($('#edit-form #assignPartnerCoordinator').find(':selected')).each(function(){ if(this.id)partnerCoordinatorIds.push(this.id)});
		console.log("selected" ,partnerCoordinatorIds);
		//projectDetails.partnerCoordinators.
		   
		    for(let i =0; i<partnerCoordinatorIds.length;  i++){
			   let flag =0;
			   if(projectDetails.partnerCoordinators){
			   for(let j =0; j<projectDetails.partnerCoordinators.length;  j++){
				   
				   if(projectDetails.partnerCoordinators[j].id === partnerCoordinatorIds[i] ){
					   console.log("matched", i , j);
					   flag =1;
					   break;
				   }
			   
			   }}
				 if(flag===0){
					 newPartnerCoordinators.push(partnerCoordinatorIds[i]);
				 }
		   } 
		
		
		
		
		 /* const keys =projectDetails.partnerCoordinators;
		console.log("old" ,projectDetails.partnerCoordinators);
		if(projectDetails.partnerCoordinators){
	     var newPartnerCoordinators = partnerCoordinatorIds.map((partner)=>{
			 console.log(partner)
			 console.log("index", projectDetails.partnerCoordinators.indexOf(partner));
			if(projectDetails.partnerCoordinators.indexOf(partner)=== -1){
				return partner;
			}
		 }) */
			
		//}
		
		console.log("new",newPartnerCoordinators);
		
       /*  if(newPartnerCoordinatorId && !projectDetails.partnerCoordinators) {
            projectDetails.partnerCoordinators = [];
            projectDetails.partnerCoordinators.push({id  : newPartnerCoordinatorId ? newPartnerCoordinatorId : ""});            
        } else if(newPartnerCoordinatorId)
            projectDetails.partnerCoordinators["0"].id = newPartnerCoordinatorId ? newPartnerCoordinatorId : ""; */
		
		if(newPartnerCoordinatorId && !projectDetails.partnerCoordinators) {
            projectDetails.partnerCoordinators = [];
			partnerCoordinatorIds.forEach(function(entry) {
			  
                    projectDetails.partnerCoordinators.push({id  : entry });
					});
            //projectDetails.partnerCoordinators.push({id  : newPartnerCoordinatorId ? newPartnerCoordinatorId : ""});            
        } else if(newPartnerCoordinatorId) {
			projectDetails.partnerCoordinators =[]
			       partnerCoordinatorIds.forEach(function(entry) {
                    projectDetails.partnerCoordinators.push({id  : entry });
					});
			
		}
		loginHandler.setter(`Tablab/Projects/${pid}/Project_Details`, projectDetails, (error) => {
            // save project under Admin's account
            loginHandler.setter(`users/${uid}/projects/${pid}`, { 'projectName': projectDetails.projectName, 'location': projectDetails.location }, (error) => {});

            // save project under assigned iDreamCoordinator's account
            loginHandler.setter(`users/${newIdreamCoordinatorId}/projects/${pid}`, { 'projectName': projectDetails.projectName, 'location': projectDetails.location }, (error) => {});

            // save project under assigned partnerCoordinator's account
          //  if (newPartnerCoordinatorId) {
              //  loginHandler.setter(`users/${newPartnerCoordinatorId}/projects/${pid}`, { 'projectName': projectDetails.projectName, 'location': projectDetails.location }, (error) => {});
               /*  if(newPartnerCoordinatorId !== oldPartnerCoordinatorId) {
                    // Request server to send mail to assigned coordinator
                    functions.sendRequest('/reports/admin/mailForProjectAssignment', 'POST', { 'email': partnerCoordinators[newPartnerCoordinatorId].email, 'username': partnerCoordinators[newPartnerCoordinatorId].name, 'projectName': projectDetails.projectName }, (response) => {
                        console.log(response);
                    });
                } */
            //}
			if(partnerCoordinatorIds.length !=0){
						  partnerCoordinatorIds.forEach(function(entry) {
				   var newPartnerCoordinatorId =entry
                   loginHandler.setter(`users/${newPartnerCoordinatorId}/projects/${pid}`, { 'projectName': projectDetails.projectName, 'location': projectDetails.location }, (error) => {});
                            
					}); 
		}
		if(newPartnerCoordinators){
			 functions.sendRequest('/reports/admin/getUsers?type=partnercoordinator', 'GET', null, (error, data) => {
            var  partnerCoordinators = data;
			newPartnerCoordinators.forEach((user)=>{
				
				
            mailOptions = {
                    'from': '',
                    'to': [partnerCoordinators[user]], // .email,
                    'subject': 'Project assignment and procurment details',
                    'html': `Dear ${partnerCoordinators[user].name}
                            <br><br>
                            You have been assigned to a new project. Please find the details below, 
                            <br><br>
                            <b>Project name:</b> ${projectDetails.projectName}
                            <br><br><br>
                            Thanks.
                            <br>
                            Regards<br>
                            iDream Operations`
                }
              
            // Request browser to send mail to assigned coordinator
            functions.sendRequest('mailRequest', 'POST', mailOptions, (response) => {
                console.log(response);
						});
					});
			 })	
		}
		
            if(oldIdreamCoordinatorId !== projectDetails.iDreamCoordinators["0"].id) {
                // Request server to send mail to assigned coordinator
                functions.sendRequest('/reports/admin/mailForProjectAssignment', 'POST', { 'email': iDreamCoordinators[newIdreamCoordinatorId].email, 'username': iDreamCoordinators[newIdreamCoordinatorId].name, 'projectName': projectDetails.projectName }, (response) => {
                    console.log(response);
                });
            }
			// when editProjectDetails functions called ,it appends the click event on the submit button. so we used unbind method to remove the click event on  submit button
			
			$("#submit-editProjectDetails").unbind();
			
			
			
            $('#editProjectDetailsModal').modal('hide');
            // window.location.reload();
        });
    });
}