$(function () {
    // instance Functions object from functions.js
    const functions = new Functions();
    const loginHandler = new Handler();
    const databaseRef = firebase.database().ref().child("newDatabase");

    // initialize objects for html elements
    const $studentRecordsTableBody = $('#studentRecords');

    //    var studentInfo = [];
    const schoolID = $('#did').val();
    var newStudents = [];
    var oldStudentsRecords = {};
    var sameNameCount = 0;
    var isModalClosed = false;
    var localSameNameCount = {};

    $('#btnCloseModal').on('click', (e) => {
        newStudents = [];
        isModalClosed = true;
        $('#uploadCSV').val('');
    });

    $('#uploadCSV').on('change', (e) => {
        isModalClosed = false;
        newStudents = [];

        $('#modalDiv').find('#tbNewStudentsFromFile').html('');
        $('#newStudentsLoader').css('display', 'block');
        $('#buttonAddAll').prop('disabled', true);
        $('#modalDiv').modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#modalDiv').modal('show');

        const file = e.target.files[0];
        var fileType = file["type"];
        var ValidCSVTypes = ["text/csv", "application/vnd.ms-excel"];
        if ($.inArray(fileType, ValidCSVTypes) < 0) {
            // invalid file type code goes here.
            alert('Not a CSV file!');
            $('#uploadCSV').val('');
            $('#modalDiv').modal('hide');
            return;
        }

        var fr = new FileReader();
        fr.onload = function (event) {
            var content = event.target.result;
            var x = content.trim().split('\n');
            newStudentsRecordUploaded(x, 1);
        }
        fr.readAsText(file);
    });


    function newStudentsRecordUploaded(uploadedStudentsRecords, i) {
        if (isModalClosed) {
            $('#newStudentsLoader').css('display', 'none');
            return;
        }

        if (i >= uploadedStudentsRecords.length) {
            $('#newStudentsLoader').css('display', 'none');
            $('#buttonAddAll').prop('disabled', false);
            alert('See the table below to review details of the students from the file!');
            return;
        }

        var values = uploadedStudentsRecords[i].split(',');

        // if learner's first contains space
        var learnerFirstName = values[0].split(' ');    // split first name
        values[0] = learnerFirstName[0];
        values[1] = (learnerFirstName[1]) ? learnerFirstName[1] + ' ' + values[1] : values[1];

        if(localSameNameCount[values[0].toLowerCase()] >= 0) {
            var count = ++localSameNameCount[values[0].toLowerCase()];
            const username = values[0].toLowerCase() + count;
            const studentID = schoolID + '_' + values[2] + '_' + username;
            const newStudentInfo = {
                'studentName': (values[1]) ? (values[0] + ' ' + values[1]) : values[0],
                'studentClass': values[2],
                'email': values[3],
                'contactNumber': values[4],
                'username': username,
                'studentPassword': username,
                'count': count,
                'activeStatus': true
            }
            newStudents.push(newStudentInfo);

            // Add student record to the table
            addRowsToTable(newStudentInfo, studentID, $('#tbNewStudentsFromFile'), true);

            // Next iteration
            newStudentsRecordUploaded(uploadedStudentsRecords, i + 1);
        } else {
            // get Count for current username
            loginHandler.getter(`Tablab/Schools/${schoolID}/appUsers/sameNameCount/${values[0].toLowerCase()}`, (data) => {
                var count = 0;
                if (data && data.val()) {
                    count = +data.val();
                }

                localSameNameCount[values[0].toLowerCase()] = count;

                const username = (count !== 0) ? values[0].toLowerCase() + count : values[0].toLowerCase();
                const studentID = schoolID + '_' + values[2] + '_' + username;
                const newStudentInfo = {
                    'studentName': (values[1]) ? (values[0] + ' ' + values[1]) : values[0],
                    'studentClass': values[2],
                    'email': values[3],
                    'contactNumber': values[4],
                    'username': username,
                    'studentPassword': username,
                    'count': count,
                    'activeStatus': true
                }
                newStudents.push(newStudentInfo);

                // Add student record to the table
                addRowsToTable(newStudentInfo, studentID, $('#tbNewStudentsFromFile'), true);

                // Next iteration
                newStudentsRecordUploaded(uploadedStudentsRecords, i + 1);
            });
        }        
    }

    // Add All students to database
    $('#buttonAddAll').on('click', (e) => {
        // const studentsDetailsRows = $('#tbNewStudentsFromFile').find('tr');
        $('#buttonAddAll').prop('disabled', true);
        $('#newStudentsLoader').css('display', 'block');
        addNewStudentsToDatabase(newStudents, 0);
    });

    function addNewStudentsToDatabase(newStudents, index) {
        if (index >= newStudents.length) {
            alert('Students created successfully!');
            $('#modalDiv').modal('hide');
            $('#uploadCSV').val('');
            return;
        }

        var studentInfo = newStudents[index];
        const studentFirstName = studentInfo.studentName.split(' ')[0];
        const data = {
            'studentInfo': studentInfo,
            'schoolID': schoolID
        }

        functions.sendRequest(`/reports/addStudents`, 'POST', data, (response) => {
            if (response.error) {
                // alert(response.error);
            } else {
                loginHandler.setter(`Tablab/Schools/${schoolID}/appUsers/sameNameCount/${studentFirstName.toLowerCase()}`, studentInfo.count + 1, () => {
                    $('#form')[0].reset();
                    addNewStudentsToDatabase(newStudents, index + 1);
                    // add newly added student to the list
                    const studentID = schoolID + '_' + studentInfo.studentClass + '_' + studentInfo.username;
                    addRowsToTable(studentInfo, studentID, $studentRecordsTableBody);
                });
            }
        });
    }

    // Get records of the students who are already registered
    getStudentRecords(schoolID, (error, responseText) => {
        if (error) {
            alert(error.message);
        } else {
            oldStudentsRecords = responseText.studentRecords;
            showStudentRecords(oldStudentsRecords);
        }
    });

    $('#studentFirstName').on('keypress', (e) => {
        $($('#studentFirstName').parent()).find('span').css('display', 'none');
    });

    $('#studentFirstName').blur((e) => {
        var studentFirstName = $('#studentFirstName').val();
        if (!studentFirstName) {
            return;
        }

        $('#addStudentLoader').css('display', 'block');

        // get Count for current username
        loginHandler.getter(`Tablab/Schools/${schoolID}/appUsers/sameNameCount/${studentFirstName.toLowerCase()}`, (data) => {
            if (data && data.val()) {
                sameNameCount = +data.val();
            }
            var username = (sameNameCount !== 0) ? studentFirstName.toLowerCase() + sameNameCount : studentFirstName.toLowerCase();
            $('#inputUsername').val(username);
            $('#inputPassword').val(username);
            $('#addStudentLoader').css('display', 'none');
        });
    });

    $('#studentClass').on('change', (e) => {
        $($('#studentClass').parent()).find('span').css('display', 'none');
    });

    $('#btnAddStudent').on('click', (e) => {
        e.preventDefault();

        if (!$('#studentFirstName').val()) {
            $($('#studentFirstName').parent()).find('span').css('display', 'block');
            $('#studentFirstName').focus();
            return;
        }
        const studentFirstName = $('#studentFirstName').val();

        if (!$('#studentClass').val()) {
            $($('#studentClass').parent()).find('span').css('display', 'block');
            $('#studentClass').focus();
            return;
        }

        if ($('#inputEmail').val() &&
            !emailRegex.test($('#inputEmail').val())) {
            alert('Invalid email address!');
            return;
        }

        if ($('#inputContactNumber').val() &&
            !contactValidationRegex.test($('#inputContactNumber').val())) {
            alert('Invalid contact number!');
            return;
        }

        var studentInfo = {
            'studentName': '',
            'studentClass': $('#studentClass').val(),
            'email': $('#inputEmail').val(),
            'contactNumber': $('#inputContactNumber').val(),
            'username': $('#inputUsername').val(),
            'studentPassword': $('#inputPassword').val(),
            'activeStatus': true
        }

        studentInfo.studentName = $('#studentLastName').val() ?
            studentFirstName + ' ' + $('#studentLastName').val() :
            studentFirstName;

        $('#addStudentLoader').css('display', 'block');
        const data = {
            'studentInfo': studentInfo,
            'schoolID': schoolID
        }

        functions.sendRequest(`/reports/addStudents`, 'POST', data, (response) => {
            if (response.error) {
                alert(response.error);
            } else {
                loginHandler.setter(`Tablab/Schools/${schoolID}/appUsers/sameNameCount/${studentFirstName.toLowerCase()}`, sameNameCount + 1, () => {
                    alert('Student added successfully');
                    $('#form')[0].reset();
                    $('#addStudentLoader').css('display', 'none');
                    sameNameCount = 0;

                    // add newly added student to the list
                    const studentID = schoolID + '_' + studentInfo.studentClass + '_' + studentInfo.username;
                    addRowsToTable(studentInfo, studentID, $studentRecordsTableBody);
                });
            }
        });
    })

    function getStudentRecords(did, done) {
        const url = `/reports/getStudentRecords?did=${did}`;
        functions.sendRequest(url, "GET", null, (error, responseText) => {
            done(error, responseText);
        });
    }

    function showStudentRecords(studentRecords) {
        for (key in studentRecords) {
            if (studentRecords.hasOwnProperty(key)) {
                // length++;
                const element = studentRecords[key];
                addRowsToTable(element, key, $studentRecordsTableBody);
            }
        }
    }

    /**
     * 
     * @param {Object} obj 
     * @param {String} key 
     * @param {HTML Element} $studentRecordsTableBody
     * @param {Boolean} isNoUpdate
     */
    function addRowsToTable(obj, key, $studentRecordsTableBody, isNoUpdate) {
        if(!obj.activeStatus) { // do not add record into the table for inactive student
            return;
        }

        // show info in the table 
        var $row = $(`<tr id="${key}"></tr>`);
        var $name_td = $(`<td><input type="text" class="form-control" style="text-align:center;" value="${obj.studentName}" disabled /></td>`);
        var $class_td = $(`<td><input type="text" class="form-control" style="text-align:center;" value="${obj.studentClass}" disabled /></td>`);
        var $username_td = $(`<td><span class="form-control" style="background-color: #eee; opacity: 1;">${obj.username} </span> </td>`);
        var $password_td = $(`<td><input type="text" class="form-control" style="text-align:center;" value="${obj.studentPassword}" disabled /></td>`);

        const email = (obj.email) ? obj.email : '';
        var $email_td = $(`<td><input type="text" class="form-control" style="text-align:center;" value="${email}" disabled /></td>`); //.text((obj.email) ? obj.email : '-');

        const contact = (obj.contactNumber) ? obj.contactNumber : '';
        var $contact_td = $(`<td><input type="text" class="form-control" style="text-align:center;" value="${contact}" disabled /></td>`); // .text((obj.contact) ? obj.contact : '-');

        var $activeStatus = $(`<td><select class="form-control" disabled>
                                    <option id=false>Inactive</option>
                                    <option id=true selected>Active</option>
                                </select></td>`);

        if (!obj.activeStatus) {
            $activeStatus.find('select option[id=false]').prop('selected', true);
        }

        var $edit = $(`<td class=${isNoUpdate ? "noUpdate": ""} style="backgroung-color:#000"><i class="edit fa fa-pencil" area-hidden="true"></i></td>`)
            .on('click', (e) => {
                if (($(e.currentTarget).find('.edit')).length > 0) { // Edit
                    $(e.currentTarget).find('.edit').removeClass('edit fa-pencil').addClass('save fa-save');
                    $(e.currentTarget).parent().find('input, select').prop("disabled", false);
                } else { // Save
                    $(e.currentTarget).find('.save').removeClass('save fa-save').addClass('edit fa-pencil');
                    $(e.currentTarget).parent().find('input, select').prop("disabled", true);

                    if(!$(e.currentTarget).hasClass("noUpdate")) {
                            // update details in the database
                        updateUserDetails($(e.currentTarget).parent());
                    }
                }
            });

        var $buttonDisable = $('<td><i class="fa fa-trash" aria-hidden="true"></i></td>')
            .on('click', (e) => {
                e.preventDefault();

                if(confirm("Delete this student?")) {
                    var studentID = $($(e.currentTarget).parent()).prop('id');
                    loginHandler.setter(`Tablab/Schools/${schoolID}/appUsers/userDetails/${studentID}/activeStatus`, false, (error) => {
                        $($(e.currentTarget).parent()).remove();
                    });
                }
            });

        // $sr_td.appendTo($row);
        $row.append($name_td);
        $row.append($class_td);
        $row.append($username_td);
        $row.append($password_td);
        $row.append($email_td);
        $row.append($contact_td);
        // $row.append($activeStatus);
        $row.append($edit);
        $row.append($buttonDisable);

        $row.appendTo($studentRecordsTableBody);
    }

    // update details in the database
    function updateUserDetails($selectedStudentRow) {
        var $selectedStudentRowInputs = $selectedStudentRow.find('input');
        
        // var $selectedStudentRowSelect = $selectedStudentRow.find('select :selected');

        var updatedDetails = {
            "activeStatus": true, // $selectedStudentRowSelect[0].id,
            "contactNumber": $($selectedStudentRowInputs[4]).val(),
            "email": $($selectedStudentRowInputs[3]).val(),
            "studentName": $($selectedStudentRowInputs[0]).val(),
            "studentClass": $($selectedStudentRowInputs[1]).val(),
            "username": $($selectedStudentRow.find('td span')[0]).text(),
            "studentPassword": $($selectedStudentRowInputs[2]).val(),
        }

        var studentID = $($selectedStudentRow).prop('id');
        loginHandler.setter(`Tablab/Schools/${schoolID}/appUsers/userDetails/${studentID}`, updatedDetails, (error) => {
            console.log('error >> ', error);
        });
    }
})