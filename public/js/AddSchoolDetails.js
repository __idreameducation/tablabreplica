var loginHandler = new Handler();
const functions = new Functions();
var classesWithDetails = {};
var classesWithDetails2 = {};
var subjectNames = {};
var subjectNames2 = {};
var schoolManagers = {};
var contentJSONArray = {};
var extracurricularContentJSONArray = {};
var iDreamCoordinators = [],
    partnerCoordinators = [];

const username = $('#username').val();
const uid = $('#uid').val();
const pid = $('#pid').val();

$(document).ready(function () {
    var screenTexts = {};

    $('[data-toggle="popover"]').popover();

    // get schools under the project
    functions.sendRequest(`getSchoolsUnderProject?pid=${pid}`, 'GET', null, (error, data) => {
        $('#loader1').css('display', 'none');
        var schools = data;
        for (var key in schools) {
            var schoolDetails = schools[key];
            var options = {
                'id': key,
                'uid': uid,
                'username': username,
                'name': schoolDetails.schoolName,
                'district': schoolDetails.location.district,
                'state': schoolDetails.location.state
            }
            var $row = createListsElement(options);
            $('#schoolsList').append($row);
			 var $option = $(`<option id="${options.uid}" value="${options.id}">${options.name} </option>`);
			 
            // if(!schoolManagers[key].active) <i class="fa fa-exclamation-triangle"></i>
            $('#schoolList').append($option);
        }
    });

    // get school manager
    functions.sendRequest('getUsers?type=schoolmanager', 'GET', null, (error, data) => {
        schoolManagers = data;

        for (var key in schoolManagers) {
            var $option = $(`<option id="${key}" value="${key}">${schoolManagers[key].name} (${schoolManagers[key].email})</option>`);
            // if(!schoolManagers[key].active) <i class="fa fa-exclamation-triangle"></i>
            $('#schoolManager').append($option);
        }
    });

    functions.sendRequest(`getAssignedCoordinators?pid=${pid}&typeOfCoordinator=iDreamCoordinators`, 'GET', null, (error, data) => {
        // get coordinator details using their IDs
        for (var index in data) {
            functions.sendRequest(`getUserDetails?uid=${data[index].id}`, 'GET', null, (e, d) => {
                iDreamCoordinators.push(d);
            });
        }
    });

    functions.sendRequest(`getAssignedCoordinators?pid=${pid}&typeOfCoordinator=partnerCoordinators`, 'GET', null, (error, data) => {
        // get coordinator details using their IDs
        for (var index in data) {
            functions.sendRequest(`getUserDetails?uid=${data[index].id}`, 'GET', null, (e, d) => {
                if(d) {
                    partnerCoordinators.push(d);
                }
            });
        }
    });

    $('form').on('keypress', (e) => {
        if (e.keyCode === 13) {
            e.preventDefault();
            // $(e.target).next('input').focus();
        }
    })

    $('#form #buttonSubmit').click((event) => {
        event.preventDefault();

        var schoolDetails = {};
        var appIDs = {};
        var appData = {
            "contentJSON": [],
            "settings": {
                "go_to_password": {
                    'value': ''
                },
                "loginMode": {
                    'value': ''
                },
                "tablabType": {
                    'value': ''
                },
                "classes": {
                    'value': ''
                },
                "contentCategories": []
            },
            "extraCurricularContent": []
        };

        // var schoolID = $('#mainDiv #inputSchoolID').val();
        schoolDetails.schoolName = '';
        schoolDetails.ProjectID = pid;
        schoolDetails.implementationPartnerName = '';
        schoolDetails.fundingPartnerName = '';
        schoolDetails.commencementDate = '';
        schoolDetails.expiryDate = '';
        schoolDetails.totalNumberOfTablets = 0;
        schoolDetails.totalNumberOfTrolleys = $('#mainDiv #tTrolleys').val();
        schoolDetails.board = '';
        schoolDetails.contentLanguage = '';
        schoolDetails.location = {};

        /*  --- [START] Check Conditions ---*/
        // Check if School name is empty
        if (!$('#mainDiv #inputSchoolName').val() || $('#mainDiv #inputSchoolName').val() === '') {
            alert('School Name is necessary!');
            $('#mainDiv #inputSchoolName').focus();
            return;
        }
        schoolDetails.schoolName = $('#mainDiv #inputSchoolName').val();

        // address as text [area]
        if (!$('#mainDiv #sLocation').val()) {
            alert('Provide School Address!');
            $('#mainDiv #sLocation').focus();
            return;
        }
        schoolDetails.location.area = $('#mainDiv #sLocation').val();

        // state
        if (!$($('#mainDiv #sLocationState').find(':selected')[0]).prop('id')) {
            alert('Please Select State!');
            $('#mainDiv #sLocationState').focus();
            return;
        }
        schoolDetails.location.state = $($('#mainDiv #sLocationState').find(':selected')[0]).val();

        // district
        if (!$($('#mainDiv #sLocationDistt').find(':selected')[0]).prop('id')) {
            alert('Please Select District!');
            $('#mainDiv #sLocationDistt').focus();
            return;
        }
        schoolDetails.location.district = $($('#mainDiv #sLocationDistt').find(':selected')[0]).val();

        if (!$('#mainDiv #fromGrade').val()) {
            alert('Enter Class!');
            $('#mainDiv #fromGrade').focus();
            return;
        }
        const fromGrade = $('#mainDiv #fromGrade').val();

        if (!$('#mainDiv #toGrade').val()) {
            alert('Enter Class!');
            $('#mainDiv #toGrade').focus();
            return;
        }
        const toGrade = $('#mainDiv #toGrade').val();

        if (+fromGrade < 1 || +fromGrade > 11) {
            alert('First entry in classes should be greater than or equal to 1 and smaller than or equal to 11');
            $('#mainDiv #fromGrade').focus();
            return;
        }

        if (+toGrade < 2 || +toGrade > 12) {
            alert('Second entry in classes should be greater than or equal to 2 and smaller than or equal to 12');
            $('#mainDiv #toGrade').focus();
            return;
        }

        if (+fromGrade >= +toGrade) {
            alert('First entry in classes should be smaller than the second entry!');
            $('#mainDiv #fromGrade').focus();
            return;
        }

        schoolDetails['Classes Covered'] = `${fromGrade} To ${toGrade}`;

        if (+($('#mainDiv #selectLanguage').find(':selected').attr('id')) <= 0) {
            alert('Select One Language For Content!');
            $('#mainDiv #selectLanguage').focus();
            return;
        }
        schoolDetails.contentLanguage = $('#mainDiv #selectLanguage').find(':selected').html();

        if (+($('#mainDiv #selectBoard').find(':selected').attr('id')) <= 0) {
            alert('Select Education Board!');
            $('#mainDiv #selectBoard').focus();
            return;
        }
        schoolDetails.board = $('#mainDiv #selectBoard').find(':selected').html();

        if (!$('#mainDiv #tTablets').val()) {
            alert('Provide Number of Tablets!');
            $('#mainDiv #tTablets').focus();
            return;
        }
        schoolDetails.totalNumberOfTablets = +($('#mainDiv #tTablets').val());

        if (!$('#mainDiv #tTrolleys').val()) {
            alert('Provide Number of Trolleys!');
            $('#mainDiv #tTrolleys').focus();
            return;
        }
        schoolDetails.totalNumberOfTrolleys = +($('#mainDiv #tTrolleys').val());

        schoolDetails.commencementDate = $('#mainDiv #inputCommencementDate').val();
        schoolDetails.expiryDate = $('#mainDiv #inputExpiryDate').val();

        var allClassesAsString = '';
        var allClassesWithSections = {};
        const lastKey = Object.keys(classesWithDetails).pop();
        for (var classSection in classesWithDetails) {
            if (classSection === lastKey) {
                allClassesAsString += classSection;
            } else {
                allClassesAsString += classSection + ',';
            }

            // for 
            allClassesWithSections[classSection] = true;
        }
        appData.settings.classes.value = allClassesAsString;

        // Tablab Type
        if (+$('#mainDiv #selectTablabType').find(':selected').attr('id') < 0) {
            alert('Select Tablab type!');
            $('#mainDiv #selectTablabType').focus();
            return;
        }
        appData.settings.tablabType.value = +($('#mainDiv #selectTablabType').find(':selected').attr('id'));

        // Login Mode
        if (+$('#mainDiv #selectLoginMode').find(':selected').attr('id') < 0) {
            alert('Select Login Mode!');
            $('#mainDiv #selectLoginMode').focus();
            return;
        }
        appData.settings.loginMode.value = +($('#mainDiv #selectLoginMode').find(':selected').attr('id'));

        // Android Mode/Reports App Password
        if (!$('#mainDiv #tablabPassword').val()) {
            alert('Enter Password For Tablab!');
            $('#mainDiv #tablabPassword').focus();
            return;
        }
        appData.settings.go_to_password.value = $('#mainDiv #tablabPassword').val();

        // Need PopUp in App? - Pop-up that asks for student contact details
        var needPopup = ($($('#mainDiv #needAppPopUp input[type=radio]:checked')[0]).val() == "true") ? true : false;
        appData.settings.ask_for_student_contact_details = {
            "value": needPopup
        }

        // Will LMS be working as a launcher?
        var keepAsLauncher = ($($('#mainDiv #keepAsLauncher input[type=radio]:checked')[0]).val() == "true") ? true : false;
        appData.settings.keepAsLauncher = keepAsLauncher;

        // Onboarding time for LMS 
        // :Represents the days of usage after which the app can ask students for their contact details
        var onboardingTime = ($('#mainDiv #onboardingTime').val()) ? $('#mainDiv #onboardingTime').val() : 30; // 30 is default value
        appData.settings.onboardingTime = {
            "value": +onboardingTime
        };

        // session_time_before_details_popup for LMS
        // :Represents the number of seconds of continuous usage of LMS by loggedin students 
        //  after which app can show a popup to the student asking for his/her contact details
        var session_time_before_details_popup = ($('#mainDiv #session_time_before_details_popup').val()) ? $('#mainDiv #session_time_before_details_popup').val() : 600; // 600 (secs) is default value
        appData.settings.session_time_before_details_popup = {
            "value": +session_time_before_details_popup
        };

        // --------------- [START] Content Categories ---------------
        var contentCategories = $('#mainDiv #contentCategories').find('input[type="checkbox"]:checked');
        if (contentCategories.length <= 0) {
            alert('Please select content categories!');
            return;
        }
        contentCategories.each(function () {
            appData.settings.contentCategories.push($(this).val());
        });
        // --------------- Content Categories [END] ---------------

        // save subjects
        if (jQuery.isEmptyObject(subjectNames)) {
            alert('Inupt subject names in the language same as of the content.');
            $('#mainDiv #subjectName').focus();
            return;
        }
        appData.settings.subjects = subjectNames;

        // screenTexts
        if (jQuery.isEmptyObject(screenTexts)) {
            alert('Upload Screen Text File');
            $('#mainDiv #screenTexts').focus();
            return;
        }
        appData.settings.text = {
            'value': screenTexts
        }

        if (jQuery.isEmptyObject(contentJSONArray)) {
            alert('Upload Content File');
            $('#mainDiv #jsonFiles').focus();
            return;
        }
        appData.contentJSON = contentJSONArray;
        appData.extraCurricularContent = extracurricularContentJSONArray;

        // [START] school manager
        var schoolManagerID = $($('#mainDiv #schoolManager').find(':selected')[0]).prop('id');
        if (schoolManagerID !== 'hint') {
            schoolDetails.schoolManagers = [];
            schoolDetails.schoolManagers.push({
                'id': schoolManagerID
            });
        }
        //  school manager [END]

        /*  --- [END] Check Conditions ---*/

        // All Conditions are true, so disable submit button
        $('#form #buttonSubmit').prop('disabled', true);
        //        $('#loader').css('display', 'block');

        // logos
        loginHandler.uploadFile('logos/', logo1, (error, fileDownloadURL) => {
            if (error) {
                alert('Error while uploading Logo');
                $('#mainDiv #logo1').focus();
                return;
            }

            appData.settings.logo1 = {
                'value': fileDownloadURL
            }

            // second logo
            loginHandler.uploadFile('logos/', logo2, (error1, fileDownloadURL1) => {
                if (error1) {
                    alert('Error while uploading Logo');
                    $('#mainDiv #logo2').focus();
                    return;
                }

                appData.settings.logo2 = {
                    'value': fileDownloadURL1
                }

                // App IDs
                loginHandler.getter('Tablab/counter/appIDs', (snap) => {
                    var count = snap.child('count').val();

                    loginHandler.setter('Tablab/counter/appIDs/count', (+count) + (+schoolDetails.totalNumberOfTablets), () => {});

                    // create appID object
                    for (var i = 0; i < schoolDetails.totalNumberOfTablets; i++) {
                        var appID = 'APP' + (count + i + 1);
                        appIDs[appID] = {
                            'tab_uid': ''
                        }
                    }

                    var object = {
                        'AppIDs': appIDs,
                        'School_Details': schoolDetails,
                        'appData': appData,
                        'Classes': {}
                    }

                    // Classes with class IDs
                    object.Classes = allClassesWithSections;

                    // Save School Details
                    loginHandler.saveDetails('Tablab/counter/schools', 'Tablab/Schools', 'school', object, (error, schoolID) => {
                        if (error) {
                            alert('Error while saving data!');
                            location.reload();
                        }

                        $('#form')[0].reset();
                        $('#mainDiv #parentInputClassesDetails .dynamicChild').remove();
                        $('#mainDiv #parentInputSubjectNames .dynamicChild').remove();

                        // append newly created school to schools list
                        var options = {
                            'id': schoolID,
                            'uid': uid,
                            'username': username,
                            'name': schoolDetails.schoolName,
                            'district': schoolDetails.location.district,
                            'state': schoolDetails.location.state
                        }
                        var $row = createListsElement(options);
                        $('#schoolsList').append($row);

                        // save appIDs and school relationship
                        for (var key in appIDs) {
                            loginHandler.setter(`Tablab/App_School_Relation/${key}`, {
                                'SchoolID': schoolID
                            }, () => {});
                        }

                        // save classes with their details under its schoolID
                        loginHandler.setter(`Tablab/Classes/${schoolID}`, classesWithDetails, () => {});

                        // save school info under its project
                        loginHandler.setter(`Tablab/Projects/${schoolDetails.ProjectID}/Schools/${schoolID}`, {
                            'schoolName': schoolDetails.schoolName,
                            'location': schoolDetails.location
                        }, () => {});

                        // save school under assigned school manager's account
                        if (schoolManagerID !== 'hint')
                            loginHandler.setter(`users/${schoolManagerID}/did`, schoolID, (error) => {});

                        // Mail to the assigned coordinators regarding school creation with its details
                        var mailOptions = {
                            'from': '',
                            'to': '',
                            'subject': `Details of the school added to your project '${$('#projectName').html()}'`,
                        }

                        var appIDsToMail = '';
                        for (var key in appIDs) {
                            appIDsToMail += `   - ${key}<br>`;
                        }

                        var toEmails = [];
                        toEmails.push.apply(toEmails, iDreamCoordinators);
                        if(partnerCoordinators)
                            toEmails.push.apply(toEmails, partnerCoordinators);

                        mailOptions.to = toEmails;
                        mailOptions.html = `Dear coordinator 
                                            <br><br>
                                            <b>Name of the school</b> - ${schoolDetails.schoolName}
                                            <br><br>
                                            Automatically generated App IDs for the school are: 
                                            <br>
                                            ${appIDsToMail}`;

                        functions.sendRequest(`mailRequest`, 'POST', mailOptions, response => {
                            console.log('res', response);
                        });

                        alert('All Details saved. Click \"OK\"');

                        classesWithDetails = {};
                        screenTexts = {};

                        $('#loader').css('display', 'none');
                        $('#form #buttonSubmit').prop('disabled', false);
                    });
                });
            });
        });
    });


     $('#form #createDuplicateSubmit').click((event) => {
	    event.preventDefault();
		$('#tempSchoolList').html("");
	     console.log("hello");
           if (!$($('#schoolList').find(':selected')[0]).prop('id')) {
            alert('Please Select School  !');
            $('#schoolList').focus();
            return;
        }
        if (!$('#inputCopies').val()) {
            alert('Enter Copies !');
            $('#inputCopies').focus();
            return;
        }
         
		  $('#createSchools').css('display', '');
   var schoolCopies = parseInt($('#inputCopies').val());
       console.log(schoolCopies);
       for(var i=0; i<schoolCopies; i++ ){
          $('#tempSchoolList').prepend('<li id="li'+i+'"class= "col-lg-6 col-md-6 col-sm-12 col-xs-12"><div class=" col-lg-6 col-md-6 col-sm-12 col-xs-12"><a class="active" id="login-form-link" data-toggle="popover" data-trigger="hover" title="Popover Header" data-content="Some content inside the popover 2"><label>School Name</label></a><input type="text" name="schoolName" id="inputSchoolName" tabindex="2" class="inputs form-control" placeholder="Enter School Name"value="" required> <button class="btn btn-default" onclick="deleteLi('+ i+');" >Delete</button</div></li>');
              
         }
    

	 })  
      

    $('#mainDiv #saveClassInputs').on('click', (event) => {
        var $parent = $($(event.target).parent().parent());
        var inputs = $parent.find('input');

        if (!$(inputs[0]).val()) {
            alert('Enter Class Number!');
            $(inputs[0]).focus();
            return;
        }

        var cls = $(inputs[0]).val();
        var section = ($(inputs[1]).val()) ? $(inputs[1]).val() : '';
        var tStudents = (+($(inputs[2]).val())) ? +($(inputs[2]).val()) : 0; // total students in the class

        $(inputs[0]).val('');
        $(inputs[1]).val('');
        $(inputs[2]).val('');

        const classKey = (section) ? `${cls}-${section}` : cls;
        classesWithDetails[classKey] = {
            'Details': {
                'totalStudents': tStudents,
                'numberOfActiveStudents': 0
            }
        }

        const newDivID = `cls_${cls}_${section}`;
        var $newClassDetailsDiv = $(`<div class="row dynamicChild" id="${newDivID}">
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <input disabled type="number" name="classnumber" id="classnumber"" tabindex="1" class="form-control" placeholder="Class" value="${cls}" min='0'>
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <input disabled type="text" class="form-control" name="section" id="section" placeholder="Section" tabindex="1" value="${section}">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <input disabled type="number" class="form-control" name="tStudents" id="tStudents" placeholder="Students" tabindex="1" value="${tStudents}" min="0">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3" style="">
                                            <input type="button" id="updateClassInputs" onclick="updateClassDetails(this)" class="blue-round" value="Change">
                                        </div>
                                    </div>`);
        $newClassDetailsDiv.insertBefore($parent);
    });

    $('#mainDiv #saveSubjectNames').on('click', (event) => {
        var $parent = $($(event.target).parent().parent());
        var inputs = $parent.find('input');

        if (!$(inputs[0]).val()) {
            alert('Enter Subject Name!');
            $(inputs[0]).focus();
            return;
        }

        var subjectName = $(inputs[0]).val();
        $(inputs[0]).val('');

        subjectNames[subjectName] = true;
        var $newSubjectDetailsDiv = $(`<div class="row dynamicChild" id="${subjectName}">
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <input disabled type="text" class="form-control" name="subjectName" id="subjectName" placeholder="Subject Name" tabindex="1" value="${subjectName}">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3" style="">
                                            <input type="button" id="updateSubjects" onclick="updateSubjectName(this)" class="blue-round" value="Change">
                                        </div>
                                    </div>`);
        $newSubjectDetailsDiv.insertBefore($parent);
    });

    $('#mainDiv1 #saveSubjectNames').on('click', (event) => {
        var $parent = $($(event.target).parent().parent());
        var inputs = $parent.find('input');

        if (!$(inputs[0]).val()) {
            alert('Enter Subject Name!');
            $(inputs[0]).focus();
            return;
        }

        var subjectName = $(inputs[0]).val();
        $(inputs[0]).val('');

        subjectNames2[subjectName] = true;
        var $newSubjectDetailsDiv = $(`<div class="row dynamicChild" id="${subjectName}">
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                            <input disabled type="text" class="form-control" name="subjectName" id="subjectName" placeholder="Subject Name" tabindex="1" value="${subjectName}">
                                        </div>
                                        <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3" style="">
                                            <input type="button" id="updateSubjects" onclick="updateSubjectName(this)" class="blue-round" value="Change">
                                        </div>
                                    </div>`);
        $newSubjectDetailsDiv.insertBefore($parent);
    });

    var logo1;
    $('#mainDiv #logo1').on('change', (event) => {
        logo1 = event.target.files[0];
        var fileType = logo1["type"];
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            // invalid file type code goes here.
            alert('Wrong File Type!');
            $('#logo1').val('');
            $('#logo1').focus();
            return;
        }
    })

    var logo2;
    $('#mainDiv #logo2').on('change', (event) => {
        logo2 = event.target.files[0];
        var fileType = logo2["type"];
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            // invalid file type code goes here.
            alert('Wrong File Type!');
            $('#logo2').val('');
            $('#logo2').focus();
            return;
        }
    })

    $('#mainDiv #screenTexts').on('change', (event) => {
        screenTexts = {};
        var file = event.target.files[0];

        var fileType = file["type"];
        var ValidCSVTypes = ["text/csv", "application/vnd.ms-excel"];
        if ($.inArray(fileType, ValidCSVTypes) < 0) {
            // invalid file type code goes here.
            alert('Wrong File Type!');
            $('#mainDiv #screenTexts').val('');
            $('#mainDiv #screenTexts').focus();
            return;
        }

        var fr = new FileReader();
        fr.onload = function (event) {
            var content = event.target.result;

            var x = content.trim().split('\r\n');
            for (var i = 0; i < x.length; i++) {
                var keyValuePair = x[i].split(',');
                screenTexts[keyValuePair[0]] = keyValuePair[1];
            }
        }
        fr.readAsText(file);
    });

    $('#mainDiv1 #screenTexts').on('change', (event) => {
        screenTexts = {};
        var file = event.target.files[0];

        var fileType = file["type"];
        var ValidCSVTypes = ["text/csv", "application/vnd.ms-excel"];
        if ($.inArray(fileType, ValidCSVTypes) < 0) {
            // invalid file type code goes here.
            alert('Wrong File Type!');
            $('#mainDiv1 #screenTexts').val('');
            $('#mainDiv1 #screenTexts').focus();
            return;
        }

        var fr = new FileReader();
        fr.onload = function (event) {
            var content = event.target.result;

            var x = content.trim().split('\r\n');
            for (var i = 0; i < x.length; i++) {
                var keyValuePair = x[i].split(',');
                screenTexts[keyValuePair[0]] = keyValuePair[1];
            }
        }
        fr.readAsText(file);
    });

    $('#mainDiv #jsonFiles').on('change', (event) => {
        contentJSONArray = {};
        // var files = event.target.files;
        var file = event.target.files[0];
        readContentJSONFile(file, 0, (json) => {
            contentJSONArray = json;
        });
    });

    $('#mainDiv #extracurricularJsonFiles').on('change', (event) => {
        extracurricularContentJSONArray = {};
        // var files = event.target.files;
        var file = event.target.files[0];
        readContentJSONFile(file, 0, (json) => {
            extracurricularContentJSONArray = json;
        });
    });

    // set content language options
    languages.forEach((language, i) => {
        $('#mainDiv #selectLanguage').append($(`<option id="${i+1}">${language}</option>`));
    });

    // set Educational boards inside options
    boards.forEach((board, i) => {
        $('#mainDiv #selectBoard').append($(`<option id="${i+1}">${board}</option>`));
    });

    // set states options
    states.forEach((state, i) => {
        $('#mainDiv #sLocationState').append($(`<option id="${i+1}">${state.name}</option>`));
    });

    $('#mainDiv #sLocationState').on('change', (event) => {
        var selectedStateIndex = +($($(event.target).find(':selected')[0]).prop('id'));
        setDistrictsAsOptions($('#mainDiv #sLocationDistt'), selectedStateIndex - 1);
    });

    // -------------------- For school details form, inside modal --------------------
    $('#mainDiv1 #sLocationState').on('change', (event) => {
        var selectedStateIndex = +($($(event.target).find(':selected')[0]).prop('id'));
        setDistrictsAsOptions($('#mainDiv1 #sLocationDistt'), selectedStateIndex - 1);
    });
});

// update classes and details
function updateClassDetails(targetButton, toUpdateSchoolDetails) {
    var element = $(targetButton).parent().parent();
    const targetInputs = $(element).find('input:not(:button)');

    if ($(targetButton).val() === 'Update') { // update finish command
        // [START] Check if all values are entered properly
        if (!$(targetInputs[0]).val()) {
            alert('Enter Class Number!');
            $(targetInputs[0]).focus();
            return;
        }

        // old values
        const id = $(element).prop('id').split('_');
        const classSectionValue = (id[2]) ? `${id[1]}-${id[2]}` : id[1];

        // delete object for old values
        if (toUpdateSchoolDetails)
            delete classesWithDetails2[classSectionValue];
        else
            delete classesWithDetails[classSectionValue];

        // New values
        var cls = $(targetInputs[0]).val();
        var section = ($(targetInputs[1]).val()) ? $(targetInputs[1]).val() : '';
        var tStudents = ($(targetInputs[2]).val()) ? +($(targetInputs[2]).val()) : 0; // total students in the class

        const key = (section) ? `${cls}-${section}` : cls;

        // Add object for new values
        if (toUpdateSchoolDetails) {
            classesWithDetails2[key] = {
                'Details': {
                    'totalStudents': tStudents,
                    'numberOfActiveStudents': 0
                }
            }
        } else {
            classesWithDetails[key] = {
                'Details': {
                    'totalStudents': tStudents,
                    'numberOfActiveStudents': 0
                }
            }
        }

        // update target element properties
        if (toUpdateSchoolDetails) {
            $(element).find('#tStudents').prop('disabled', true);
        } else {
            $(targetInputs[0]).prop('disabled', true);
            $(targetInputs[1]).prop('disabled', true);
            $(targetInputs[2]).prop('disabled', true);
        }
        $(targetButton).removeClass('red-round').addClass('blue-round').val('Change');

        // update id
        const newID = `cls_${cls}_${section}`;
        $(element).prop('id', newID);
    } else { // initiate update command
        if (toUpdateSchoolDetails) {
            $(element).find('#tStudents').prop('disabled', false);
        } else {
            $(targetInputs[0]).prop('disabled', false);
            $(targetInputs[1]).prop('disabled', false);
            $(targetInputs[2]).prop('disabled', false);
        }
        $(targetButton).removeClass('blue-round').addClass('red-round').val('Update');
    }
}

// update subjects
function updateSubjectName(targetButton, toUpdateSchoolDetails) {
    var element = $(targetButton).parent().parent();
    const targetInputs = $(element).find('input:not(:button)');

    if ($(targetButton).val() === 'Update') { // update finish command
        // [START] Check if all values are entered properly
        if (!$(targetInputs[0]).val()) {
            alert('Enter Subject Name!');
            $(targetInputs[0]).focus();
            return;
        }

        // old value
        const oldSubjectName = $(element).prop('id');

        // delete object for old values
        if (toUpdateSchoolDetails)
            delete subjectNames2[oldSubjectName];
        else
            delete subjectNames[oldSubjectName];

        var newSubjectName = $(targetInputs[0]).val();

        // Add object for new values
        if (toUpdateSchoolDetails) {
            subjectNames2[newSubjectName] = true;
        } else {
            subjectNames[newSubjectName] = true;
        }

        // update target element properties
        if (toUpdateSchoolDetails) {
            $(targetInputs).prop('disabled', true);
        } else {
            $(targetInputs[0]).prop('disabled', true);
        }
        $(targetButton).removeClass('red-round').addClass('blue-round').val('Change');

        // update id
        $(element).prop('id', newSubjectName);
    } else { // initiate update command
        if (toUpdateSchoolDetails) {
            $(targetInputs).prop('disabled', false);
        } else {
            $(targetInputs[0]).prop('disabled', false);
        }
        $(targetButton).removeClass('blue-round').addClass('red-round').val('Update');
    }
}

// set districts options
function setDistrictsAsOptions(element, stateIndex) {
    $(element).html('');
    $(element).append('<option disabled selected value>--- Select District ---</option>');

    if (stateIndex < 0) {
        return;
    }

    states[+stateIndex].districts.forEach((district, i) => {
        $(element).append($(`<option id="${i+1}">${district}</option>`))
    });
}

function createListsElement(options) {
    var $row = $(`<tr></tr>`);
    var $td1 = $(`<td class="col-md-4">${options.name}</td>`);
    var $td2 = $(`<td class="col-md-2">${options.district}</td>`);
    var $td3 = $(`<td class="col-md-2">${options.state}</td>`);
    var $td4 = $(`<td class="col-md-2">
                    <button class="blue-round form-group" id="${options.id}" onclick="showSchoolDetails(this)" href="#">
                        <!--<i class="fa fa-eye"></i>-->
                        Click
                    </button>
                </td>`);
    var $td5 = $(`<td class="col-md-2">
<!--                    <a href="/reports/schoolPage?u=${options.uid}&n=${options.username}&did=${options.id}"> -->
                        <button id="${options.id}" class="red-round form-group" onclick=requestSchoolReports(this)>
                            <!--<i class="fa fa-eye"></i>-->
                            Click
                        </button>
                    </a>
                </td>`);
    $row.append($td1);
    $row.append($td2);
    $row.append($td3);
    $row.append($td4);
    $row.append($td5);
    return $row;
}

// navigation
var currentURL = window.location.href;
var navTitles = (currentURL.split("#")[1]) ? ((currentURL.split("#")[1]).replace(/%20/g, " ")).split(',') : [];

function requestSchoolReports(target) {
    var schoolID = $(target).prop('id');
    var nextPageURL = `/reports/schoolPage?u=${uid}&n=${username}&did=${schoolID}#`;

    for (var index = 0; index < navTitles.length; index++) {
        nextPageURL += navTitles[index] + ",";
    }
    
    window.location = nextPageURL + ($('title').html().split("-")[1]).trim();
}

var schoolDetailsSnapshot = {};
var currentlySelectedSchoolID;

function showSchoolDetails(element) {
    $('#2a').removeClass('active');
    $('#1a').addClass('active');
    $('#21a').removeClass('active');
    $('#11a').addClass('active');
    $('#11a form')[0].reset();
    $('#mainDiv1 #schoolManager option:gt(0)').remove();
    $('#mainDiv1 #parentInputClassesDetails .dynamicChild').remove();
    $('#mainDiv1 #parentInputSubjectNames .dynamicChild').remove();
    $('#modalDiv').modal('show');

    $('#showAppIDs').removeClass('active');
    $('#bodyAppIDs').html('');

    // reset subjects object
    subjectNames2 = {};

    // Reset screenTexts object
    screenTexts = {};
    $('#mainDiv #screenTexts').val('');

    for (var key in schoolManagers) {
        var $option = $(`<option id="${key}" value="${key}">${schoolManagers[key].name} (${schoolManagers[key].email})</option>`);
        $('#mainDiv1 #schoolManager').append($option);
    }

    currentlySelectedSchoolID = $(element).prop('id');

    // request to the server to get the AppIDs of the school
    functions.sendRequest(`getSchoolAppIDs?schoolID=${currentlySelectedSchoolID}`, 'GET', null, (error, data) => {
        // show AppIDs and linked device IDs in the table
        if (error) {
            $('#bodyAppIDs').html(error.error);
            return;
        }

        var count = 0;
        for (var key in data) {
            $('#bodyAppIDs').append(`<tr>
                        <td>${++count}</td>
                        <td>${key}</td>
                        <td>${data[key].tab_uid}</td>
                    </tr>`);
        }

    });

    // Request to the server get the school details from the database
    functions.sendRequest(`getSchoolDetails?did=${currentlySelectedSchoolID}`, 'GET', null, (error, data) => {
        if (error) {
            alert('Error while fetching school details!');
            $('#mainDiv1').html('Error while fetching school details!');
            return;
        }

        schoolDetailsSnapshot = data;

        // set content language options
        languages.forEach((language, i) => {
            $('#mainDiv1 #selectLanguage').append($(`<option id="${i+1}">${language}</option>`))
        });

        // set Educational boards inside options
        boards.forEach((board, i) => {
            $('#mainDiv1 #selectBoard').append($(`<option id="${i+1}">${board}</option>`))
        });

        var selectedStateIndex = -1;

        // set states options
        states.forEach((state, i) => {
            if (data.School_Details.location.state === state.name) {
                selectedStateIndex = i;
            }
            $('#mainDiv1 #sLocationState').append($(`<option id="${i+1}">${state.name}</option>`))
        });

        setDistrictsAsOptions($('#mainDiv1 #sLocationDistt'), selectedStateIndex);
        $('#mainDiv1 #inputSchoolName').val(data.School_Details.schoolName);
        $('#mainDiv1 #sLocation').val(data.School_Details.location.area);
        $('#mainDiv1 #sLocationState').val(data.School_Details.location.state);
        $('#mainDiv1 #sLocationDistt').val(data.School_Details.location.district);
        $('#mainDiv1 #inputCommencementDate').val(data.School_Details.commencementDate);
        $('#mainDiv1 #inputExpiryDate').val(data.School_Details.expiryDate);

        const classesCovered = (data.School_Details['Classes Covered']).split(' To ');
        $('#mainDiv1 #fromGrade').val(+classesCovered[0]);
        $('#mainDiv1 #toGrade').val(+classesCovered[1]);
        $('#mainDiv1 #inputClasses').val(data.appData.settings.classes.value);
        $('#mainDiv1 #tTablets').val(+(data.School_Details.totalNumberOfTablets));
        $('#mainDiv1 #tTrolleys').val(+(data.School_Details.totalNumberOfTrolleys));
        $('#mainDiv1 #selectBoard').val(data.School_Details.board).prop('disabled', true);
        $('#mainDiv1 #selectLanguage').val(data.School_Details.contentLanguage).prop('disabled', true);

        // set school manager
        var x = (data.School_Details.schoolManagers && data.School_Details.schoolManagers[0].id) ?
            $(`#mainDiv1 #schoolManager option[id="${data.School_Details.schoolManagers[0].id}"]`).prop('selected', true) :
            $('#mainDiv1 #schoolManager').val($('#mainDiv1 #schoolManager option').eq(0).val());

        $('#mainDiv1 #selectTablabType').val($('#mainDiv1 #selectTablabType option').eq(+(data.appData.settings.tablabType.value) + 1).val());
        $('#mainDiv1 #selectLoginMode').val($('#mainDiv1 #selectLoginMode option').eq(+(data.appData.settings.loginMode.value) + 1).val());
        $('#mainDiv1 #tablabPassword').val(data.appData.settings.go_to_password.value);

        if (data.appData.settings.ask_for_student_contact_details && data.appData.settings.ask_for_student_contact_details.value) {
            $($('#mainDiv1 #needAppPopUp input[type="radio"]')[0]).prop("checked", true);
        } else {
            $($('#mainDiv1 #needAppPopUp input[type="radio"]')[1]).prop("checked", true);
        }

        // Will LMS be working as a launcher?
        if (data.appData.settings.keepAsLauncher == false) {
            $($('#mainDiv1 #keepAsLauncher input[type="radio"]')[1]).prop("checked", true);
        } else {
            $($('#mainDiv1 #keepAsLauncher input[type="radio"]')[0]).prop("checked", true);
        }

        // Onboarding time for LMS 
        // :Represents the days of usage after which the app can ask students for their contact details
        if (data.appData.settings.onboardingTime) {
            $('#mainDiv1 #onboardingTime').val(data.appData.settings.onboardingTime.value);
        }

        // session_time_before_details_popup for LMS
        // :Represents the number of seconds of continuous usage of LMS by loggedin students 
        //  after which app can show a popup to the student asking for his/her contact details
        if (data.appData.settings.session_time_before_details_popup) {
            $('#mainDiv1 #session_time_before_details_popup').val(session_time_before_details_popup.value)
        }

        // content categories
        const contentCategories = data.appData.settings.contentCategories;
        contentCategories.forEach((category) => {
            $(`#mainDiv1 #contentCategories input:checkbox[value=${category}]`).prop('checked', true);
        });

        // list all the subjects
        if (data.appData.settings.subjects)
            subjectNames2 = data.appData.settings.subjects;

        for (var key in subjectNames2) {
            if (subjectNames2[key]) {
                var $newSubjectNameDiv = $(`<div class="row dynamicChild" id="${key}">
                                            <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <input disabled type="text" class="form-control" name="subjectName" id="subjectName" placeholder="Section" tabindex="1" value="${key}">
                                            </div>
                                            <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3" style="">
                                                <input type="button" id="updateSubjects" onclick="updateSubjectName(this, 2)" class="blue-round" value="Change">
                                            </div>
                                        </div>`);
                $newSubjectNameDiv.insertBefore($('#mainDiv1 #inputSubjectNames'));
            }
        }

        // Show the loader for classes
        $('#loader2a').css('display', 'block');

        // Request to get classes from database and list them
        functions.sendRequest(`getClassesDetails?did=${currentlySelectedSchoolID}`, 'GET', null, (error, classesData) => {
            $('#loader2a').css('display', 'none');
            if (error) {
                alert('Error while fetching classes and their details!');
                $('#21a').html('Error while fetching classes and their details!');
                return;
            }

            // list all the classes
            classesWithDetails2 = classesData;
            for (var key in classesWithDetails2) {
                if (classesWithDetails2[key]) {
                    var cls = key.split('-')[0];
                    var section = (key.split('-')[1]) ? key.split('-')[1] : "";
                    var tStudents = (classesWithDetails2[key].Details.totalStudents) ? +(classesWithDetails2[key].Details.totalStudents) : 0; // total students in the class

                    const newDivID = `cls_${cls}_${section}`;
                    var $newClassDetailsDiv = $(`<div class="row dynamicChild" id="${newDivID}">
                                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <input disabled type="number" name="classnumber" id="classnumber" tabindex="1" class="form-control" placeholder="Class" value="${cls}" min='0'>
                                                </div>
                                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <input disabled type="text" class="form-control" name="section" id="section" placeholder="Section" tabindex="1" value="${section}">
                                                </div>
                                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                    <input disabled type="number" class="form-control tStudents" name="tStudents" id="tStudents" placeholder="Students" tabindex="1" value="${tStudents}" min="0">
                                                </div>
                                                <div class="form-group col-lg-3 col-md-3 col-sm-3 col-xs-3" style="">
                                                    <input type="button" id="updateClassInputs" onclick="updateClassDetails(this, 2)" class="blue-round" value="Change">
                                                </div>
                                            </div>`);
                    $newClassDetailsDiv.insertBefore($('#mainDiv1 #inputClassesDetails'));
                }
            }
        });
    });
}

$('#modal-form #buttonSubmit').click((event) => {
    event.preventDefault();

    /*  --- [START] Check Conditions ---*/
    // Check if School name is empty
    if (!$('#mainDiv1 #inputSchoolName').val() || $('#mainDiv1 #inputSchoolName').val() === '') {
        alert('School Name is necessary!');
        $('#mainDiv1 #inputSchoolName').focus();
        return;
    }
    schoolDetailsSnapshot.School_Details.schoolName = $('#mainDiv1 #inputSchoolName').val();

    // address as text [area]
    if (!$('#mainDiv1 #sLocation').val()) {
        alert('Provide School Address!');
        $('#mainDiv1 #sLocation').focus();
        return;
    }
    schoolDetailsSnapshot.School_Details.location.area = $('#mainDiv1 #sLocation').val();

    // state
    if (!$($('#mainDiv1 #sLocationState').find(':selected')[0]).prop('id')) {
        alert('Please Select State!');
        $('#mainDiv1 #sLocationState').focus();
        return;
    }
    schoolDetailsSnapshot.School_Details.location.state = $($('#mainDiv1 #sLocationState').find(':selected')[0]).val();

    // district
    if (!$($('#mainDiv1 #sLocationDistt').find(':selected')[0]).prop('id')) {
        alert('Please Select District!');
        $('#mainDiv1 #sLocationDistt').focus();
        return;
    }
    schoolDetailsSnapshot.School_Details.location.district = $($('#mainDiv1 #sLocationDistt').find(':selected')[0]).val();

    // Grades/Classes information
    if (!$('#mainDiv1 #fromGrade').val()) {
        alert('Enter Class!');
        $('#mainDiv1 #fromGrade').focus();
        return;
    }
    const fromGrade = $('#mainDiv1 #fromGrade').val();

    if (!$('#mainDiv1 #toGrade').val()) {
        alert('Enter Class!');
        $('#mainDiv1 #toGrade').focus();
        return;
    }
    const toGrade = $('#mainDiv1 #toGrade').val();
    if (+fromGrade < 1 || +fromGrade > 11) {
        alert('First entry in classes should be greater than or equal 1 and smaller than or equal to 11.');
        $('#mainDiv1 #fromGrade').focus();
        return;
    }
    if (+toGrade < 2 || +toGrade > 12) {
        alert('Second entry in classes should be greater than or equal to 2 and smaller than or equal to 12');
        $('#mainDiv1 #toGrade').focus();
        return;
    }
    if (+fromGrade >= +toGrade) {
        alert('First entry in classes should be smaller than the second entry!');
        $('#mainDiv1 #fromGrade').focus();
        return;
    }
    schoolDetailsSnapshot.School_Details['Classes Covered'] = `${fromGrade} To ${toGrade}`;

    // Content Language
    if (+($('#mainDiv1 #selectLanguage').find(':selected').attr('id')) <= 0) {
        alert('Select One Language For Content!');
        $('#mainDiv1 #selectLanguage').focus();
        return;
    }
    schoolDetailsSnapshot.School_Details.contentLanguage = $('#mainDiv1 #selectLanguage').find(':selected').html();

    // Education Board
    if (+($('#mainDiv1 #selectBoard').find(':selected').attr('id')) <= 0) {
        alert('Select one education board!');
        $('#mainDiv1 #selectBoard').focus();
        return;
    }
    schoolDetailsSnapshot.School_Details.board = $('#mainDiv1 #selectBoard').find(':selected').html();

    // Tablets
    if (!$('#mainDiv1 #tTablets').val()) {
        alert('Provide Number of Tablets!');
        $('#mainDiv1 #tTablets').focus();
        return;
    }
    var oldTotalTablets = +schoolDetailsSnapshot.School_Details.totalNumberOfTablets;
    if (+($('#mainDiv1 #tTablets').val()) < oldTotalTablets) {
        alert('Number of tablets cannot be decremented!');
        $('#mainDiv1 #tTablets').focus();
        return;
    }
    schoolDetailsSnapshot.School_Details.totalNumberOfTablets = +($('#mainDiv1 #tTablets').val());

    // Trolleys
    if (!$('#mainDiv1 #tTrolleys').val()) {
        alert('Provide number of Trolleys!');
        $('#mainDiv1 #tTrolleys').focus();
        return;
    }
    if (+($('#mainDiv1 #tTrolleys').val()) < schoolDetailsSnapshot.School_Details.totalNumberOfTrolleys) {
        alert('Number of trolleys cannot be decremented!');
        $('#mainDiv1 #tTrolleys').focus();
        return;
    }
    schoolDetailsSnapshot.School_Details.totalNumberOfTrolleys = +($('#mainDiv1 #tTrolleys').val());

    // Project commencement date & Project expiry date
    var newCommencementDate = Date.parse($('#mainDiv1 #inputCommencementDate').val());
    var newExpiryDate = Date.parse($('#mainDiv1 #inputExpiryDate').val());
    if (newCommencementDate && newExpiryDate && newCommencementDate < newExpiryDate) {
        schoolDetailsSnapshot.School_Details.commencementDate = $('#mainDiv1 #inputCommencementDate').val();
        schoolDetailsSnapshot.School_Details.expiryDate = $('#mainDiv1 #inputExpiryDate').val();
    }

    // More info on classes
    var allClassesAsString = '';
    var allClassesWithSections = {};
    const lastKey = Object.keys(classesWithDetails2).pop();
    for (var classSection in classesWithDetails2) {
        if (classSection === lastKey) {
            allClassesAsString += classSection;
        } else {
            allClassesAsString += classSection + ',';
        }

        // for 
        allClassesWithSections[classSection] = true;
    }
    schoolDetailsSnapshot.appData.settings.classes.value = allClassesAsString;

    // Tablab Type
    if (+$('#mainDiv1 #selectTablabType').find(':selected').attr('id') < 0) {
        alert('Select Tablab type!');
        $('#mainDiv1 #selectTablabType').focus();
        return;
    }
    schoolDetailsSnapshot.appData.settings.tablabType.value = +($('#mainDiv1 #selectTablabType').find(':selected').attr('id'));

    // Login Mode
    if (+$('#mainDiv1 #selectLoginMode').find(':selected').attr('id') < 0) {
        alert('Select Login Mode!');
        $('#mainDiv1 #selectLoginMode').focus();
        return;
    }
    schoolDetailsSnapshot.appData.settings.loginMode.value = +($('#mainDiv1 #selectLoginMode').find(':selected').attr('id'));

    // Android Mode/Reports App Password
    if (!$('#mainDiv1 #tablabPassword').val()) {
        alert('Enter Password For Tablab!');
        $('#mainDiv1 #tablabPassword').focus();
        return;
    }
    schoolDetailsSnapshot.appData.settings.go_to_password.value = $('#mainDiv1 #tablabPassword').val();

    // Need PopUp in App? - Pop-up that asks for student contact details
    var needPopup = ($($('#mainDiv1 #needAppPopUp input[type=radio]:checked')[0]).val() == "true") ? true : false;
    if(!schoolDetailsSnapshot.appData.settings.ask_for_student_contact_details) {
        schoolDetailsSnapshot.appData.settings.ask_for_student_contact_details = {
            value: ""
        }
    }
    schoolDetailsSnapshot.appData.settings.ask_for_student_contact_details.value = needPopup;

    // Will LMS be working as a launcher?
    var keepAsLauncher = ($($('#mainDiv1 #keepAsLauncher input[type=radio]:checked')[0]).val() == "true") ? true : false;
    schoolDetailsSnapshot.appData.settings.keepAsLauncher = keepAsLauncher;

    // Onboarding time for LMS 
    // :Represents the days of usage after which the app can ask students for their contact details
    var onboardingTime = ($('#mainDiv1 #onboardingTime').val()) ? $('#mainDiv1 #onboardingTime').val() : 30; // 30 is default value
    schoolDetailsSnapshot.appData.settings.onboardingTime = {
        "value": +onboardingTime
    };

    // session_time_before_details_popup for LMS
    // :Represents the number of seconds of continuous usage of LMS by loggedin students 
    //  after which app can show a popup to the student asking for his/her contact details
    var session_time_before_details_popup = ($('#mainDiv1 #session_time_before_details_popup').val()) ? $('#mainDiv1 #session_time_before_details_popup').val() : 600; // 600 (secs) is default value
    schoolDetailsSnapshot.appData.settings.session_time_before_details_popup = {
        "value": +session_time_before_details_popup
    };

    // --------------- [START] Content Categories ---------------
    var contentCategories = $('#mainDiv1 #contentCategories').find('input[type="checkbox"]:checked');
    if (contentCategories.length <= 0) {
        alert('Please select content categories!');
        return;
    }
    var categoriesArray = [];
    contentCategories.each(function () {
        categoriesArray.push($(this).val());
    });
    schoolDetailsSnapshot.appData.settings.contentCategories = categoriesArray;
    // --------------- Content Categories [END] ---------------

    // save subjects
    if (jQuery.isEmptyObject(subjectNames2)) {
        alert('Inupt subject names in the language same as of the content.');
        $('#mainDiv1 #subjectName').focus();
        return;
    }
    schoolDetailsSnapshot.appData.settings.subjects = subjectNames2;

    // screenTexts
    if (!jQuery.isEmptyObject(screenTexts)) {
        schoolDetailsSnapshot.appData.settings.text = {
            'value': screenTexts
        }
    }

    if (!jQuery.isEmptyObject(contentJSONArray)) {
        schoolDetailsSnapshot.appData.contentJSON = contentJSONArray;
    }

    if (!jQuery.isEmptyObject(extracurricularContentJSONArray)) {
        schoolDetailsSnapshot.appData.extraCurricularContent = extracurricularContentJSONArray;
    }

    // [START] school manager
    var schoolManagerID = $($('#mainDiv1 #schoolManager').find(':selected')[0]).prop('id');
    if (schoolManagerID !== 'hint') {
        var newSchoolManager = [];
        newSchoolManager.push({
            'id': schoolManagerID
        });
        schoolDetailsSnapshot.School_Details.schoolManagers = newSchoolManager;
    }
    //  school manager [END]

    /*  --- [END] Check Conditions ---*/


    // All Conditions are true, so disable submit button 
    $('#modal-form #buttonSubmit').prop('disabled', true);
    // $('#loader').css('display', 'block');

    // logos
    loginHandler.uploadFile('logos/', logo21, (error, fileDownloadURL) => {
        if (fileDownloadURL) {
            schoolDetailsSnapshot.appData.settings.logo1 = {
                'value': fileDownloadURL
            }
        }

        // second logo
        loginHandler.uploadFile('logos/', logo22, (error1, fileDownloadURL1) => {
            if (fileDownloadURL1) {
                schoolDetailsSnapshot.appData.settings.logo2 = {
                    'value': fileDownloadURL1
                }
            }

            // App IDs
            loginHandler.getter('Tablab/counter/appIDs', (snap) => {
                if (!schoolDetailsSnapshot.hasOwnProperty("AppIDs")) {
                    schoolDetailsSnapshot.AppIDs = {}
                }

                var count = +snap.child('count').val();
                var newTotalTablets = +schoolDetailsSnapshot.School_Details.totalNumberOfTablets;
                if (newTotalTablets > oldTotalTablets) {
                    loginHandler.setter('Tablab/counter/appIDs/count', count + (newTotalTablets - oldTotalTablets), () => {});

                    // create appID object
                    var appIDs = {};
                    for (var i = 0; i < newTotalTablets - oldTotalTablets; i++) {
                        var appID = 'APP' + (count + i + 1);
                        appIDs[appID] = true;

                        schoolDetailsSnapshot.AppIDs[appID] = {
                            'tab_uid': ''
                        }
                    }
                }

                // Classes with class IDs
                schoolDetailsSnapshot.Classes = allClassesWithSections;

                // Save School Details
                // 1. School_Details
                loginHandler.setter(`Tablab/Schools/${currentlySelectedSchoolID}/School_Details`, schoolDetailsSnapshot.School_Details, (error) => {
                    if (error) {
                        alert('Error while saving data!');
                        location.reload();
                    }

                    // 2. App Data
                    loginHandler.setter(`Tablab/Schools/${currentlySelectedSchoolID}/appData`, schoolDetailsSnapshot.appData, (error) => {
                        if (error) {
                            alert('Error while saving data!');
                            location.reload();
                        }

                        // 3. Classes
                        loginHandler.setter(`Tablab/Schools/${currentlySelectedSchoolID}/Classes`, schoolDetailsSnapshot.Classes, (error) => {
                            if (error) {
                                alert('Error while saving data!');
                                location.reload();
                            }

                            // 4. AppIDs
                            loginHandler.setter(`Tablab/Schools/${currentlySelectedSchoolID}/AppIDs`, schoolDetailsSnapshot.AppIDs, (error) => {
                                if (error) {
                                    alert('Error while saving data!');
                                    location.reload();
                                }

                                // All data saved properly
                                $('#modal-form')[0].reset();
                                $('#mainDiv1 #parentInputClassesDetails .dynamicChild').remove();

                                // append newly created school to schools list
                                var options = {
                                    'id': currentlySelectedSchoolID,
                                    'uid': uid,
                                    'username': username,
                                    'name': schoolDetailsSnapshot.School_Details.schoolName,
                                    'district': schoolDetailsSnapshot.School_Details.location.district,
                                    'state': schoolDetailsSnapshot.School_Details.location.state
                                }
                                var $row = createListsElement(options);
                                $('#mainDiv1 #schoolsList').append($row);

                                // save appIDs and school relationship
                                for (var key in appIDs) {
                                    loginHandler.setter(`Tablab/App_School_Relation/${key}`, {
                                        'SchoolID': currentlySelectedSchoolID
                                    }, () => {})
                                }

                                // save classes with their details under its schoolID
                                loginHandler.setter(`Tablab/Classes/${currentlySelectedSchoolID}`, classesWithDetails2, () => {});

                                // save school info under its project
                                loginHandler.setter(`Tablab/Projects/${schoolDetailsSnapshot.School_Details.ProjectID}/Schools/${currentlySelectedSchoolID}`, {
                                    'schoolName': schoolDetailsSnapshot.School_Details.schoolName,
                                    'location': schoolDetailsSnapshot.School_Details.location
                                }, () => {});

                                // save school under assigned school manager's account
                                if (schoolManagerID !== 'hint')
                                    loginHandler.setter(`users/${schoolManagerID}/did`, currentlySelectedSchoolID, (error) => {});

                                alert('All Details saved. Click \"OK\"');

                                classesWithDetails2 = {};
                                screenTexts = {};

                                $('#modalDiv').modal('hide');

                                // $('#loader').css('display', 'none');
                                $('#modal-form #buttonSubmit').prop('disabled', false);
                            });
                        });
                    });
                });
            });
        });
    });
});

var logo21;
$('#mainDiv1 #logo1').on('change', (event) => {
    logo21 = event.target.files[0];
    var fileType = logo21["type"];
    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
    if ($.inArray(fileType, ValidImageTypes) < 0) {
        // invalid file type code goes here.
        alert('Wrong File Type!');
        $('#mainDiv1 #logo1').val('');
        $('#mainDiv1 #logo1').focus();
        return;
    }
});

var logo22;
$('#mainDiv1 #logo2').on('change', (event) => {
    logo22 = event.target.files[0];
    var fileType = logo22["type"];
    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
    if ($.inArray(fileType, ValidImageTypes) < 0) {
        // invalid file type code goes here.
        alert('Wrong File Type!');
        $('#mainDiv1 #logo2').val('');
        $('#mainDiv1 #logo2').focus();
        return;
    }
});

$('#mainDiv1 #screenTexts').on('change', (event) => {
    screenTexts = {};
    var file = event.target.files[0];

    var fileType = file["type"];
    var ValidCSVTypes = ["text/csv", "application/vnd.ms-excel"];
    if ($.inArray(fileType, ValidCSVTypes) < 0) {
        // invalid file type code goes here.
        alert('Wrong File Type!');
        $('#mainDiv1 #screenTexts').val('');
        $('#mainDiv1 #screenTexts').focus();
        return;
    }

    var fr = new FileReader();
    fr.onload = function (event) {
        var content = event.target.result;

        var x = content.trim().split('\r\n');
        for (var i = 0; i < x.length; i++) {
            var keyValuePair = x[i].split(',');
            screenTexts[keyValuePair[0]] = keyValuePair[1];
        }
    }
    fr.readAsText(file);
});

function readContentJSONFile(files, i, done) {
    if (i >= files.length) {
        return;
    }

    var fr = new FileReader();
    fr.onload = function (event) {
        var content = event.target.result;
        var x = content.trim();
        var object = {};

        try {
            object = JSON.parse(x);
            // array.push(JSON.parse(x));
            // readContentJSONFile(files, i + 1, array);
        } catch (error) {
            alert(`Not a valid JSON file!\nPlease verify and upload the file again.`);
            $('#jsonFiles').val('');
            $('#jsonFiles').focus();
            object = [];
        }
        done(object);
    }
    fr.readAsText(files);
    // fr.readAsText(files[i]);
}

$('#mainDiv1 #jsonFiles').on('change', (event) => {
    contentJSONArray = {};
    // var files = event.target.files;
    var file = event.target.files[0];
    readContentJSONFile(file, 0, (json) => {
        contentJSONArray = json;
    });
});

function deleteLi(id){
	
	
	 $("#li"+id).remove();
	
	 if($("#tempSchoolList").children().length ===0) {
      $('#createSchools').css('display', 'none');
      }
}

$('#mainDiv1 #extracurricularJsonFiles').on('change', (event) => {
    extracurricularContentJSONArray = {};
    // var files = event.target.files;
    var file = event.target.files[0];
    readContentJSONFile(file, 0, (json) => {
        extracurricularContentJSONArray = json;
    });
});


 $('#createSchools').click((event) => {
	 event.preventDefault();
	 var schoolNames =[];
	 var flag =0;
	//console.log($('#tempSchoolList')find("input").length);
	  $('#tempSchoolList').find("input").map(function() {
		  if($(this).val()==""){
			 console.log($(this).val()); 
			 flag =1;
			 $(this).focus();
			 return false;
		  }	else {
			  schoolNames.push($(this).val());
		  }  
	  })
	  console.log(schoolNames)
	  if(flag==1){
		  alert("Please Fill All School Names");
		   return false;
	  }else{
		   if (!$($('#schoolList').find(':selected')[0]).val()) {
            alert('Please Select School  !');
            $('#schoolList').focus();
            return;
        }
      var schoolId = $($('#schoolList').find(':selected')[0]).val();
      console.log(schoolId);
	  
	    functions.sendRequest(`getSchoolDetails?did=${schoolId}`, 'GET', null, (error, schoolSnapData) => {
        if (error) {
            alert('Error while fetching school details!');
            $('#mainDiv1').html('Error while fetching school details!');
            return;
        }
  for(var i =0; i<schoolNames.length; i++){
        console.log(schoolSnapData);
		console.log(pid);
		console.log()
		 var schoolDetails = {};
        var appIDs = {};
        
        schoolDetails.schoolName = schoolNames[i];
        schoolDetails.ProjectID = pid;
        schoolDetails.implementationPartnerName = schoolSnapData.School_Details.implementationPartnerName;
        schoolDetails.fundingPartnerName = schoolSnapData.School_Details.fundingPartnerName;
        schoolDetails.commencementDate = '';
        schoolDetails.expiryDate = '';
        schoolDetails.totalNumberOfTablets = 0;
        schoolDetails.totalNumberOfTrolleys =  0 ;
        schoolDetails.board = schoolSnapData.School_Details.board ;
        schoolDetails.contentLanguage = schoolSnapData.School_Details.contentLanguage;
		schoolDetails.location = {};
        schoolDetails.location.area =   schoolSnapData.School_Details.location.area;
		schoolDetails.location.state =  schoolSnapData.School_Details.location.state;
		schoolDetails.location.district =  schoolSnapData.School_Details.location.district;
		schoolDetails['Classes Covered'] = schoolSnapData.School_Details['Classes Covered'];
		
		var object = {
                        'AppIDs': {},
                        'School_Details': schoolDetails,
                        'appData': schoolSnapData.appData,
                        'Classes': schoolSnapData.Classes
                    }
		
         console.log(object);
		 
		 loginHandler.saveDetails('Tablab/counter/schools', 'Tablab/Schools', 'school', object, (error, schoolID) => {
                        if (error) {
                            alert('Error while saving data!');
                            //location.reload();
						}
						console.log("------------------------")
						console.log(schoolID)
						console.log(schoolDetails.ProjectID)
						var options = {
                            'id': schoolID,
                            'uid': uid,
                            'username': username,
                            'name': schoolDetails.schoolName,
                            'district': schoolDetails.location.district,
                            'state': schoolDetails.location.state
                        }
						
						loginHandler.setter(`Tablab/Projects/${schoolDetails.ProjectID}/Schools/${schoolID}`, {
                            'schoolName': schoolDetails.schoolName,
                            'location': schoolDetails.location
                        }, () => {
							 alert('School Created !');
						});
		 })
		
		}
		})
		  
	    
	  }
	 
 })