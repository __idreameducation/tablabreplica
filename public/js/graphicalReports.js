var app = {
    functions: new Functions(),
    did: "",
    reportType: "",
    CATEGORY_PIECHART: 1,
    CLASS_PIECHART: 2,
    SUBJECT_PIECHART: 3,
    filterDetails: {},

    initialize: function(startDate, endDate) {
        const instance = this;

        var piechartContainer1 = $("#piechart1");
        var piechartContainer2 = $("#piechart2");
        var piechartContainer3 = $("#piechart3");
		var barGraphContainer = $("#barGraph1");
		

        this.did = this.functions.getParameterByName("did");
        this.reportType = this.functions.getParameterByName("reportType") // "ContentUsageReports";

        // const forWhat = ["category_accessed", "className", "subject_of_content", "month_year"];
        const forWhat = ["contentCategory", "className", "subjectOfContent", "monthYear"];
        const queryString1 = `?did=${app.did}&reportType=${app.reportType}&forWhat=${forWhat[0]}&start=${startDate}&end=${endDate}`;
        const queryString2 = `?did=${app.did}&reportType=${app.reportType}&forWhat=${forWhat[1]}&start=${startDate}&end=${endDate}`;
        const queryString3 = `?did=${app.did}&reportType=${app.reportType}&forWhat=${forWhat[2]}&start=${startDate}&end=${endDate}`;
        const queryString4 = `?did=${app.did}&reportType=${app.reportType}&forWhat=${forWhat[3]}&start=${startDate}&end=${endDate}`;

        /**
         * on page load, requests for data to show in pie chart
         * This particular request is for the pie chart that will show Categorywise usage
         */
        this.requestForGraphData("/reports/graphicalReports" + queryString1, "piechart1", false, 'loader1');
        this.requestForGraphData("/reports/graphicalReports" + queryString2, "piechart2", false, 'loader2');
        this.requestForGraphData("/reports/graphicalReports" + queryString3, "piechart3", false, 'loader3');
        this.requestForGraphData("/reports/graphicalReports" + queryString4, "barGraph1", true, 'loader4');


        // back button functionality
        $('#backButton').on('click', (e) => {
            //   window.history.back();
        })
    },

    /**
     * Generates Donut shaped chart using generate function of billboard.js
     * 
     * @param {Object} nameList
     * @param {Object} timeList
     * @param {HTML Tag} graphElement
     */
    createPieChart: function(nameList, timeList, graphElement) {
        const instance = this;
        // generate the chart
        var piechart = bb.generate({
            bindto: '#' + graphElement,
            data: {
                type: "donut",
                rows: [
                    nameList,
                    timeList
                ],
                "onclick": function(d, i) {
                    var forWhat = "";
                    var selectedDataType = "";

                    /** 
                     * Extracting last character of the id of pie chart clicked
                     * ---------------------------- Note -------------------------------
                     * A negative start index slices the string from length+index, 
                     * to length, being index -1, the last character is extracted:
                     * "abc".slice(-1); // "c";
                     * -----------------------------------------------------------------
                     */

                    var chartSelected = +(($(i).closest('div').prop('id')).slice(-1));

                    switch (chartSelected) {
                        case instance.CATEGORY_PIECHART:
                            forWhat = "className";
                            selectedDataType = "contentCategory";
                            break;
                        case instance.CLASS_PIECHART:
                            forWhat = "contentCategory";
                            selectedDataType = "className";
                            break;
                        case instance.SUBJECT_PIECHART:
                            forWhat = "className";
                            selectedDataType = "subjectOfContent";
                            break;
                        default:
                            chartSelected = -1;
                            break;
                    }

                    // if chartSelected has value -1, 
                    // this indicates that selected chart a modal window chart
                    // => no need to drill down more
                    if (chartSelected !== -1) {

                        // Get Filter Dates for graph to be drilled down
                        const currentGraphFilterDetails = instance.filterDetails[graphElement];
                        const startDate = currentGraphFilterDetails.startDate;
                        const endDate = currentGraphFilterDetails.endDate;

                        // url to request drilled down graph data
                        const url = `/reports/graphicalReports?did=${instance.did}&reportType=${instance.reportType}&forWhat=${forWhat}&selectedData=${d.id}&selectedDataType=${selectedDataType}&start=${startDate}&end=${endDate}`;

                        // remove old pie-chart from the modal-window
                        $('#modal-pie-chart').html('Loading...');
                        $('#modal-pie-chart').parent().find('h3')
                            .html((forWhat === "className") ?
                                `Graph For ${d.id} (${d.value} hrs)` :
                                `Graph For Class ${d.id} (${d.value} hrs)`);

                        // show modal-window
                        instance.showModalPieChart();

                        // request data for modal-window pie-chart
                        instance.requestForGraphData(url, 'modal-pie-chart');
                    }
                }
            },

            color: {
                pattern: ["#ffb366", "#ff4d4d", "#e6e600", "#cc6600", "#ff99cc", "#ff6666", "#1a8cff", "#0073e6"]
            },

            donut: {
                width: 60,
                title: "% Usage",
                padAngle: 0.03
            }
        });

        piechart.resize({
            width: 500,
            height: 400
        });
    },

    /**
     * Generates BarChart using generate function of billboard.js
     * 
     * @param {Object} nameList
     * @param {Object} timeList
     * @param {HTML Tag} graphElement
     */
    createBarGraph: function(cumlative, category_wise, subject_wise, category_wise_group, subject_wise_group, graphElement) {
		
		console.log(cumlative)
		
        var barchart1 = bb.generate({
            data: {
                columns:[...subject_wise],
				type: "bar",
				groups : [subject_wise_group]
            },
            bar: {
                width: {
                    ratio: 0.5,
                    max: 340
				}
            },
            axis: {
                x: {
                    type: "category",
                    // label: "Your X-axis",
                    categories: cumlative.nameList
                }
            },

            bindto: '#barGraph3'
		});

		var barchart2 = bb.generate({
            data: {
                columns:[...category_wise],
				type: "bar",
				groups : [category_wise_group]
            },
            bar: {
                width: {
                    ratio: 0.5,
                    max: 340
				}
            },
            axis: {
                x: {
                    type: "category",
                    // label: "Your X-axis",
                    categories: cumlative.nameList
                }
            },

            bindto: '#barGraph2'
		});
		
		let cumlative_ = ["Monthly Usage",...cumlative.timeList]
		var barchart3 = bb.generate({
            data: {
                columns:[cumlative_],
				type: "bar",
            },
            bar: {
                width: {
                    ratio: 0.5,
                    max: 340
				}
            },
            axis: {
                x: {
                    type: "category",
                    // label: "Your X-axis",
                    categories: cumlative.nameList
                }
            },

            bindto: '#barGraph1'
		});
		



    },

    /**
     * 
     * @param {String} url 
     * @param {HTML Element} element
     * @returns {Void} void
     */
    requestForGraphData: function(url, element, isBarGraphRequired, loader) {
		
        const instance = this;
        if (loader)
            $(`#${loader}`).css('display', 'block');

        this.functions.sendRequest(url, "GET", null, function(error, data) {
            if (error) {
				 $("#dataDiv").css("display", "none");
				 $("#errorDiv").css("display", "");
                // show error in the area of graph
                /* $(`#${element}`).parent().html(`<div class="row form-group">
                                            <div class="col-md-4" style="display:block; margin:10px auto;">
                                                <label>Nothing To Show!</label>
                                                <img src="/images/nothingToShow.png" width="600" height="300"/>
                                            </div>
                                        </div>`); */
										

            } else {
                // hide spinner/loader
				$("#dataDiv").css("display", "");
				if (loader)
                    $(`#${loader}`).css('display', 'none');

                if (isBarGraphRequired) {
					
					let cumlative = instance.convertToArray(data.graphData.cumlative);
					//console.log(instance.convertToArray,cumlative);

					let category_wise = [];
					let subject_wise = [];
					let category_wise_group = [];
					let subject_wise_group = [];
					
					for(let key in data.graphData.category_wise)
					{
						category_wise_group.push(key);
						let category = instance.convertToArray(data.graphData.category_wise[key]);
						let category_array = [key,...category.timeList];
						category_wise.push(category_array);
					}
					for(let key in data.graphData.subject_wise)
					{	subject_wise_group.push(key)
						let subject = instance.convertToArray(data.graphData.subject_wise[key]);
						let subject_array = [key,...subject.timeList];
						subject_wise.push(subject_array);
					}
					
                    instance.createBarGraph(cumlative, category_wise,subject_wise,category_wise_group,subject_wise_group, element);
                } else {
                    // create graph with values 'data', in the area of graph
                    instance.createPieChart(data.nameList, data.timeList, element);
                }
            }
        })
    },

	convertToArray : function(JSONObject, nameList = [], timeList = []) {
		for (x in JSONObject) {
			nameList.push(x);
			timeList.push((JSONObject[x].toFixed(2)));
		}
		return {nameList,timeList}
	},

    showModalPieChart: function() {
        var $modal = $('.js-modal-pie');
        //            $modalPieChart = $modal.find('.modal-pie-chart');

        $modal.modal('show');
    },

    toggleRedMessage: function(id, message) {
        if (message)
            $(`#${id}`).css('display', 'block').text(message);
        else
            $(`#${id}`).css('display', 'none');
    },

    toggleLoader: function(id, show) {
        if (show)
            $(`#${id}`).css('display', 'block');
        else
            $(`#${id}`).css('display', 'none');
    },

    getFormattedDate: function(date) {
        return ("0000" + date.getFullYear()).slice(-4) + "-" + ("00" + (date.getMonth() + 1)).slice(-2) + "-" + ("00" + date.getDate()).slice(-2);
    },

    getLastWeekDate: function(currentDate) {
        const last = 6;
        return new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - last)
    },

    getLastXMonthsDate: function(currentDate, last) {
        return new Date(currentDate.getFullYear(), currentDate.getMonth() - last, currentDate.getDate())
    },

    getActiveGraph: function() {
        // selectors in jquery
        // https://www.w3schools.com/jquery/jquery_ref_selectors.asp
        return $('#myCarousel .item.active');
    }
}

$(function() {

	const LAST_1_MONTH = 1;
	 const LAST_1_WEEK = 2;
	 const LAST_6_MONTHS = 3;
     const FROM_BEGINNING = 4;
     const CUSTOM_DATE = 5;
     const TOTAL_GRAPHS = 4;

    var loader;
    var graphElement;

    var activeGraph = $('#myCarousel .item.active');
    var x = $(activeGraph).find('.chart');

    // load data for active grade in the list
    const activeGrade = $('ul>li.active');
    const gradeId = $(activeGrade).find('h3').text();
    const grade = $(activeGrade).find('span').text();

    // setting default value for "#from" and "#to" input fields
    // default value for time interval is past one year from today
    const today = new Date();
    const endDate = app.getFormattedDate(today);
    const date = app.getLastXMonthsDate(today, 1);
    const startDate = app.getFormattedDate(date);
    $('#from').val(startDate);
    $('#to').val(endDate);

    const charts = $('#myCarousel .item .chart');

    // var filterDetails = {};

    for (var i = 0; i < charts.length; i++) {
        const element = charts[i];

        app.filterDetails[$(element).attr('id')] = {
            "startDate": startDate,
            "endDate": endDate,
            "selectedIntervalOption": $($('#intervalOptions')[0][0]).text()
        }
    }
     console.log( app.filterDetails);
    // request for graphical data for the first time when page loads
    app.initialize(startDate, endDate);

    // on change of time interval
    $('#intervalOptions').on('change', (e) => {
        var date, startDate;

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        switch (+($(e.currentTarget).find(':selected').attr('id'))) {
            case FROM_BEGINNING:
                date = app.getLastXMonthsDate(today, 12);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_6_MONTHS:
                date = app.getLastXMonthsDate(today, 6);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_1_MONTH:
                date = app.getLastXMonthsDate(today, 1);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_1_WEEK:
                date = app.getLastWeekDate(today);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case CUSTOM_DATE:
                $('#from').prop('readonly', false).val("");
                $('#to').prop('readonly', false).val("");
                //          $('#go').prop('disabled', false);
                break;
            default:
                break;
        }
    });

    $('#go').on('click', (e) => {
        const startDate = $('#from').val();
        const endDate = $('#to').val();
         $("#errorDiv").css("display", "none");
        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        if (!startDate || !endDate) {
            app.toggleRedMessage('alertMessage', 'Dates can not be empty');
            return;
        } else if (startDate > endDate) {
            app.toggleRedMessage('alertMessage', 'Start date can not be greater than end date');
            return;
        }

        const activeGraph = $('#myCarousel .item.active');
        loader = $(activeGraph).find('.spinner');
        graphElement = $(activeGraph).find('.chart');
		const activeGraphId=activeGraph.attr('id')
          console.log(activeGraphId);
        //        console.log('txt', $('#intervalOptions').find(':selected'));
        //        console.log('txt', $($('#intervalOptions').find(':selected')).attr('id'));

        app.filterDetails[graphElement.attr('id')] = {
            "spinner": loader.attr('id'),
            "startDate": startDate,
            "endDate": endDate,
            "selectedIntervalOption": $($('#intervalOptions').find(':selected')).text()
        }

        const activeGraphIndex = $(activeGraph).index();

        //        const forWhat = ["category_accessed", "className", "subject_of_content", "month_year"];
        const forWhat = ["contentCategory", "className", "subjectOfContent", "monthYear"];
        const queryString = `?did=${app.did}&reportType=${app.reportType}&forWhat=${forWhat[activeGraphIndex]}&start=${startDate}&end=${endDate}`;
       if(activeGraphId === "graphDiv"){
		  app.requestForGraphData("/reports/graphicalReports" + queryString, graphElement.attr('id'), true, loader.attr('id'));
        
	  }else {
		   app.requestForGraphData("/reports/graphicalReports" + queryString, graphElement.attr('id'), false, loader.attr('id'));
      
	  }
	
	
	});


    $('#prev').on('click', (e) => {
        const activeGraph = $('#myCarousel .item.active');
        var activeGraphIndex = $(activeGraph).index();

        if (activeGraphIndex === 0)
            activeGraphIndex = TOTAL_GRAPHS;

        const prevElement = ($('#myCarousel .item').find('.chart'))[activeGraphIndex - 1];
        const prevElementId = $(prevElement).attr('id');
        const filterDetail = app.filterDetails[prevElementId];

        //        if (filterDetail) {
        $('#intervalOptions').val(filterDetail.selectedIntervalOption);

        if ((filterDetail.selectedIntervalOption).toLowerCase() === 'custom date') {
            $('#from').prop('readonly', false).val(filterDetail.startDate);
            $('#to').prop('readonly', false).val(filterDetail.endDate);
        } else {
            $('#from').prop('readonly', true).val(filterDetail.startDate);
            $('#to').prop('readonly', true).val(filterDetail.endDate);
        }
        //        }
    });

    $('#next').on('click', (e) => {
        const activeGraph = $('#myCarousel .item.active');
        var activeGraphIndex = $(activeGraph).index();

        if (activeGraphIndex === TOTAL_GRAPHS - 1)
            activeGraphIndex = -1;

        const prevElement = ($('#myCarousel .item').find('.chart'))[activeGraphIndex + 1];
        const prevElementId = $(prevElement).attr('id');
        const filterDetail = app.filterDetails[prevElementId];
   
        //        if (filterDetail) {
        $('#intervalOptions').val(filterDetail.selectedIntervalOption);
        if ((filterDetail.selectedIntervalOption).toLowerCase() === 'custom date') {
            $('#from').prop('readonly', false).val(filterDetail.startDate);
            $('#to').prop('readonly', false).val(filterDetail.endDate);
        } else {
            $('#from').prop('readonly', true).val(filterDetail.startDate);
            $('#to').prop('readonly', true).val(filterDetail.endDate);
        }
        //        }
    });
});