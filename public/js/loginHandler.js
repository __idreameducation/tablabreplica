// configuring firebase and admin accounts
  /* const config = {
    apiKey: "AIzaSyBwITllwXkUoycQsFkV-JrDgfc4OOYG8bc",
    authDomain: "tablab-786cc.firebaseapp.com",
    databaseURL: "https://tablab-786cc.firebaseio.com",
    projectId: "tablab-786cc",
    storageBucket: "tablab-786cc.appspot.com",
    messagingSenderId: "1020838244930"
};  */
      const config = {
    apiKey: "AIzaSyDPtezYy6GzmJ0M2nQw2Un3rm82Odc-LDU",
    authDomain: "tablabreplica.firebaseapp.com",
    databaseURL: "https://tablabreplica.firebaseio.com",
    projectId: "tablabreplica",
    storageBucket: "tablabreplica.appspot.com",
    messagingSenderId: "561139363651",
    appId: "1:561139363651:web:b1463473661f54e6"
  };  
   /* const config = {
    apiKey: "AIzaSyBwITllwXkUoycQsFkV-JrDgfc4OOYG8bc",
    authDomain: "tablab-786cc.firebaseapp.com",
    databaseURL: "https://tablab-786cc.firebaseio.com",
    projectId: "tablab-786cc",
    storageBucket: "tablab-786cc.appspot.com",
    messagingSenderId: "1020838244930"
}; */
 

firebase.initializeApp(config);

var Handler = function() {
    this.CheckUserState();
}

Handler.prototype.isLoginBtnClicked = false;
Handler.prototype.backendInitialRoute = "";
Handler.prototype.count = 0;
Handler.prototype.databaseRef = firebase.database().ref();
Handler.prototype.functions = new Functions();

Handler.prototype.login = function(email, password, backendInitialRoute) {
    this.isLoginBtnClicked = true;
    this.backendInitialRoute = backendInitialRoute;
    return firebase.auth().signInWithEmailAndPassword(email, password);
}

Handler.prototype.getUserInfo = function(userId, done) {
    firebase.database().ref().child(`users/${userId}`)
        .once("value", snap => {
			console.log(snap.val())
            done(snap.val());
        })
}

Handler.prototype.CheckUserState = function() {
    const instance = this;
    firebase.auth().onAuthStateChanged(function(user) {
        if (!instance.isLoginBtnClicked)
            return;

        if (instance.count > 0) return;
        instance.count++;
          console.log(instance.count);
        if (user) {
			console.log(user);
			console.log(user.uid);
            instance.getUserInfo(user.uid, userInfo => {
				console.log(userInfo);
                if (userInfo) {
                    userInfo.uid = user.uid;
					console.log(userInfo.uid);
                    instance.functions.sendRequest(`/${instance.backendInitialRoute}/login`, "POST", userInfo, (action) => {
                        if (action.error) {
                            console.log("------------------ error --------------------", action.error)
                        }

                        location.href = action.url;
                    });
                } else alert('Something is wrong!');
            });
        } else {
            // wrong credentials
            alert("Wrong Credentials!");
        }
    });

    /**
     * 
     * @param {String} str 
     * @returns {String} str
     */
    var toTitleCase = function(str) {
        return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
    }
}

Handler.prototype.saveProjectDetails = function(projectDetails, done) {
    var instance = this;
    var projectID = '';
    instance.databaseRef.child(`Tablab/counter/projects`).once('value', (projectCount) => {
        var count = projectCount.child('count').val();
        projectID = 'project' + (++count);

        instance.databaseRef.child(`Tablab/Projects/${projectID}`)
            .set(projectDetails, (error) => {
                if (error) {
                    done(error);
                } else {
                    instance.databaseRef.child(`Tablab/counter/projects/count`)
                        .set(count, (error) => {
                            done(error, projectID);
                        });
                }
            });
    });
}

// To save School and Project Details
Handler.prototype.saveDetails = function(path1, path2, nodeIdInitials, dataToSave, done) {
    var instance = this;
    var nodeID = '';
    instance.databaseRef.child(path1).once('value', (countSnap) => {
        var count = countSnap.child('count').val();
        nodeID = nodeIdInitials + (++count);

        instance.databaseRef.child(`${path2}/${nodeID}`)
            .set(dataToSave, (error) => {
                if (error) {
                    done(error);
                } else {
                    instance.databaseRef.child(`${path1}/count`)
                        .set(count, (error) => {
                            done(error, nodeID);
                        });
                }
            });
    });
}

// // To save Class Details
// Handler.prototype.saveDetails = function(path, dataToSave, done) {
//     var instance = this;
//     instance.databaseRef.child(`${path}`)
//         .set(dataToSave, (error) => {
//             done(error);
//         });
// }

Handler.prototype.getter = function(path, done) {
    this.databaseRef.child(path).once('value', (snap) => {
        done(snap);
    });
};

Handler.prototype.setter = function(path, data, done) {
    this.databaseRef.child(path).set(data).then(() => {
        done();
    });
}

/**
 * 
 * @param {String} path 
 * @param {File} file 
 * @param {function} done 
 */
Handler.prototype.uploadFile = function(path, file, done) {
    if (!file || file.size <= 0) {
        done(null, '');
        return;
    }

    firebase.storage().ref().child(`${path}/${file.name}`).put(file).then((snap) => {
        if (!snap) {
            var error = {
                message: "Error while uploading file"
            }
            done(error);
        }
        done(null, snap.downloadURL);
    })
}