var admin;;
var databaseRef;

const iPrepModel = function iPrepModel() {};

iPrepModel.prototype.init = function(firebaseAdmin) {
    admin = firebaseAdmin;
    databaseRef = admin.database().ref();
}

iPrepModel.prototype.getter = function(path, done) {
    databaseRef.child(path)
        .once('value', snap => {
            done(snap);
        });
}

iPrepModel.prototype.getter2 = function(path, x, done) {
    if (x) {
        databaseRef.child(path)
            .orderByChild('enabled')
            .equalTo(x)
            .once('value', snap => {
                done(snap);
            });
    } else {
        databaseRef.child(path)
            .once('value', snap => {
                done(snap);
            });
    }
}

/**
 * 	purpose: create user and send confirmation callback to the admin for updating database.
 * this function take input from controller , this inputs are username, email , mobile number.
 *	output:-> callback to the controller admin.js for adding new user in database.
 *	
 *	Input :- user information, send by admin.
 *	output :- return callback of successfully created user.
 *	
 *	Dependencies:- admin.auth().createUser(),
 *				   admin.database().update()
 */

iPrepModel.prototype.createUser = function(user, done) {
    var pass = createPassword();
    admin.auth().createUser({
        email: user.userEmail,
        emailVerified: false,
        password: pass,
        displayName: user.firstName + " " + user.lastName
    }).then(function(userRecord) {
        var clientRef = databaseRef.child("BackendData/users/" + userRecord.uid);
        var updatedUserRecord = {
            name: userRecord.displayName,
            email: userRecord.email,
            type: "client",
            date: Date.now(),
            phoneNumber: (user.mobileNum) ? user.mobileNum : '',
            filters: user.filters,
            active: false
        };

        clientRef.update(updatedUserRecord).then(status => {
            done(null, userRecord, pass)
            console.log("Successfully created new user:", userRecord.uid);
        }).catch((err) => {
            console.log(err)
        });
    }).catch(function(error) {
        // console.log("Error during creating new user:", error);
        done(error)
    });
}

iPrepModel.prototype.deleteUser = function(uid, done) {
    // delete the user
    admin.auth().deleteUser(uid).then(function() {
        console.log("Successfully deleted user");
        new iPrepModel().setter(`BackendData/users/${uid}`, {}, (error) => {
            done(error);
        })
    }).catch(function(error) {
        done(error);
    });
}

iPrepModel.prototype.checkRegisterStatus = function(uid, done) {
    var userRef = databaseRef.child("BackendData/users/" + uid);
    userRef.once("value", function(snap) {
        var user = snap.val()
        if (user && user.active == false) {
            var obj = {};
            obj["active"] = true;
            userRef.update(obj).then(status => {
                done(user.email)
            }).catch(error => {
                done(false);
            });
        } else {
            done(false);
        }
    })
}

/** 
 * update password and store in database.
 * Inputs :- userId, new password
 * Output :- return callback of successful operation/error.
 */
iPrepModel.prototype.sendVerification = function(uid, password, done) {
    if (uid && password) {
        admin.auth().updateUser(uid, {
            emailVerified: true,
            password: password
        }).then(function(userRecord) {
            done(null, userRecord.toJSON())
        }).catch(function(error) {
            done(error)
        });
    } else {
        done("something is missing")
    }
}

/* Uses current system date as the value of password */
function createPassword() {
    var password = Date.now();
    return password + "";
}

module.exports = iPrepModel;