# 1.1.1 release (2017-09-22)

## Bug Fixes :

- **Shape.line**
	- Correct return type (#148) ([#147](https://github.com/naver/billboard.js/issues/147))
