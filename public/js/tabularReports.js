var App = function App() {
    this.tableDiv = $('#tableDiv');
    this.usageTableBody = $('#usageTableBody');
    this.assessmentUsageTableBody = $('#assessmentsUsageTableBody');
    this.summaryTableBody = $('#summaryTableBody');
    this.usageTableBottomInfo = $('#usageTableBottomInfo');
    this.assessmentsTableBottomInfo = $('#assessmentTableBottomInfo');
    this.startDate;
    this.endDate;
    this.did = $('#did').text();
    this.currentGrade = "";
}

App.prototype.functions = new Functions();

App.prototype.init = function(grade, gradeId, startDate, endDate) {
    this.currentGrade = grade;

    this.getGradeWiseUsageData(`/reports/getClassWiseCategoryUsageData?did=${this.did}&grade=${grade}&start=${startDate}&end=${endDate}`);
    this.getGradeWiseAssessmentsUsageData(`/reports/getClassWiseAssessmentsUsageData?did=${this.did}&grade=${grade}&start=${startDate}&end=${endDate}`);
    this.getGradeDetails(`/reports/getGradeDetails?did=${this.did}&gradeId=${gradeId}`);
}

App.prototype.getGradeWiseUsageData = function(url) {
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function(error, data) {
        // hide spinner/loader
        instance.toggleLoader('loader2', false);

        $('html, body').animate({
            scrollTop: $("#summaryTableBody").offset().top
        }, 800);

        if (data) {
            instance.showGradeUsageDataInTable(data);
            return;
        }
        instance.tableDiv.html(error);
    })
}

/**
 * Note: table row creation can be generic for any number of categories.
 * But for now keeping it hardcoded for 5 categories
 * 
 * @param {Object} data
 */
App.prototype.showGradeUsageDataInTable = function(data) {

    // Using only single table element in the html,
    // so need to clean old rows from the table
    $('#usageTableBody tr').css("display", "none");

    var length = 0;
    // create row with classwise values from data object
    for (key in data.cumulativeUsage) {
        if (data.cumulativeUsage.hasOwnProperty(key)) {
            length++;
            const element = data.cumulativeUsage[key];

            var row = $('<tr></tr>').attr("style", "color:black;");
            const studentName = $('<td></td>').attr("align", "center").text(key);
            var multimedia = $('<td></td>').attr("align", "center");
            var books = $('<td></td>').attr("align", "center");
            var apps = $('<td></td>').attr("align", "center");
            var videos = $('<td></td>').attr("align", "center");
            var assessmentApp = $('<td></td>').attr("align", "center");

            if (element.multimedia)
                multimedia.text((element.multimedia).toFixed(1));
            else
                multimedia.text('-');

            if (element.videos)
                videos.text((element.videos).toFixed(1));
            else
                videos.text('-');

            if (element.apps)
                apps.text((element.apps).toFixed(1));
            else
                apps.text('-');

            if (element.books)
                books.text((element.books).toFixed(1));
            else
                books.text('-');

            if (element.assessments)
                assessmentApp.text((element.assessments).toFixed(1));
            else
                assessmentApp.text('-');

            row.append(studentName);
            row.append(apps);
            row.append(assessmentApp);
            row.append(books);
            row.append(multimedia);
            row.append(videos);

            this.usageTableBody.append(row);
        }
    };
    this.usageTableBottomInfo.html('');
    this.usageTableBottomInfo.html((+length) + " out of " + (+length));
}

// ----------------------------------------------------------------------------------------- //

App.prototype.getGradeWiseAssessmentsUsageData = function(url) {
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function(error, data) {
        // hide spinner/loader
        instance.toggleLoader('loader3', false);
        if (data) {
            instance.showAssessmentsDataInTable(data);
            return;
        }

        instance.tableDiv.html(error);
    })
}

App.prototype.showAssessmentsDataInTable = function(data) {
    // Using only same table elements for all the grades in the html,
    // so need to clean old rows from the table
    $('#assessmentsUsageTableBody tr').css("display", "none");

    var length = 0;
    const ASSESSMENT_TYPE_PRACTICE = 'Practice';
    const ASSESSMENT_TYPE_LOCKED_TERM = 'LockedTerm';
    const ASSESSMENT_TYPE_UNLOCKED_TERM = 'UnlockedTerm';

    // create row with classwise values from data object
    for (userId in data.cumulativeUsage) {
        if (data.cumulativeUsage.hasOwnProperty(userId)) {
            const element = data.cumulativeUsage[userId];
            var row = $('<tr></tr>').attr("style", "color:black;").attr('id', userId);
            const studentName = $('<td></td>').attr("align", "center").text(element.userName);
            row.append(studentName);

            var usage = element.usage;
            // for (assessmentType in usage) {

            var count1 = $('<td></td>').attr("align", "center");
            var average1 = $('<td></td>').attr("align", "center");

            var count2 = $('<td></td>').attr("align", "center");
            var average2 = $('<td></td>').attr("align", "center");

            var count3 = $('<td></td>').attr("align", "center");
            var average3 = $('<td></td>').attr("align", "center");

            if (usage.hasOwnProperty(ASSESSMENT_TYPE_PRACTICE)) {
                count1.text(usage[ASSESSMENT_TYPE_PRACTICE]['count']);
                // average1.text(usage[ASSESSMENT_TYPE_PRACTICE]['average']);
                const averageText = 100 * (usage[ASSESSMENT_TYPE_PRACTICE]['totalScore'] / usage[ASSESSMENT_TYPE_PRACTICE]['totalQuestions']);
                average1.text(averageText.toFixed(1) + "%");
            } else {
                count1.text('0');
                average1.text('0');
            }

            if (usage.hasOwnProperty(ASSESSMENT_TYPE_LOCKED_TERM)) {
                count2.text(usage[ASSESSMENT_TYPE_LOCKED_TERM]['count']);
                // average2.text(usage[ASSESSMENT_TYPE_LOCKED_TERM]['average']);
                const averageText = 100 * (usage[ASSESSMENT_TYPE_LOCKED_TERM]['totalScore'] / usage[ASSESSMENT_TYPE_LOCKED_TERM]['totalQuestions']);
                average2.text(averageText.toFixed(1) + "%");
            } else {
                count2.text('0');
                average2.text('0');
            }

            if (usage.hasOwnProperty(ASSESSMENT_TYPE_UNLOCKED_TERM)) {
                count3.text(usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['count']);
                // average3.text(usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['average']);
                const averageText = 100 * (usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['totalScore'] / usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['totalQuestions']);
                average3.text(averageText.toFixed(1) + "%");
            } else {
                count3.text('0');
                average3.text('0');
            }

            row.append(count1);
            row.append(average1);
            row.append(count2);
            row.append(average2);
            row.append(count3);
            row.append(average3);
            // }

            // click listener to the per student assessments usage row
            row.on('click', (e) => {
                e.preventDefault();
                const uid = $('#uid').text();
                const userDisplayName = $('#siteUser').text().trim();
                $('#assessmentUsageTableBody tr').attr("style", "background-color: transparent");
                $(e.currentTarget).attr("style", "background-color:#41acf4;color:white;");

                const studentName = $($(e.currentTarget).find('td')[0]).html();
                var nextURL = `/reports/studentWiseAssessmentsDetailsPage?u=${uid}&n=${userDisplayName}&did=${this.did}&studentID=${e.currentTarget.id}&studentName=${studentName}`;
                onNextPageLinkSelected(nextURL);
            });

            // hover listener to the per student assessments usage row
            row.hover((e) => {
                e.preventDefault();
                $('#assessmentUsageTableBody tr').attr("style", "background-color: transparent");
                $(e.currentTarget).attr("style", "background-color:#41acf4; color:white;");

            }, (e) => {
                e.preventDefault();
                $(e.currentTarget).attr("style", "background-color:transparent;");
            });


            this.assessmentUsageTableBody.append(row);
            length++;
        }
    };
    this.assessmentsTableBottomInfo.html('');
    this.assessmentsTableBottomInfo.html((+length) + " out of " + (+length));
}


// navigation
var currentURL = window.location.href;
var navTitles = (currentURL.split("#")[1]) ? ((currentURL.split("#")[1]).replace(/%20/g, " ")).split(',') : [];

function onNextPageLinkSelected(url) {  
    var nextPageURL =  url + "#";
    for (var index = 0; index < navTitles.length; index++) {
        nextPageURL += navTitles[index] + ",";
    }
    
    window.location = nextPageURL + ($('title').html().split("-")[1]).trim();
}

// ----------------------------------------------------------------------------------------- //


App.prototype.getGradeDetails = function(url) {
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function(error, data) {
        if (error) {
            // hide spinner/loader
            instance.toggleLoader('loader1', false);
            $('#summaryTableBody tr').css("display", "none");
            $('#summaryTableBody').html($(`<span>${error.error.message}</span>`));
        } else {
            // Using only single table element in the html,
            // so need to clean old rows from the table
            $('#summaryTableBody tr').css("display", "none");
            $('#summaryTableBody span').css("display", "none");

            // hide spinner/loader
            instance.toggleLoader('loader1', false);

            for (key in data.gradeDetails) {
                if (data.gradeDetails.hasOwnProperty(key)) {
                    const value = data.gradeDetails[key];
                    var row = $('<tr></tr>');
                    var tdKey = $(`<td><b>${key}</b></td>`);
                    var tdValue = instance.functions.create_td(value);
                    row.append(tdKey)
                    row.append(tdValue);
                    instance.summaryTableBody.append(row);
                }
            }
        }
    });
}

App.prototype.getFormattedDate = function(date) {
    return ("0000" + date.getFullYear()).slice(-4) + "-" + ("00" + (date.getMonth() + 1)).slice(-2) + "-" + ("00" + date.getDate()).slice(-2);
}

App.prototype.getLastWeekDate = function(currentDate) {
    const last = 6;
    return new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - last)
}

App.prototype.getLastXMonthsDate = function(currentDate, last) {
    return new Date(currentDate.getFullYear(), currentDate.getMonth() - last, currentDate.getDate())
}

App.prototype.toggleRedMessage = function(id, message) {
    if (message)
        $(`#${id}`).css('display', 'block').text(message);
    else
        $(`#${id}`).css('display', 'none');
}

App.prototype.toggleLoader = function(id, show) {
    if (show)
        $(`#${id}`).css('display', 'block');
    else
        $(`#${id}`).css('display', 'none');
}

$(function() {
    const FROM_BEGINNING = 1;
    const LAST_6_MONTHS = 2;
    const LAST_1_MONTH = 3;
    const LAST_1_WEEK = 4;
    const CUSTOM_DATE = 5;

    const app = new App();

    // load data for active grade in the list
    const activeGrade = $('.scrollmenu>a.active');
    // const gradeId = $(activeGrade).find('h3').text();
    const grade = $(activeGrade).find('span').text();

    // setting default value for "#from" and "#to" input fields
    // default value for time interval is, last one year from today
    const today = new Date();
    const endDate = app.getFormattedDate(today);
    const date = app.getLastXMonthsDate(today, 12);
    const startDate = app.getFormattedDate(date);
    $('#from').val(startDate);
    $('#to').val(endDate);

    // request for grade wise usage data for the first time when page loads
    app.init(grade, grade, startDate, endDate);

    // on selected any grade every time
    this.classSelected = function(el) {
        // const gradeId = $(el).parent().find('h3').text();

        // remove Class active from previous active link 
        $($(el).parent().find('.active')).removeClass('active');

        // set current element as active
        $(el).addClass('active');

        const grade = $(el).find('span').text();

        $('#intervalOptions').val($('#1').val());
        $('#from').prop('readonly', true).val(startDate);
        $('#to').prop('readonly', true).val(endDate);

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        // init
        app.init(grade, grade, $('#from').val(), $('#to').val());
    }

    $('#download')
        .css('display', 'block')
        .on('click', (e) => {
            location.href = `/reports/downloadUsageReportsByStudents?type=ContentUsageReports&schoolID=${app.did}&grade=${app.currentGrade}&start=${$('#from').val()}&end=${$('#to').val()}`;
            //            location.href = '/downloadReport?type=Studentwise Category Usage';
        })

    // on change of time interval
    $('#intervalOptions').on('change', (e) => {
        var date, startDate;

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        switch (+($(e.currentTarget).find(':selected').attr('id'))) {
            case FROM_BEGINNING:
                date = app.getLastXMonthsDate(today, 12);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_6_MONTHS:
                date = app.getLastXMonthsDate(today, 6);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_1_MONTH:
                date = app.getLastXMonthsDate(today, 1);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_1_WEEK:
                date = app.getLastWeekDate(today);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case CUSTOM_DATE:
                $('#from').prop('readonly', false).val("");
                $('#to').prop('readonly', false).val("");
                //          $('#go').prop('disabled', false);
                break;
            default:
                break;
        }
    });

    $('#go').on('click', (e) => {
        const startDate = $('#from').val();
        const endDate = $('#to').val();

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        if (!startDate || !endDate) {
            app.toggleRedMessage('alertMessage', 'Dates can not be empty');
            return;
        } else if (startDate > endDate) {
            app.toggleRedMessage('alertMessage', 'Start date can not be greater than end date');
            return;
        }

        // Grade wise content usage data request
        app.toggleLoader('loader2', true);
        app.getGradeWiseUsageData(`/reports/getClassWiseCategoryUsageData?did=${app.did}&grade=${app.currentGrade}&start=${startDate}&end=${endDate}`);

        // Grade wise assessments usage data request
        app.toggleLoader('loader3', true);
        app.getGradeWiseAssessmentsUsageData(`/reports/getClassWiseAssessmentsUsageData?did=${app.did}&grade=${app.currentGrade}&start=${startDate}&end=${endDate}`);
    });
});