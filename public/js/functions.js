var Functions = function Functions() {};

Functions.prototype.sendRequest = function(url, requestMethod, extraInfo, response) {
	var request = new XMLHttpRequest();
	console.log(url)
    request.open(requestMethod, url);
    if (!extraInfo) { // GET
        request.send();
        request.addEventListener('load', function(event) {
            if (request.status !== 200 ||
                (request.status === 200 && JSON.parse(request.responseText).error)) {
                var error = JSON.parse(request.responseText)
                response(error);
            } else {
                var data;
				if (request.responseText)
				{
					data = JSON.parse(request.responseText);
				}
				
                response(null, data);
            }
        });
    } else { // POST and (GET with extraInfo)
        request.setRequestHeader("Content-type", "application/json");
        request.send(JSON.stringify(extraInfo));
        request.addEventListener('load', function(event) {
            if (response) {
                response(JSON.parse(request.responseText));
            }
        });
    }
}

Functions.prototype.create_td = function(data) {
    const td = $('<td></td>');
    return ((data) ? td.text(data) : td.text('-'));
}

/**
 * 
 * @param {String} str 
 * @returns {String} str
 */
Functions.prototype.toTitleCase = function(str) {
    return str.charAt(0).toUpperCase() + str.substr(1).toLowerCase();
}

Functions.prototype.getParameterByName = function(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function goBack() {
    window.history.back();
}