var states = [{
        "code": "AN",
        "name": "Andaman and Nicobar Islands",
        "districts": [
            'Nicobar', 'North and Middle Andaman', 'South Andaman'
        ]
    },
    {
        "code": "AP",
        "name": "Andhra Pradesh",
        "districts": [
            'Anantapur', 'Chittoor', 'East Godavari', 'Guntur', 'Cuddapah', 'Krishna', 'Kurnool',
            'Prakasam', 'Nellore', 'Srikakulam', 'Vishakhapatnam', 'Vizianagaram', 'West Godavari'
        ]
    },
    {
        "code": "AR",
        "name": "Arunachal Pradesh",
        "districts": [
            'Changlang District', 'Tirap District', 'Anjaw District', 'Lohit District', 'Upper Dibang Valley',
            'Lower Dibang Valley', 'West Siang District', 'East Siang District', 'Lower Subansiri District',
            'Upper Siang District', 'Kurung Kumey District', 'Upper Subansiri District', 'East Kameng District',
            'Papum Pare District', 'Tawang District', 'West Kameng District'
        ]
    },
    {
        "code": "AS",
        "name": "Assam",
        "districts": [
            'Tinsukia', 'Dibrugarh', 'Dhemaji', 'Charaideo', 'Sivasagar', 'Lakhimpur', 'Majuli', 'Jorhat', 'Biswanath', 'Golaghat',
            'Karbi Anglong East', 'Sonitpur', 'Nagaon', 'Hojai', 'Karbi Anglong West', 'Dima Hassao', 'Cachar', 'Hailakandi', 'Karimganj',
            'Morigaon', 'Udalguri', 'Darrang', 'Kamrup Metro', 'Baksa', 'Nalbari', 'Kamrup', 'Barpeta', 'Chirang', 'Bongaigaon',
            'Goalpara', 'Kokrajhar', 'Dhubri', 'South Salmara-Mankachar'
        ]
    },
    {
        "code": "BR",
        "name": "Bihar",
        "districts": [
            'Araria', 'Arwal', 'Aurangabad', 'Banka', 'Begusarai', 'Bhabhua(Kaimur)', 'Bhagalpur', 'Bhojpur', 'Buxar', 'Darbhanga', 'East Champaran',
            'Gaya', 'Gopalganj', 'Jamui', 'Jehanabad', 'Katihar', 'Khagaria', 'Kishanganj', 'Lakhisarai', 'Madhepura', 'Madhubani', 'Munger', 'Muzaffarpur',
            'Nalanda', 'Nawada', 'Patna', 'Purnea', 'Rohtas', 'Saharsa', 'Samastipur', 'Saran', 'Sheikhpura', '', 'Sheohar', 'Sitamarhi',
            'Siwan', 'Supaul', 'Vaishali', 'West Champaran'
        ]
    },
    {
        "code": "CG",
        "name": "Chandigarh",
        "districts": ['Chandigarh']
    },
    {
        "code": "CH",
        "name": "Chhattisgarh",
        "districts": [
            "Raipur", "Bhilai Nagar", "Korba", "Bilaspur", "Durg", "Rajnandgaon", 
            "Jagdalpur", "Raigarh", "Ambikapur", "Mahasamund", "Dhamtari", "Chirmiri", "Bhatapara",
            "Dalli-Rajhara", "Naila Janjgir", "Tilda Newra", "Mungeli", "Manendragarh", "Sakti"]
    },
    {
        "code": "DH",
        "name": "Dadra and Nagar Haveli",
        "districts": ["Silvassa"]
    },
    {
        "code": "DD",
        "name": "Daman and Diu",
        "districts": ["Daman", "Diu"]
    },
    {
        "code": "DL",
        "name": "Delhi",
        "districts": ["Delhi", "New Delhi"]
    },
    {
        "code": "GA",
        "name": "Goa",
        "districts": ['North Goa', 'South Goa']
    },
    {
        "code": "GJ",
        "name": "Gujarat",
        "districts": [
            'Ahmedabad', 'Vadodara', 'Anand', 'Chhota Udaipur', 'Dahod', 'Kheda', 'Mahisagar', 'Panchmahal', 'Gandhinagar', 'Aravalli', 'Banaskantha',
            'Mehsana', 'Patan', 'Sabarkantha', 'Rajkot', 'Amreli', 'Bhavnagar', 'Botad', 'Devbhoomi Dwarka', 'Gir Somnath', 'Jamnagar', 'Junagadh',
            'Morbi', 'Porbandar', 'Surendranagar', 'Kachchh', 'Surat', 'Bharuch', 'Dang', 'Narmada', 'Navsari', 'Tapi', 'Valsad'
        ]
    },
    {
        "code": "HR",
        "name": "Haryana",
        "districts": [
            'Ambala', 'Bhiwani', 'Charkhi Dadri', 'Faridabad', 'Fatehabad', 'Gurgaon', 'Hisar',
            'Jhajjar', 'Jind', 'Kaithal', 'Karnal', 'Kurukshetra', 'Mahendragarh', 'Mewat',
            'Palwal', 'Panchkula', 'Panipat', 'Rewari', 'Rohtak', 'Sirsa', 'Sonipat', 'Yamunanagar'
        ]
    },
    {
        "code": "HP",
        "name": "Himachal Pradesh",
        "districts": [
            'Hamirpur', 'Kangra', 'Bilaspur', 'Mandi', 'Chamba', 'Una', 'Sirmaur', 
            'Lahul and Spiti', 'Kullu', 'Kinnaur', 'Shimla', 'Solan'
        ]
    },
    {
        "code": "JK",
        "name": "Jammu and Kashmir",
        "districts": [
            'Anantnag', 'Badgam', 'Bandipora', 'Baramulla', 'Doda', 'Ganderbal', 'Jammu', 'Kargil', 'Kathua', 'Kishtwar', 'Kulgam', 'Kupwara',
            'Leh', 'Poonch', 'Pulwama', 'Rajouri', 'Ramban', 'Reasi', 'Samba', 'Shopian', 'Srinagar', 'Udhampur'
        ]
    },
    {
        "code": "JH",
        "name": "Jharkhand",
        "districts": ["Dhanbad", "Ranchi", "Jamshedpur", "Bokaro Steel City", "Deoghar", "Phusro",
            "Adityapur", "Hazaribag", "Giridih", "Ramgarh", "Jhumri Tilaiya", "Saunda",
            "Sahibganj", "Medininagar (Daltonganj)", "Chaibasa", "Chatra", "Gumia", "Dumka",
            "Madhupur", "Chirkunda", "Pakaur", "Simdega", "Musabani", "Mihijam",
            "Patratu", "Lohardaga", "Tenu dam-cum-Kathhara"
        ]
    },
    {
        "code": "KA",
        "name": "Karnataka",
        "districts": [
            "Bengaluru","Hubli-Dharwad","Belagavi","Mangaluru","Davanagere",
            "Ballari","Mysore","Tumkur","Shivamogga","Raayachuru","Robertson Pet",
            "Kolar","Mandya","Udupi","Chikkamagaluru","Karwar","Ranebennuru",
            "Ranibennur","Ramanagaram","Gokak","Yadgir","Rabkavi Banhatti","Shahabad",
            "Sirsi","Sindhnur","Tiptur","Arsikere","Nanjangud","Sagara","Sira","Puttur",
            "Athni","Mulbagal","Surapura","Siruguppa","Mudhol","Sidlaghatta","Shahpur",
            "Saundatti-Yellamma","Wadi","Manvi","Nelamangala","Lakshmeshwar","Ramdurg",
            "Nargund","Tarikere","Malavalli","Savanur","Lingsugur","Vijayapura","Sankeshwara",
            "Madikeri","Talikota","Sedam","Shikaripur","Mahalingapura","Mudalagi","Muddebihal",
            "Pavagada","Malur","Sindhagi","Sanduru","Afzalpur","Maddur","Madhugiri","Tekkalakote",
            "Terdal","Mudabidri","Magadi","Navalgund","Shiggaon","Shrirangapattana","Sindagi",
            "Sakaleshapura","Srinivaspur","Ron","Mundargi","Sadalagi","Piriyapatna","Adyar"
        ]
    },
    {
        "code": "KL",
        "name": "Kerala",
        "districts": [
            "Thiruvananthapuram","Kochi","Kozhikode","Kollam","Thrissur",
            "Palakkad","Alappuzha","Malappuram","Ponnani","Vatakara","Kanhangad",
            "Taliparamba","Koyilandy","Neyyattinkara","Kayamkulam","Nedumangad",
            "Kannur","Tirur","Kottayam","Kasaragod","Kunnamkulam","Ottappalam",
            "Thiruvalla","Thodupuzha","Chalakudy","Changanassery","Punalur","Nilambur",
            "Cherthala","Perinthalmanna","Mattannur","Shoranur","Varkala","Paravoor",
            "Pathanamthitta","Peringathur","Attingal","Kodungallur","Pappinisseri",
            "Chittur-Thathamangalam","Muvattupuzha","Adoor","Mavelikkara","Mavoor",
            "Perumbavoor","Vaikom","Palai","Panniyannur","Guruvayoor","Puthuppally","Panamattom"
        ]
    },
    {
        "code": "LD",
        "name": "Lakshadweep",
        "districts": ["Lakshadweep"]
    },
    {
        "code": "MP",
        "name": "Madhya Pradesh",
        "districts": [
            "Indore","Bhopal","Jabalpur","Gwalior","Ujjain","Sagar","Ratlam",
            "Satna","Murwara (Katni)","Morena","Singrauli","Rewa","Vidisha","Ganjbasoda",
            "Shivpuri","Mandsaur","Neemuch","Nagda","Itarsi","Sarni","Sehore","Mhow Cantonment",
            "Seoni","Balaghat","Ashok Nagar","Tikamgarh","Shahdol","Pithampur","Alirajpur",
            "Mandla","Sheopur","Shajapur","Panna","Raghogarh-Vijaypur","Sendhwa","Sidhi",
            "Pipariya","Shujalpur","Sironj","Pandhurna","Nowgong","Mandideep","Sihora",
            "Raisen","Lahar","Maihar","Sanawad","Sabalgarh","Umaria","Porsa","Narsinghgarh",
            "Malaj Khand","Sarangpur","Mundi","Nepanagar","Pasan","Mahidpur","Seoni-Malwa",
            "Rehli","Manawar","Rahatgarh","Panagar","Wara Seoni","Tarana","Sausar","Rajgarh",
            "Niwari","Mauganj","Manasa","Nainpur","Prithvipur","Sohagpur","Nowrozabad (Khodargama)",
            "Shamgarh","Maharajpur","Multai","Pali","Pachore","Rau","Mhowgaon","Vijaypur","Narsinghgarh"
        ]
    },
    {
        "code": "MH",
        "name": "Maharashtra",
        "districts": ["Ahmednagar","Akola","Amravati","Aurangabad","Beed","Bhandara","Buldhana","Chandrapur","Dhule","Gadchiroli","Gondia",
						"Hingoli","Jalgaon","Jalna","Kolhapur","Latur","Mumbai City","Mumbai Suburban","Nagpur","Nanded","Nandurbar","Nashik",
						"Osmanabad","Palghar","Parbhani","Pune","Raigad","Ratnagiri","Sangli","Satara","Sindhudurg","Solapur","Thane","Wardha",
						"Washim","Yavatmal"]
    },
    {
        "code": "MN",
        "name": "Manipur",
        "districts": ["Imphal", "Thoubal", "Lilong", "Mayang Imphal"]
    },
    {
        "code": "ML",
        "name": "Meghalaya",
        "districts": ["Shillong", "Tura", "Nongstoin"]
    },
    {
        "code": "MZ",
        "name": "Mizoram",
        "districts": ["Aizawl", "Lunglei", "Saiha"]
    },
    {
        "code": "NL",
        "name": "Nagaland",
        "districts": ["Dimapur", "Kohima", "Zunheboto", "Tuensang", "Wokha", "Mokokchung"]
    },
    {
        "code": "OR",
        "name": "Odisha",
        "districts": [
            "Bhubaneswar","Cuttack","Raurkela","Brahmapur","Sambalpur","Puri",
            "Baleshwar Town","Baripada Town","Bhadrak","Balangir","Jharsuguda","Bargarh",
            "Paradip","Bhawanipatna","Dhenkanal","Barbil","Kendujhar","Sunabeda","Rayagada",
            "Jatani","Byasanagar","Kendrapara","Rajagangapur","Parlakhemundi","Talcher","Sundargarh",
            "Phulabani","Pattamundai","Titlagarh","Nabarangapur","Soro","Malkangiri","Rairangpur",
            "Tarbha"
        ]
    },
    {
        "code": "PY",
        "name": "Puducherry",
        "districts": ["Pondicherry", "Karaikal", "Yanam", "Mahe"]
    },
    {
        "code": "PB",
        "name": "Punjab",
        "districts": [
            "Ludhiana", "Patiala", "Amritsar", "Jalandhar","Bathinda","Pathankot",
            "Hoshiarpur","Batala","Moga","Malerkotla","Khanna","Mohali","Barnala","Firozpur",
            "Phagwara","Kapurthala","Zirakpur","Kot Kapura","Faridkot","Muktsar","Rajpura",
            "Sangrur","Fazilka","Gurdaspur","Kharar","Gobindgarh","Mansa","Malout","Nabha",
            "Tarn Taran","Jagraon","Sunam","Dhuri","Firozpur Cantt.","Sirhind Fatehgarh Sahib",
            "Rupnagar","Jalandhar Cantt.","Samana","Nawanshahr","Rampura Phul","Nangal","Nakodar",
            "Zira","Patti","Raikot","Longowal","Urmar Tanda","Morinda",
            "Phillaur","Pattran","Qadian","Sujanpur","Mukerian","Talwara"
        ]
    },
    {
        "code": "RJ",
        "name": "Rajasthan",
        "districts": [
            "Ajmer","Alwar","Banswara","Baran","Barmer","Bharatpur","Bhilwara","Bikaner","Bundi",
            "Chittorgarh","Churu","Dausa","Dholpur","Dungarpur","Hanumangarh","Jaipur","Jaisalmer",
            "Jalor","Jhalawar","Jhunjhunu","Jodhpur","Karauli","Kota","Nagaur","Pali","Pratapgarh",
            "Rajsamand","Sawai Madhopur","Sikar","Sirohi","Sri Ganganagar","Tonk","Udaipur"
        ]
    },
    {
        "code": "SK",
        "name": "Sikkim",
        "districts": ["Gangtok", "Pelling", "Lachung", "Lachen", "Namchi", "Ravangla"]
    },
    {
        "code": "TN",
        "name": "Tamil Nadu",
        "districts": [
            "Chennai","Coimbatore","Madurai","Tiruchirappalli","Salem",
            "Tirunelveli","Tiruppur","Ranipet","Nagercoil","Thanjavur","Vellore",
            "Kancheepuram","Erode","Tiruvannamalai","Pollachi","Rajapalayam","Sivakasi",
            "Pudukkottai","Neyveli (TS)","Nagapattinam","Viluppuram","Tiruchengode",
            "Vaniyambadi","Theni Allinagaram","Udhagamandalam","Aruppukkottai","Paramakudi",
            "Arakkonam","Virudhachalam","Srivilliputhur","Tindivanam","Virudhunagar","Karur",
            "Valparai","Sankarankovil","Tenkasi","Palani","Pattukkottai","Tirupathur",
            "Ramanathapuram","Udumalaipettai","Gobichettipalayam","Thiruvarur","Thiruvallur",
            "Panruti","Namakkal","Thirumangalam","Vikramasingapuram","Nellikuppam","Rasipuram",
            "Tiruttani","Nandivaram-Guduvancheri","Periyakulam","Pernampattu","Vellakoil","Sivaganga",
            "Vadalur","Rameshwaram","Tiruvethipuram","Perambalur","Usilampatti","Vedaranyam",
            "Sathyamangalam","Puliyankudi","Nanjikottai","Thuraiyur","Sirkali","Tiruchendur",
            "Periyasemur","Sattur","Vandavasi","Tharamangalam","Tirukkoyilur","Oddanchatram",
            "Palladam","Vadakkuvalliyur","Tirukalukundram","Uthamapalayam","Surandai","Sankari",
            "Shenkottai","Vadipatti","Sholingur","Tirupathur","Manachanallur","Viswanatham","Polur",
            "Panagudi","Uthiramerur","Thiruthuraipoondi","Pallapatti","Ponneri","Lalgudi","Natham",
            "Unnamalaikadai","P.N.Patti","Tharangambadi","Tittakudi","Pacode","O' Valley","Suriyampalayam",
            "Sholavandan","Thammampatti","Namagiripettai","Peravurani","Parangipettai","Pudupattinam",
            "Pallikonda","Sivagiri","Punjaipugalur","Padmanabhapuram", "Thirupuvanam"
        ]
    },
    {
        "code": "TS",
        "name": "Telangana",
        "districts": [
            "Hyderabad", "Warangal", "Nizamabad", "Karimnagar", "Ramagundam", 
            "Khammam", "Mahbubnagar", "Mancherial", "Adilabad", "Suryapet",
            "Jagtial", "Miryalaguda", "Nirmal", "Kamareddy", "Kothagudem", 
            "Bodhan", "Palwancha", "Mandamarri", "Koratla", "Sircilla",
            "Tandur", "Siddipet", "Wanaparthy", "Kagaznagar", "Gadwal", 
            "Sangareddy", "Bellampalle", "Bhongir", "Vikarabad", "Jangaon",
            "Bhadrachalam", "Bhainsa", "Farooqnagar", "Medak", "Narayanpet", 
            "Sadasivpet", "Yellandu", "Manuguru", "Kyathampalle", "Nagarkurnool"
        ]
    },
    {
        "code": "TR",
        "name": "Tripura",
        "districts": [
            "Agartala", "Udaipur", "Dharmanagar", "Pratapgarh", 
            "Kailasahar", "Belonia", "Khowai"
        ]
    },
    {
        "code": "UP",
        "name": "Uttar Pradesh",
        "districts": ['Agra', 'Aligarh', 'Allahabad', 'Ambedkar Nagar', 'Amethi', 'Amroha', 'Auraiya',
            'Azamgarh', 'Baghpat', 'Bahraich', 'Ballia', 'Balrampur', 'Banda', 'Barabanki', 'Bareilly',
            'Basti', 'Bijnor', 'Budaun', 'Bulandshahr', 'Chandauli', 'Chitrakoot', 'Deoria', 'Etah', 'Etawah',
            'Faizabad', 'Farrukhabad', 'Fatehpur', 'Firozabad', 'Gautam Buddha Nagar', 'Ghaziabad', 'Ghazipur',
            'Gonda', 'Gorakhpur', 'Hamirpur', 'Hardoi', 'Hathras (Mahamaya Nagar)', 'Jalaun', 'Jaunpur', 'Jhansi',
            'Jyotiba Phule Nagar', 'Kannauj', 'Kanpur Dehat (Ramabai Nagar)', 'Kanpur Nagar', 'Kanshiram Nagar',
            'Kaushambi', 'Kheri', 'Kushinagar', 'Lalitpur', 'Lucknow', 'Maharajganj', 'Mahoba', 'Mainpuri', 'Mathura',
            'Mau', 'Meerut', 'Mirzapur', 'Moradabad', 'Muzaffarnagar', 'Panchsheel Nagar district (Hapur)', 'Pilibhit',
            'Pratapgarh', 'Raebareli', 'Rampur', 'Saharanpur', 'Sant Kabir Nagar', 'Sant Ravidas Nagar', 'Shahjahanpur',
            'Shamli', 'Shravasti', 'Siddharthnagar', 'Sitapur', 'Sonbhadra', 'Sultanpur', 'Unnao', 'Varanasi'
        ]
    },
    {
        "code": "UK",
        "name": "Uttarakhand",
        "districts": [
            "Dehradun","Hardwar","Haldwani-cum-Kathgodam","Srinagar","Kashipur","Roorkee",
            "Rudrapur","Rishikesh","Ramnagar","Pithoragarh","Manglaur","Nainital","Mussoorie",
            "Tehri","Pauri","Nagla","Sitarganj","Bageshwar"
        ]
    },
    {
        "code": "WB",
        "name": "West Bengal",
        "districts": [
            "Kolkata","Siliguri","Asansol","Raghunathganj","Kharagpur","Naihati",
            "English Bazar","Baharampur","Hugli-Chinsurah","Raiganj","Jalpaiguri","Santipur",
            "Balurghat","Medinipur","Habra","Ranaghat","Bankura","Nabadwip","Darjiling","Purulia",
            "Arambagh","Tamluk","AlipurdUrban Agglomerationr","Suri","Jhargram","Gangarampur",
            "Rampurhat","Kalimpong","Sainthia","Taki","Murshidabad","Memari","Paschim Punropara",
            "Tarakeswar","Sonamukhi","PandUrban Agglomeration","Mainaguri","Malda","Panchla",
            "Raghunathpur","Mathabhanga","Monoharpur","Srirampore","Adra"
        ]
    }
]

var boards = [
    'Andhra Pradesh Board of Secondary Education',
    'Andhra Pradesh Board of Intermediate Education',
    'Andhra Pradesh Open School Society',
    'Assam Board of Secondary Education',
    'Assam Higher Secondary Education Council',
    'Assam State Open School',
    'Bihar Board of Open Schooling & Examination',
    'Bihar School Examination Board',
    'Board of Higher Secondary Education Delhi(BHSE)',
    'Central Board of Secondary Education(CBSE)',
    'Chhattisgarh Secondary Board of Education',
    'Council for the Indian School Certificate Examinations(CISCE)',
    'Grameen Mukt Vidhyalayi Shiksha Sansthan(GMVSS), Shahdra, Delhi',
    'Goa Board of Secondary & Higher Secondary Education',
    'Gujarat Secondary Education Board',
    'Gujarat State Open School',
    'Haryana Board of School Education',
    'Haryana State Open School',
    'Himachal Pradesh Board of School Education',
    'Himachal Pradesh State Open School',
    'Indian Board of School Education',
    'Jammu and Kashmir State Board of School Education',
    'Jammu and Kashmir State Open School',
    'Jharkhand Academic Council',
    'Karnataka Secondary Education Examination Board',
    'Kerala Higher Secondary Examination Board',
    'Kerala State Open School',
    'Madhya Pradesh Board of Secondary Education',
    'Madhya Pradesh State Open School',
    'Maharashtra State Board of Secondary and Higher Secondary Education',
    'Meghalaya Board of School Education',
    'Mizoram Board of School Education',
    'Nagaland Board of School Education',
    'National Institute of Open Schooling(NIOS)',
    'Odisha Board of Secondary Education',
    'Odisha Council of Higher Secondary Education',
    'Punjab School Education Board',
    'Rajasthan Board of Secondary Education',
    'Rajasthan State Open School',
    'Tamil Nadu Board of Secondary Education',
    'Telangana Board of Intermediate Education',
    'Telangana Board of Secondary Education',
    'Tripura Board of Secondary Education',
    'Uttar Pradesh Board of High School and Intermediate Education Allahabad',
    'Uttarakhand Board of School Education',
    'West Bengal Board of Madrasah Education',
    'West Bengal Board of Primary Education',
    'West Bengal Board of Secondary Education',
    'West Bengal Council of Higher Secondary Education',
    'West Bengal Council of Rabindra Open Schooling'
];

var languages = [
    'Assamese',
    'Bengali',
    'Bodo',
    'Dogri',
    'English',
    'Gujarati',
    'Hindi',
    'Kannada',
    'Kashmiri',
    'Konkani',
    'Maithili',
    'Malayalam',
    'Marathi',
    'Meitei(Manipuri)',
    'Nepali',
    'Odia',
    'Punjabi',
    'Sanskrit',
    'Santali',
    'Sindhi',
    'Tamil',
    'Telugu',
    'Urdu'
];