var App = function App() {
    this.tableDiv = $('#tableDiv');
    this.usageTableBody = $('#usageTableBody');
    this.assessmentUsageTableBody = $('#assessmentsUsageTableBody');
    this.summaryTableBody = $('#summaryTableBody');
    this.usageTableBottomInfo = $('#usageTableBottomInfo');
    this.assessmentsTableBottomInfo = $('#assessmentTableBottomInfo');
    this.startDate;
    this.endDate;
    this.uid = $('#uid').val();
    this.did = $('#did').val();
    this.currentGrade = "";
    this.linkedClassrooms = {};
}

App.prototype.functions = new Functions();

App.prototype.init = function (grade, subjectName, startDate, endDate) {
    this.currentGrade = grade;

    // Get data for the graphs
    const queryString = `?did=${this.did}&grade=${grade}&subjectName=${subjectName}&reportType=ContentUsageReports&forWhat=contentCategory&start=${startDate}&end=${endDate}`;
    this.requestForGraphData("/reports/graphicalReports" + queryString, "piechart", false, 'pie-loader');
    // this.requestForGraphData("/reports/getLowestUsageData" + queryString, "barGraph", true, 'barGraph-highest-usage');
    // this.requestForGraphData("/reports/getHighestUsageData" + queryString, "barGraph", true, 'barGraph-lowest-usage');

    // Get Data for tables
    this.getGradeWiseUsageData(`getClassWiseCategoryUsageData?did=${this.did}&grade=${grade}&subjectName=${subjectName}&start=${startDate}&end=${endDate}`);
    this.getGradeWiseAssessmentsUsageData(`/reports/getClassWiseAssessmentsUsageData?did=${this.did}&grade=${grade}&subjectName=${subjectName}&start=${startDate}&end=${endDate}`);
}

App.prototype.getGradeWiseUsageData = function (url) {
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function (error, data) {
        // hide spinner/loader
        instance.toggleLoader('loader2', false);

        // $('html, body').animate({
        //     scrollTop: $("#summaryTableBody").offset().top
        // }, 800);

        if (data && data.cumulativeUsage) {

            // var data = {
            //     "cumulativeUsage": {
            //         'A': {
            //             "total": 41.5,
            //             "multimedia": 10.2,
            //             "videos": 21.1,
            //             "books": 10.2
            //         },
            //         'B': {
            //             "total": 22.6,
            //             "multimedia": 10.3,
            //             "videos": 2.1,
            //             "books": 10.2
            //         },
            //         'C': {
            //             "total": 22.6,
            //             "multimedia": 0.2,
            //             "videos": 21.1,
            //             "books": 10.2
            //         },
            //         'D': {
            //             "total": 10.3,
            //             "multimedia": 10.2,
            //             "videos": 40.0,
            //             "books": 10.0
            //         },
            //         'E': {
            //             "total": 51.5,
            //             "multimedia": 10.2,
            //             "videos": 21.1,
            //             "books": 10.2
            //         },
            //         'F': {
            //             "total": 12.6,
            //             "multimedia": 10.3,
            //             "videos": 2.1,
            //             "books": 10.2
            //         },
            //         'G': {
            //             "total": 13.5,
            //             "multimedia": 0.2,
            //             "videos": 21.1,
            //             "books": 10.2
            //         },
            //         'H': {
            //             "total": 60.9,
            //             "multimedia": 10.2,
            //             "videos": 40.0,
            //             "books": 10.0
            //         },
            //         'I': {
            //             "total": 4.5,
            //             "multimedia": 10.2,
            //             "videos": 21.1,
            //             "books": 10.2
            //         },
            //         'J': {
            //             "total": 29.6,
            //             "multimedia": 10.3,
            //             "videos": 2.1,
            //             "books": 10.2
            //         },
            //         'K': {
            //             "total": 3.5,
            //             "multimedia": 0.2,
            //             "videos": 21.1,
            //             "books": 10.2
            //         },
            //         'L': {
            //             "total": 0.3,
            //             "multimedia": 10.2,
            //             "videos": 40.0,
            //             "books": 10.0
            //         }
            //     }
            // }


            // var copyDataForTopFive = data;
            // var copyDataForLowestFive = data;

            var copyDataForTopFive = jQuery.extend(true, {}, data);
            var copyDataForLowestFive = jQuery.extend(true, {}, data);
            // var topFiveStudents = instance.getTopFiveStudents(copyData);
            // var bottomFiveStudents, topFiveStudents;
            instance.getTopFiveStudents(copyDataForTopFive, (topFiveStudents) => {
                console.log('topFiveStudents ', topFiveStudents);
                instance.createBarGraph(topFiveStudents.names, topFiveStudents.usageTime, 'barGraph-highest-usage', "Top 5 Users");            
            });

            instance.getBottomFiveStudents(copyDataForLowestFive, (bottomFiveStudents) => {
                console.log('bottomFiveStudents ', bottomFiveStudents);
                instance.createBarGraph(bottomFiveStudents.names, bottomFiveStudents.usageTime, 'barGraph-lowest-usage', "Bottom 5 Users");
            });

            instance.showGradeUsageDataInTable(data);
            return;
        }
        instance.tableDiv.html(error);
    })
}

// App.prototype.sortData = function (data) {
//     var newArray = {
//         "names": [],
//         "usageTimes": []
//     }

//     var keys = Object.keys(data.cumulativeUsage);
//     newArray.usageTimes.push(data.cumulativeUsage[keys[0]].total);
//     newArray.names.push(keys[0]);

//     for(var i = 1; i< keys.length; i++) {
//         if(data.cumulativeUsage.hasOwnProperty(keys[i])) {
//             const iElement = data.cumulativeUsage[keys[i]];

//             for(var j = 0; j<newArray.usageTimes.length; j++) {
//                 if(newArray.usageTimes[j] > iElement.total) {

//                     for(var k = newArray.length - 2; k > j; k++) {
//                         var oldName = newArray.names[k];
//                         var oldUsageTimes = newArray.usageTimes[k];

//                         newArray.names[k] = keys[i];
//                         newArray.usageTimes[k] = iElement.total;    
//                     }
//                 }
//             }
//         }
//     }
// }

App.prototype.getTopFiveStudents = function (data, done) {
    var topFiveStudents = {
        "names": ["", "", "", "", ""],
        "usageTime": [0, 0, 0, 0, 0] // decreasing order
    };

    var firstStudentName = Object.keys(data.cumulativeUsage)[0];
    for (var j = 0; j < topFiveStudents.usageTime.length; j++) {
        var max = data.cumulativeUsage[firstStudentName].total;
        var lastMaxIndex = firstStudentName;
        for (var studentName in data.cumulativeUsage) {
            if (data.cumulativeUsage.hasOwnProperty(studentName)) {
                const iElement = data.cumulativeUsage[studentName];
                if(max < iElement.total) {
                    max = iElement.total;
                    lastMaxIndex = studentName;
                }
            }
        }
        data.cumulativeUsage[lastMaxIndex].total = 0;
        topFiveStudents.usageTime[j] = max;
        topFiveStudents.names[j] = lastMaxIndex;

        console.log('Largest ' + j + ' = ' + topFiveStudents.usageTime[j]);
        console.log('Largest Name' + j + ' = ' + topFiveStudents.names[j]);
    }
    // return topFiveStudents;
    done(topFiveStudents);
}

App.prototype.getBottomFiveStudents = function (data, done) {
    var bottomFiveStudents = {
        "names": ["", "", "", "", ""],
        "usageTime": [0, 0, 0, 0, 0] // increasing order
    };

    var firstStudentName = Object.keys(data.cumulativeUsage)[0];
    for (var j = 0; j < bottomFiveStudents.usageTime.length; j++) {
        var min = data.cumulativeUsage[firstStudentName].total;
        var lastMinIndex = firstStudentName;
        for (var studentName in data.cumulativeUsage) {
            if (data.cumulativeUsage.hasOwnProperty(studentName)) {
                const iElement = data.cumulativeUsage[studentName];
                if(min > iElement.total) {
                    min = iElement.total;
                    lastMinIndex = studentName;
                }
            }
        }
        
        data.cumulativeUsage[lastMinIndex].total = 100000000;
        bottomFiveStudents.usageTime[j] = min;
        bottomFiveStudents.names[j] = lastMinIndex;

        console.log('Lowest ' + j + ' = ' + bottomFiveStudents.usageTime[j]);
        console.log('Lowest Name' + j + ' = ' + bottomFiveStudents.names[j]);
    }

    // return bottomFiveStudents;
    done(bottomFiveStudents);
}

/**
 * Note: table row creation can be generic for any number of categories.
 * But for now keeping it hardcoded for 5 categories
 * 
 * @param {Object} data
 */
App.prototype.showGradeUsageDataInTable = function (data) {

    // Using only single table element in the html,
    // so need to clean old rows from the table
    $('#usageTableBody tr').css("display", "none");

    var length = 0;
    // create row with classwise values from data object
    for (key in data.cumulativeUsage) {
        if (data.cumulativeUsage.hasOwnProperty(key)) {
            length++;
            const element = data.cumulativeUsage[key];

            var row = $('<tr></tr>').attr("style", "color:black;");
            const studentName = $('<td></td>').attr("align", "center").text(key);
            var multimedia = $('<td></td>').attr("align", "center");
            var books = $('<td></td>').attr("align", "center");
            var apps = $('<td></td>').attr("align", "center");
            var videos = $('<td></td>').attr("align", "center");
            var assessmentApp = $('<td></td>').attr("align", "center");
            var total = $(`<td></td>`).attr("align", "center");

            if (element.multimedia)
                multimedia.text((element.multimedia).toFixed(1));
            else
                multimedia.text('0');

            if (element.videos)
                videos.text((element.videos).toFixed(1));
            else
                videos.text('0');

            if (element.apps)
                apps.text((element.apps).toFixed(1));
            else
                apps.text('0');

            if (element.books)
                books.text((element.books).toFixed(1));
            else
                books.text('0');

            if (element.assessments)
                assessmentApp.text((element.assessments).toFixed(1));
            else
                assessmentApp.text('0');

            total.text((element.total).toFixed(1));

            row.append(studentName);
            row.append(apps);
            row.append(assessmentApp);
            row.append(books);
            row.append(multimedia);
            row.append(videos);
            row.append(total);

            this.usageTableBody.append(row);
        }
    };
    this.usageTableBottomInfo.html('');
    this.usageTableBottomInfo.html((+length) + " out of " + (+length));
}

// ----------------------------------------------------------------------------------------- //

App.prototype.getGradeWiseAssessmentsUsageData = function (url) {
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function (error, data) {
        // hide spinner/loader
        instance.toggleLoader('loader3', false);

        if (data) {
            instance.showAssessmentsDataInTable(data);
            return;
        }

        instance.tableDiv.html(error);
    })
}

App.prototype.showAssessmentsDataInTable = function (data) {
    // Using only same table elements for all the grades in the html,
    // so need to clean old rows from the table
    $('#assessmentsUsageTableBody tr').css("display", "none");

    var length = 0;
    const ASSESSMENT_TYPE_PRACTICE = 'Practice';
    const ASSESSMENT_TYPE_LOCKED_TERM = 'LockedTerm';
    const ASSESSMENT_TYPE_UNLOCKED_TERM = 'UnlockedTerm';

    // create row with classwise values from data object
    for (userId in data.cumulativeUsage) {
        if (data.cumulativeUsage.hasOwnProperty(userId)) {
            const element = data.cumulativeUsage[userId];
            var row = $('<tr></tr>').attr("style", "color:black;").attr('id', userId);
            const studentName = $('<td></td>').attr("align", "center").text(element.userName);
            row.append(studentName);

            var usage = element.usage;
            // for (assessmentType in usage) {

            var count1 = $('<td></td>').attr("align", "center");
            var average1 = $('<td></td>').attr("align", "center");

            var count2 = $('<td></td>').attr("align", "center");
            var average2 = $('<td></td>').attr("align", "center");

            var count3 = $('<td></td>').attr("align", "center");
            var average3 = $('<td></td>').attr("align", "center");

            if (usage.hasOwnProperty(ASSESSMENT_TYPE_PRACTICE)) {
                count1.text(usage[ASSESSMENT_TYPE_PRACTICE]['count']);
                // average1.text(usage[ASSESSMENT_TYPE_PRACTICE]['average']);
                const averageText = 100 * (usage[ASSESSMENT_TYPE_PRACTICE]['totalScore'] / usage[ASSESSMENT_TYPE_PRACTICE]['totalQuestions']);
                average1.text(averageText.toFixed(1) + "%");
            } else {
                count1.text('0');
                average1.text('0');
            }

            if (usage.hasOwnProperty(ASSESSMENT_TYPE_LOCKED_TERM)) {
                count2.text(usage[ASSESSMENT_TYPE_LOCKED_TERM]['count']);
                // average2.text(usage[ASSESSMENT_TYPE_LOCKED_TERM]['average']);
                const averageText = 100 * (usage[ASSESSMENT_TYPE_LOCKED_TERM]['totalScore'] / usage[ASSESSMENT_TYPE_LOCKED_TERM]['totalQuestions']);
                average2.text(averageText.toFixed(1) + "%");
            } else {
                count2.text('0');
                average2.text('0');
            }

            if (usage.hasOwnProperty(ASSESSMENT_TYPE_UNLOCKED_TERM)) {
                count3.text(usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['count']);
                // average3.text(usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['average']);
                const averageText = 100 * (usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['totalScore'] / usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['totalQuestions']);
                average3.text(averageText.toFixed(1) + "%");
            } else {
                count3.text('0');
                average3.text('0');
            }

            row.append(count1);
            row.append(average1);
            row.append(count2);
            row.append(average2);
            row.append(count3);
            row.append(average3);
            // }

            // click listener to the per student assessments usage row
            row.on('click', (e) => {
                e.preventDefault();
                const uid = $('#uid').text();
                const userDisplayName = $('#siteUser').text();
                $('#assessmentUsageTableBody tr').attr("style", "background-color: transparent");
                $(e.currentTarget).attr("style", "background-color:#41acf4;color:white;");

                const studentName = $($(e.currentTarget).find('td')[0]).html();
                window.location.href = `/reports/studentWiseAssessmentsDetailsPage?u=${uid}&n=${userDisplayName}&did=${this.did}&studentID=${e.currentTarget.id}&studentName=${studentName}`;
            });

            // hover listener to the per student assessments usage row
            row.hover((e) => {
                e.preventDefault();
                $('#assessmentUsageTableBody tr').attr("style", "background-color: transparent");
                $(e.currentTarget).attr("style", "background-color:#41acf4; color:white;");

            }, (e) => {
                e.preventDefault();
                $(e.currentTarget).attr("style", "background-color:transparent;");
            });


            this.assessmentUsageTableBody.append(row);
            length++;
        }
    };
    this.assessmentsTableBottomInfo.html('');
    this.assessmentsTableBottomInfo.html((+length) + " out of " + (+length));
}

// ----------------------------------------------------------------------------------------- //

/**
 * --------------- RequestForGraphData ---------------
 * 
 * @param {String} url 
 * @param {HTML Element} element
 * @returns {Void} void
 */
App.prototype.requestForGraphData = function (url, element, isBarGraphRequired, loader) {
    const instance = this;
    if (loader)
        $(`#${loader}`).css('display', 'block');

    this.functions.sendRequest(url, "GET", null, function (error, data) {
        if (error) {
            // show error in the area of graph
            $(`#${element}`).parent().html(`<div class="row form-group">
                                        <div class="col-md-4" style="display:block; margin:10px auto;">
                                            <label>Nothing To Show!</label>
                                            <img src="/images/nothingToShow.png" width="600" height="300"/>
                                        </div>
                                    </div>`);

        } else {
            // hide spinner/loader
            if (loader)
                $(`#${loader}`).css('display', 'none');

            if (isBarGraphRequired) {
                instance.createBarGraph(data.nameList, data.timeList, element);
            } else {
                // create graph with values 'data', in the area of graph
                instance.createPieChart(data.nameList, data.timeList, element);
            }
        }
    })
}

/**
 * Generates Donut shaped chart using generate function of billboard.js
 * 
 * @param {Object} nameList
 * @param {Object} timeList
 * @param {HTML Tag} graphElement
 */
App.prototype.createPieChart = function (nameList, timeList, graphElement) {
    const instance = this;
    // generate the chart
    var piechart = bb.generate({
        bindto: '#' + graphElement,
        data: {
            type: "donut",
            rows: [
                nameList,
                timeList
            ]
        },
        color: {
            pattern: ["#ffb366", "#ff4d4d", "#e6e600", "#cc6600", "#ff99cc", "#ff6666", "#1a8cff", "#0073e6"]
        },
        donut: {
            width: 60,
            title: "% Usage",
            padAngle: 0.03
        }
    });

    piechart.resize({
        width: 500,
        height: 300
    });
}

/**
 * Generates BarChart using generate function of billboard.js
 * 
 * @param {Object} nameList
 * @param {Object} timeList
 * @param {HTML Tag} graphElement
 */
App.prototype.createBarGraph = function (nameList, timeList, graphElement, forWhat) {
    timeList = [forWhat].concat(timeList);
    // bar chart
    var barchart = bb.generate({
        data: {
            columns: [
                timeList
            ],
            type: "bar",
        },
        bar: {
            width: {
                ratio: 0.3,
                max: 300
            }
        },

        color: {
            pattern: ["#99ff66"]
        },

        axis: {
            x: {
                type: "category",
                // label: "Your X-axis",
                categories: nameList
            }
        },

        bindto: '#' + graphElement
    });
}

App.prototype.getFormattedDate = function (date) {
    return ("0000" + date.getFullYear()).slice(-4) + "-" + ("00" + (date.getMonth() + 1)).slice(-2) + "-" + ("00" + date.getDate()).slice(-2);
}

App.prototype.getLastWeekDate = function (currentDate) {
    const last = 6;
    return new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - last)
}

App.prototype.getLastXMonthsDate = function (currentDate, last) {
    return new Date(currentDate.getFullYear(), currentDate.getMonth() - last, currentDate.getDate())
}

App.prototype.toggleRedMessage = function (id, message) {
    if (message)
        $(`#${id}`).css('display', 'block').text(message);
    else
        $(`#${id}`).css('display', 'none');
}

App.prototype.toggleLoader = function (id, show) {
    if (show)
        $(`#${id}`).css('display', 'block');
    else
        $(`#${id}`).css('display', 'none');
}

$(function () {
    const FROM_BEGINNING = 1;
    const LAST_6_MONTHS = 2;
    const LAST_1_MONTH = 3;
    const LAST_1_WEEK = 4;
    const CUSTOM_DATE = 5;

    const app = new App();

    /*
        // load data for active grade in the list
        const activeGrade = $('.scrollmenu>a.active');
        // const gradeId = $(activeGrade).find('h3').text();
        const grade = $(activeGrade).find('span').text();
    */

    // setting default value for "#from" and "#to" input fields
    // default value for time interval is, last one year from today
    const today = new Date();
    const endDate = app.getFormattedDate(today);
    const date = app.getLastXMonthsDate(today, 12);
    const startDate = app.getFormattedDate(date);
    $('#from').val(startDate);
    $('#to').val(endDate);

    // request for grade wise usage data for the first time when page loads
    /*
        app.init(grade, grade, startDate, endDate);
    */

    // on selection of any grade each time
    this.classSelected = function (el) {
        // const gradeId = $(el).parent().find('h3').text();

        // remove Class active from previous active link 
        $($(el).parent().find('.active')).removeClass('active');

        // set current element as active
        $(el).addClass('active');

        const $selectedButton = $($(el).find('span'));
        const grade = $($selectedButton[0]).text();
        const subjectName = $($selectedButton[1]).text();

        $('#intervalOptions').val($('#1').val());
        $('#from').prop('readonly', true).val(startDate);
        $('#to').prop('readonly', true).val(endDate);

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        // init
        app.init(grade, subjectName, $('#from').val(), $('#to').val());
    }

    $('#download')
        .css('display', 'block')
        .on('click', (e) => {
            console.log('grade ', app.currentGrade);
            location.href = `/reports/downloadUsageReportsByStudents?type=ContentUsageReports&schoolID=${app.did}&grade=${app.currentGrade}&start=${$('#from').val()}&end=${$('#to').val()}`;
        });

    // on change of time interval
    $('#intervalOptions').on('change', (e) => {
        var date, startDate;

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        switch (+($(e.currentTarget).find(':selected').attr('id'))) {
            case FROM_BEGINNING:
                date = app.getLastXMonthsDate(today, 12);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                break;
            case LAST_6_MONTHS:
                date = app.getLastXMonthsDate(today, 6);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                break;
            case LAST_1_MONTH:
                date = app.getLastXMonthsDate(today, 1);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                break;
            case LAST_1_WEEK:
                date = app.getLastWeekDate(today);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                break;
            case CUSTOM_DATE:
                $('#from').prop('readonly', false).val("");
                $('#to').prop('readonly', false).val("");
                break;
            default:
                break;
        }
    });

    $('#go').on('click', (e) => {
        const startDate = $('#from').val();
        const endDate = $('#to').val();

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        if (!startDate || !endDate) {
            app.toggleRedMessage('alertMessage', 'Dates can not be empty');
            return;
        } else if (startDate > endDate) {
            app.toggleRedMessage('alertMessage', 'Start date can not be greater than end date');
            return;
        }

        // Grade wise content usage data request
        app.toggleLoader('loader2', true);
        app.getGradeWiseUsageData(`/reports/getClassWiseCategoryUsageData?did=${app.did}&grade=${app.currentGrade}&start=${startDate}&end=${endDate}`);

        // Grade wise assessments usage data request
        app.toggleLoader('loader3', true);
        app.getGradeWiseAssessmentsUsageData(`/reports/getClassWiseAssessmentsUsageData?did=${app.did}&grade=${app.currentGrade}&start=${startDate}&end=${endDate}`);
    });


    // ------------------------------------------------------------------------------------ //
    app.functions.sendRequest(`getLinkedClasses?uid=${app.uid}&did=${app.did}`, 'GET', null, (error, data) => {
        if (error) {
            $('#teacherClasses').html(error.error);
        } else {
            app.linkedClassrooms = data;
            for (var className in app.linkedClassrooms) {
                for (var index in app.linkedClassrooms[className].subjects) {
                    var subjectName = app.linkedClassrooms[className].subjects[index];
                    var $tr = $(`<tr id="${className}_${subjectName}">
                                    <td>${className}</td>
                                    <td>${subjectName}</td>
                                    <td>${app.linkedClassrooms[className].totalStudents}</td>
                                    <td></td>
                                    <td>
                                        <button class="blue-round form-group" id="" onclick="showClassReports(this)" href="#">
                                            Click
                                        </button>
                                    </td>
                                </tr>`);

                    app.functions.sendRequest(`getTotalUsageByClass?did=${app.did}&className=${className}&subjectName=${subjectName}`, 'GET', null, (err, data) => {
                        var rowID = data.className + "_" + data.subjectName;
                        $($(`#${rowID}`).find('td')[3]).html(data.totalUsage);
                    });
                    $('#teacherClasses').append($tr);
                }

                // If the teacher a class teacher of the class
                if (app.linkedClassrooms[className].isClassTeacher) {
                    $('#classTeacherClasses').append(`<tr id="${className}">
                                                        <td>${className}</td>
                                                        <td>${app.linkedClassrooms[className].totalStudents}</td>
                                                        <td></td>
                                                        <td>
                                                            <button class="blue-round form-group" id="" onclick="showClassReports(this)" href="#">
                                                                Click
                                                            </button>
                                                        </td>
                                                    </tr>`);
                    app.functions.sendRequest(`getTotalUsageByClass?did=${app.did}&className=${className}`, 'GET', null, (err, data) => {
                        var rowID = data.className;
                        $($(`#${rowID}`).find('td')[2]).html(data.totalUsage);
                    });
                }
            }
        }
    });

    this.showClassReports = function (element) {
        var rowID = $(element).parent().parent()[0].id;
        var className = rowID.split("_")[0];
        var subjectName = rowID.split("_")[1];

        const startDate = $('#from').val();
        const endDate = $('#to').val();

        var isClassTeacher = app.linkedClassrooms[className].isClassTeacher;
        if (isClassTeacher && !subjectName) { // If the teacher is the class teacher of the class selected
            var count = 1;
            for (var index in app.linkedClassrooms[className].classTeacherSubjects) {
                var subject = app.linkedClassrooms[className].classTeacherSubjects[index];
                if (count === 1) {
                    subjectName = subject;
                    $('#ul-pills').append($(`<a class="sky active" onclick="classSelected(this)" href="#${count}" data-toggle="tab">
                                                    <span>${className}</span> <span>${subject}</span>
                                                </a>`));
                } else {
                    $('#ul-pills').append($(`<a class="sky" onclick="classSelected(this)" href="#${count}" data-toggle="tab">
                                                    <span>${className}</span> <span>${subject}</span>
                                                </a>`));
                }
                count++;
            }
        } else {
            $('#ul-pills').append($(`<a class="sky active" onclick="classSelected(this)" href="#1" data-toggle="tab">
                                            <span>${className}</span> <span>${subjectName}</span>
                                        </a>`));
        }

        // request for grade wise usage data for the first time when page loads
        app.init(className, subjectName, startDate, endDate);
    }
});