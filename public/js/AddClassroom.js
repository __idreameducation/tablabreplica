var loginHandler = new Handler();
const functions = new Functions();
var classesWithDetails = {};
var schoolClasses;

const username = $('#username').val();
const uid = $('#uid').val();
const schoolID = $('#did').val();

$(document).ready(function () {

    functions.sendRequest(`getSchoolGrades?did=${schoolID}`, 'GET', null, (error, data) => {
        console.log('Grades >> ', data);
        schoolClasses = data.grades;
        showGradesInTable(data.grades);
    });

    function showGradesInTable(data) {
        for (var key in data) {
            if(data[key]) {
                $('#classroomsList').append(`<tr>
                                                <td class="col-md-4"><span class="form-control" style="background-color: #eee; opacity: 1;">${key}</span></td>
                                                <td class="col-md-3"><input type="text" class="form-control" style="text-align:center;" value="${data[key].Details.totalStudents}" disabled /></td>
                                                <td class="col-md-3">
                                                    <button class="blue-round form-group" id="${key}" onclick="showGradeTeachers(this)">
                                                        Click
                                                    </button>
                                                </td>
                                                <td class="col-md-2" style="backgroung-color:#000" onclick="editClassDetails(this)">
                                                    <i class="edit fa fa-pencil" area-hidden="true"></i>
                                                </td>
                                            </tr>`);
            }
        }
    }

    this.editClassDetails = function (targetElement) {
        if (($(targetElement).find('.edit')).length > 0) { // Edit
            $(targetElement).find('.edit').removeClass('edit fa-pencil').addClass('save fa-save');
            $(targetElement).parent().find('input, select').prop("disabled", false);
        } else { // Save
            $(targetElement).find('.save').removeClass('save fa-save').addClass('edit fa-pencil');
            $(targetElement).parent().find('input, select').prop("disabled", true);

            // update details in the database
            updateClassDetails($(targetElement).parent());
        }
    }

    function updateClassDetails($selectedClassRow) {
        var updatedTotalNumberOfStudents = $($selectedClassRow.find('input')[0]).val();
        var classID = $($selectedClassRow.find('td span')).text();
        loginHandler.setter(`Tablab/Classes/${schoolID}/${classID}/Details/totalStudents`, updatedTotalNumberOfStudents, (error) => {});
    }

    this.showGradeTeachers = function (targetElement) {
        $('#modalDiv').modal('show');

        // refresh modal area - remove old data, if any
        $('#subjectTeachers').html('');

        var classID = $(targetElement).prop('id');
        $('#selectedClass').html(classID);
        var linkedTeachers = schoolClasses[classID].linkedTeachers;

        for (var teacherID in linkedTeachers) {
            var $name = $(`<td class="col-md-2">${linkedTeachers[teacherID].name}</td>`);
            var $email = $(`<td class="col-md-2">${linkedTeachers[teacherID].email}</td>`);
            var $subjects = $(`<td class="col-md-2"></td>`);
            var allSubjects = linkedTeachers[teacherID].subjects;
            for (var subjectIndex in allSubjects) {
                $subjects.append(`<input class="form-control" style="margin-bottom:3px;" value="${allSubjects[subjectIndex]}" disabled />`);
            }

            var $role = $(`<td class="col-md-2"></td>`);
            $role.text((linkedTeachers[teacherID].isClassTeacher) ? "Class Teacher" : "Subject Teacher");

            var $edit = $(`<td style="backgroung-color:#000"><i class="edit fa fa-pencil" area-hidden="true"></i></td>`)
                .on('click', (e) => {
                    if (($(e.currentTarget).find('.edit')).length > 0) { // Edit
                        $(e.currentTarget).find('.edit').removeClass('edit fa-pencil').addClass('save fa-save');
                        $(e.currentTarget).parent().find('input, select').prop("disabled", false);
                    } else { // Save
                        $(e.currentTarget).find('.save').removeClass('save fa-save').addClass('edit fa-pencil');
                        $(e.currentTarget).parent().find('input, select').prop("disabled", true);

                        // update details in the database
                        updateTeacherDetails($(e.currentTarget).parent());
                    }
                });

            var $buttonDisable = $('<td><i class="fa fa-trash" aria-hidden="true"></i></td>')
                .on('click', (e) => {
                    e.preventDefault();

                    if (confirm("Remove teacher?")) {
                        var classID = $('#selectedClass').html();
                        var teacherID = $($(e.currentTarget).parent()).prop('id');
                        loginHandler.setter(`Tablab/Classes/${schoolID}/${classID}/linkedTeachers/${teacherID}`, {}, (error) => {
                            loginHandler.setter(`users/${teacherID}/linkedClassrooms/${classID}`, {}, (error) => {});
                            $($(e.currentTarget).parent()).remove();
                        });
                    }
                });

            // create row
            $row = $(`<tr id="${teacherID}"></tr>`);

            // append all TD to a row
            $row.append($name);
            $row.append($email);
            $row.append($subjects);
            $row.append($role);
            $row.append($edit);
            $row.append($buttonDisable);

            // append row to table body
            $('#subjectTeachers').append($row);
        }
    }

    function updateTeacherDetails(targetElement) {
        var teacherID = $(targetElement).prop('id');
        var classID = $('#selectedClass').html();

        var updatedSubjects = [];
        var $subjectInputs = targetElement.find('td input');

        $subjectInputs.each(newSubject => {
            if($($subjectInputs[newSubject]).val()) {
                updatedSubjects.push($($subjectInputs[newSubject]).val());
            } else {
                $($subjectInputs[newSubject]).remove();
            }
        });

        loginHandler.setter(`Tablab/Classes/${schoolID}/${classID}/linkedTeachers/${teacherID}/subjects`, updatedSubjects, error => {
            loginHandler.setter(`users/${teacherID}/linkedClassrooms/${classID}/subjects`, updatedSubjects, error => {});
        });
    }


    $('#saveClassInputs').on('click', (event) => {
        var $parent = $($(event.target).parent().parent());
        var inputs = $parent.find('input');

        if (!$(inputs[0]).val()) {
            alert('Enter Class Number!');
            $(inputs[0]).focus();
            return;
        }

        if ($(inputs[1]).val() && !($(inputs[1]).val()).match(/(?:^|[^A-Za-z])[A-Za-z](?![A-Za-z])/)) {
            alert('Enter Valid Section!');
            $(inputs[1]).focus();
            return;
        }

        var cls = $(inputs[0]).val();

        var section = ($(inputs[1]).val()) ? $(inputs[1]).val().toUpperCase() : '';
        var tStudents = (+($(inputs[2]).val())) ? +($(inputs[2]).val()) : 0; // total students in the class

        const classKey = (section) ? `${cls}-${section}` : cls;
        var classDetails = {
            'totalStudents': tStudents,
            'numberOfActiveStudents': 0
        }

        $('loader').css('display', 'block');

        loginHandler.setter(`Tablab/Schools/${schoolID}/Classes/${classKey}`, true, (error1) => {
            loginHandler.setter(`Tablab/Classes/${schoolID}/${classKey}/Details`, classDetails, (error2) => {
                $('loader').css('display', 'none');
                $(inputs[0]).val('');
                $(inputs[1]).val('');
                $(inputs[2]).val('');
            });
        });
    });
});

// update classes and details
function updateClassDetails(targetButton) {
    var element = $(targetButton).parent().parent();

    if ($(targetButton).val() === 'Update') { // update finish command
        // >>>> [START] Check if all values are entered properly
        const targetInputs = $(element).find('input:not(:button)');
        if (!$(targetInputs[0]).val()) {
            alert('Enter Class Number!');
            $(targetInputs[0]).focus();
            return;
        }

        // if (!$(targetInputs[1]).val()) {
        //     alert('Enter Section!');
        //     $(targetInputs[1]).focus();
        //     return;
        // }

        if ($(targetInputs[1]).val() && !($(targetInputs[1]).val()).match(/(?:^|[^A-Za-z])[A-Za-z](?![A-Za-z])/)) {
            alert('Enter Valid Section!');
            $(targetInputs[1]).focus();
            return;
        }

        // if (!$(targetInputs[2]).val()) {
        //     alert('Enter Number of students!');
        //     $(targetInputs[2]).focus();
        //     return;
        // }

        // <<<<< All values are entered properly [END]

        // old values
        const id = $(element).prop('id').split('_');
        const classSectionValue = (id[2]) ? `${id[1]}-${id[2]}` : id[1];

        // delete object for old values
        delete classesWithDetails[classSectionValue];

        // New values
        var cls = $(targetInputs[0]).val();

        var section = ($(targetInputs[1]).val()) ? $(targetInputs[1]).val().toUpperCase() : '';
        var tStudents = ($(targetInputs[2]).val()) ? +($(targetInputs[2]).val()) : 0; // total students in the class

        const key = (section) ? `${cls}-${section}` : cls;
        // Add object for new values
        classesWithDetails[key] = {
            'Details': {
                'totalStudents': tStudents,
                'numberOfActiveStudents': 0
            }
        }

        // update target element properties
        $(element).find('input:not(:button):enabled').prop('disabled', true);
        $(targetButton).removeClass('red-round').addClass('blue-round').val('Change');
        // update id
        const newID = `cls_${cls}_${section}`;
        $(element).prop('id', newID);
    } else { // update initiate command
        $(element).find(':disabled').prop('disabled', false);
        $(targetButton).removeClass('blue-round').addClass('red-round').val('Update');
    }
}

function showClassDetails(element) {
    $('#11a form')[0].reset();
    $('#modalDiv').modal('show');

    functions.sendRequest(`/reports/getGradeDetails?did=${schoolID}&gradeId=${$(element).prop('id')}`, 'GET', null, (error, data) => {
        if (error) {
            alert('Error while fetching school details!');
            $('#mainDiv').html('Error while fetching school details!');
            return;
        }

        for (key in data.gradeDetails) {
            if (data.gradeDetails.hasOwnProperty(key)) {
                const value = data.gradeDetails[key];
                var tdKey = $(`<span>${key}</span>`);
                var tdValue = $(`<input type="text" class="form-control" name="" id="" placeholder="" tabindex="1" value="${value}">`);
                $('#modalForm').append(tdKey);
                $('#modalForm').append(tdValue);
            }
        }

        for (teacherID in data.linkedTeachers) {
            if (data.linkedTeachers.hasOwnProperty(teacherID)) {
                var teacherInfo = data.linkedTeachers[teacherID];
                var $div = $(`<div class="row" id="${teacherID}">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    ${teacherInfo.name}
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    ${teacherInfo.email}
                                </div>
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    ${teacherInfo.email}
                                </div>
                            </div>`);
            }
        }

    });
}