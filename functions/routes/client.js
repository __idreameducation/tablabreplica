var express = require("express");
var bodyParser = require("body-parser");
var controllerClient = require('../controller/client');

var app = express();
var router = express.Router();

router.get("/", controllerClient.tablabHomePage)
router.get("/blog", controllerClient.blogPage)
router.get("/about", controllerClient.aboutPage)
router.get("/faqPage", controllerClient.faqPage)
router.get("/post", controllerClient.postPage)

// login user
router.get("/reports", controllerClient.loginPage);
router.post("/reports/login", bodyParser.json(), controllerClient.loginUser);

// logout
router.get('/reports/logout', controllerClient.logout);

// Render pages on request
router.get('/reports/schoolPage', controllerClient.serveSchoolPage);
router.get('/reports/projectPage', controllerClient.serveProjectPage);
router.get('/reports/teacherPage', controllerClient.serveTeacherPage);
router.get('/reports/schoolReports', controllerClient.serveReportsPage);
router.get('/reports/tabularReports', controllerClient.serveTabularReportsPage);
router.get('/reports/assessmentsAnalyticsPage', controllerClient.serveAssessmentsAnalyticsPage);
router.get('/reports/downloadSampleCSVForStudentInfo', controllerClient.downloadSampleCSVForStudentInfo);
router.get('/reports/studentWiseAssessmentsDetailsPage', controllerClient.serveStudentWiseAssessmentsDetailsPage);
router.get('/reports/masterReportsDetailsPage',controllerClient.serveMasterReportsDetailsPage)


// Send project related data to client on request from Project Page
router.get('/reports/getProjectDetails', controllerClient.getProjectDetails);
router.get('/reports/getProjectSchools', controllerClient.getProjectSchools);

// Send school related data to client on request from School Page
router.get('/reports/getClassWiseCategoryUsageData', controllerClient.getClassWiseCategoryUsageData);
router.get('/reports/getSchoolDetails', controllerClient.getSchoolDetails);
router.get('/reports/schoolMediaDetails', controllerClient.schoolMediaDetails);
router.get('/tablab/analytics',controllerClient.getMonthlyUsageData);


// Assessments related data request
router.get('/reports/getAttemptedAssessmentsReportsPerUser', controllerClient.getAttemptedAssessmentsReportsPerUser);
router.get('/reports/getAssessmentsReportsPerUser', controllerClient.getAssessmentsReportsPerUser);
router.get('/reports/getClassWiseAssessmentsUsageData', controllerClient.getAssessmentsUsage);
router.get('/reports/getAssessmentsUsage', controllerClient.getAssessmentsUsage);

// User Master Details
router.get('/reports/usermasterReportsDetailsPage', controllerClient.usermasterReportsDetails );

// Send usage report data to client on request from Graphical Reports Page
router.get('/reports/graphicalReports', controllerClient.getDataForGraphicalReports);
router.get('/reports/downloadCategorywiseUsageByClasses', controllerClient.downloadCategorywiseUsageByClasses);
router.get('/reports/downloadCategorywiseUsageByStudents', controllerClient.downloadCategorywiseUsageByStudents);
router.get('/reports/downloadUsageReportsByStudents', controllerClient.downloadUsageReportsByStudents);
router.get('/reports/downloadAssessmentsRawReports', controllerClient.downloadAssessmentsRawReports);
router.get('/reports/downloadAssessmentReportsPerStudent', controllerClient.downloadAssessmentReportsPerStudent);
router.get('/reports/downloadUserMasterReports',controllerClient.downloadUserMasterReports);
router.get('/reports/downloadSampleCSVForStudentInfo', controllerClient.downloadSampleCSVForStudentInfo);

// AddClassrooms page
router.get('/reports/getSchoolGrades', controllerClient.getSchoolGrades);
router.get('/reports/getGradeDetails', controllerClient.getGradeDetails);

// getLinkedClasses of a teacher
router.get('/reports/getLinkedClasses', controllerClient.getLinkedClasses);

// 
router.get('/reports/getTotalUsageByClass', controllerClient.getTotalUsageByClass);

// request for adding new student
router.get('/reports/addStudents', controllerClient.serveAddStudentsPage);
router.post('/reports/addStudents', controllerClient.addStudents);

router.get('/reports/addTeachers', controllerClient.serveAddTeachersPage);
router.get('/reports/addClassrooms', controllerClient.serveAddClassroomsPage);

// Request from addTeachers.ejs
// get Teachers
router.get('/reports/getTeachers', controllerClient.getTeachers);

// get, already registered student records
router.get('/reports/getStudentRecords', controllerClient.getStudentRecords)

router.get('/reports/setPassword', controllerClient.setPassword);
router.post("/reports/setPassword", controllerClient.activateUser);

/** Request Main Page for Reports */
router.get('/reports/mainPage', controllerClient.serveMainPage);

module.exports = router;
//router.post("/login", controllerClient.loginUser);