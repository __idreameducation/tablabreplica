$(function() {
    const $email = $('#email');
    const $contactNum = $('#mobileNum');
    const $firstName = $('#fName');
    const $lastName = $('#lName');
    const $designation = $('#designation');
    const $createUserBtn = $('#login-submit');

    var functions = new Functions();
    functions.sendRequest('getFilterKeys?key=classes', 'GET', null, (error, data) => {
        for(var key in data) {
            $('#classList').append(`<option>${data[key]}</option>`);
        }
    });

    functions.sendRequest('getFilterKeys?key=languages', 'GET', null, (error, data) => {
        for(var key in data) {
            $('#languageList').append(`<option>${data[key]}</option>`);
        }
    });

    functions.sendRequest('getFilterKeys?key=eduBoards', 'GET', null, (error, data) => {
        for(var key in data) {
            $('#eduBoardList').append(`<option>${data[key]}</option>`);
        }
    });

    citiesList.forEach((city) => {
        $('#addressList').append(`<option>${city.name}, ${city.state}</option>`);
    })

    $createUserBtn.on('click', function(e) {
        e.preventDefault();

        if ($email.val() === undefined || !$email.val()) {
            alert("Enter email")
            $email.focus();
            return;
        }

        if (!emailRegex.test($email.val())) {
            alert('Invalid Email address!');
            $email.focus();
            return;
        }

        if ($firstName.val() === undefined || !$firstName.val()) {
            alert("Enter First Name")
            $firstName.focus();
            return;
        }

        if ($lastName.val() === undefined || !$lastName.val()) {
            alert("Enter Last Name")
            $lastName.focus();
            return;
        }

        if ($contactNum.val() === undefined ||
            !$contactNum.val() ||
            !contactValidationRegex.test($contactNum.val())) {

            alert("Enter Valid Contact Number");
            $contactNum.focus();
            return;
        }

        var selectedClass = $($('#classList').find(':selected')[0]).val();
        var selectedLanguage = $($('#languageList').find(':selected')[0]).val();
        var selectedBoard = $($('#eduBoardList').find(':selected')[0]).val();
        var selectedAddress = $($('#addressList').find(':selected')[0]).val();

        var userDetails = {
            "userEmail": $email.val(),
            "firstName": $firstName.val(),
            "lastName": $lastName.val(),
            "mobileNum": $contactNum.val(),
            "designation": $($designation.find(':selected')[0]).prop('id'),
            "filters": {
                "class": selectedClass,
                "eduBoard": selectedBoard,
                "language": selectedLanguage,
                "address": {
                    "state": selectedAddress.split(",")[1].trim(),
                    "city": selectedAddress.split(",")[0].trim()
                }
            }
        };

        $createUserBtn.prop('disabled', true);

        var req = new XMLHttpRequest();
        req.open("POST", "admin/createUser");
        req.setRequestHeader("Content-type", "application/json");
        req.send(JSON.stringify(userDetails));

        req.addEventListener("load", function(res) {
            $('#login-form').find('input:not([type=button])').val('');
            $($('#login-form').find('select > option')[0]).prop('selected', true);

            $createUserBtn.prop('disabled', false);
            alert(JSON.parse(req.responseText).message);
        });
    });
});