const path = require('path');
const os = require('os');
const express = require('express');
var app = express();

var Client = function Client() {};
var analyticsJson = {};
var users = {};

var admin,
    functions,
    databaseRef;

Client.prototype.init = function (firebaseFunctions, firebaseAdmin) {
    functions = firebaseFunctions;
    admin = firebaseAdmin;
    databaseRef = admin.database().ref(); //.child("newDatabase");
}

Client.prototype.checkRegisterStatus = function (uid, done) {
    var userRef = databaseRef.child("users/" + uid);
    userRef.once("value", function (snap) {
        var user = snap.val()
        if (user && user.active == false) {
            var obj = {};
            obj["active"] = true;
            userRef.update(obj).then(status => {
                done(user.email)
            }).catch(error => {
                done(false);
            });
        } else {
            done(false);
        }
    })
}

/** 
 * update password and store in database.
 * Inputs :- userId, new password
 * Output :- return callback of successful operation/error.
 */
Client.prototype.sendVerification = function (uid, password, done) {
    if (uid && password) {
        admin.auth().updateUser(uid, {
            emailVerified: true,
            password: password
        }).then(function (userRecord) {
            console.log('user > -', userRecord);
            done(null, userRecord.toJSON())
        }).catch(function (error) {
            console.log('error SV', error);
            done(error)
        });
    } else {
        done("something is missing")
    }
}

/**
 * 
 * 
 * 
 * @param {String} path
 * @param {function} done
 */
Client.prototype.getter = function (path, done) {
    databaseRef.child(path)
        .once('value', snap => {
            done(snap);
        })
}

Client.prototype.getterWithQuery = function (path, startAt, endAt, limitToLast, done) {
    if (limitToLast > 0) {
        databaseRef.child(path)
            .orderByKey()
            .startAt(startAt)
            .endAt(endAt)
            .limitToLast(limitToLast)
            .once('value', snap => {
                done(snap);
            });
    } else {
        databaseRef.child(path)
            .orderByKey()
            .startAt(startAt)
            .endAt(endAt)
            .once('value', snap => {
                done(snap);
            });
    }
}

Client.prototype.getOrderedByChild = function (path, argOrderByChild, valueOrderByChild, done) {
    databaseRef.child(path)
    .orderByChild(argOrderByChild)
    .equalTo(valueOrderByChild)
    .once('value', snap => {
        done(snap);
    });
}

Client.prototype.setter = function (path, data, done) {
    return databaseRef.child(path).set(data).then(() => {
        return done();
    }).catch(reason => {
        const error = {
            "reason": reason,
            "message": "Error Occured!"
        }
        return done(error);
    });
}

/**
 * tool to convert Bhiwadi JSON reports to csv/excel
 */
Client.prototype.jsonReportsToCSV = function (done) {
    admin.database().ref().child("iReports").once("value", snap => {
        done(snap);
    });
}

module.exports = Client;