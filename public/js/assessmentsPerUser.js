var App = function App() {
    this.assessmentsUsageTableBody = $('#assessmentsUsageTableBody');
    this.summaryTableBody = $('#summaryTableBody');
    this.assessmentsTableBottomInfo = $('#assessmentTableBottomInfo');
    this.did = $('#did').text();
    this.startDate = '';
    this.endDate = '';
    this.studentID = '';
    this.type = '';
}

App.prototype.functions = new Functions();

App.prototype.init = function(startDate, endDate) {
    const instance = this;
    this.studentID = this.functions.getParameterByName("studentID");
    this.getAssessmentsReportsPerUser(`/reports/getAssessmentsReportsPerUser?did=${this.did}&studentID=${this.studentID}&start=${startDate}&end=${endDate}`, 1);
    this.getAssessmentsReportsPerUser(`/reports/getAttemptedAssessmentsReportsPerUser?did=${this.did}&studentID=${this.studentID}&start=${startDate}&end=${endDate}`, 2);
}

App.prototype.getAssessmentsReportsPerUser = function(url, flag) {
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function(error, data) {
        console.log(flag, data);
        if (data instanceof String) {
            instance.tableDiv.html(data);
        } else {
            if (flag === 1)
                instance.showAssessmentsDataInTable(data);
            else if (flag === 2)
                instance.showAttemptedAssessmentsReportsPerUser(data);
        }
    })
}

App.prototype.showAssessmentsDataInTable = function(data) {
    // Using only same table elements for all the grades in the html,
    // so need to clean old rows from the table
    $('#summaryTableBody tr').css("display", "none");

    // hide spinner/loader
    this.toggleLoader('loader1', false);

    const ASSESSMENT_TYPE_PRACTICE = 'Practice';
    const ASSESSMENT_TYPE_LOCKED_TERM = 'LockedTerm';
    const ASSESSMENT_TYPE_UNLOCKED_TERM = 'UnlockedTerm';

    var row = $('<tr></tr>').attr("style", "color:black;");

    var usage = data.cumulativeUsage;
    // for (assessmentType in usage) {

    var count1 = $('<td></td>').attr("align", "center");
    var average1 = $('<td></td>').attr("align", "center");

    var count2 = $('<td></td>').attr("align", "center");
    var average2 = $('<td></td>').attr("align", "center");

    var count3 = $('<td></td>').attr("align", "center");
    var average3 = $('<td></td>').attr("align", "center");

    if (usage.hasOwnProperty(ASSESSMENT_TYPE_PRACTICE)) {
        count1.text(usage[ASSESSMENT_TYPE_PRACTICE]['count']);
        const averageText = 100 * (usage[ASSESSMENT_TYPE_PRACTICE]['totalScore'] / usage[ASSESSMENT_TYPE_PRACTICE]['totalQuestions']);
        average1.text(averageText.toFixed(1) + "%");
    } else {
        count1.text('0');
        average1.text('0');
    }

    if (usage.hasOwnProperty(ASSESSMENT_TYPE_LOCKED_TERM)) {
        count2.text(usage[ASSESSMENT_TYPE_LOCKED_TERM]['count']);
        // average2.text(usage[ASSESSMENT_TYPE_LOCKED_TERM]['average']);
        const averageText = 100 * (usage[ASSESSMENT_TYPE_LOCKED_TERM]['totalScore'] / usage[ASSESSMENT_TYPE_LOCKED_TERM]['totalQuestions']);
        average2.text(averageText.toFixed(1) + "%");
    } else {
        count2.text('0');
        average2.text('0');
    }

    if (usage.hasOwnProperty(ASSESSMENT_TYPE_UNLOCKED_TERM)) {
        count3.text(usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['count']);
        // average3.text(usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['average']);
        const averageText = 100 * (usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['totalScore'] / usage[ASSESSMENT_TYPE_UNLOCKED_TERM]['totalQuestions']);
        average3.text(averageText.toFixed(1) + "%");
    } else {
        count3.text('0');
        average3.text('0');
    }

    row.append(count1);
    row.append(average1);
    row.append(count2);
    row.append(average2);
    row.append(count3);
    row.append(average3);

    this.summaryTableBody.append(row);
}

App.prototype.showAttemptedAssessmentsReportsPerUser = function(data) {
    // Using only same table elements for all the grades in the html,
    // so need to clean old rows from the table
    $('#assessmentsUsageTableBody tr').css("display", "none");

    // hide spinner/loader
    this.toggleLoader('loader2', false);

    const ASSESSMENT_TYPE_PRACTICE = 'Practice';
    const ASSESSMENT_TYPE_LOCKED_TERM = 'LockedTerm';
    const ASSESSMENT_TYPE_UNLOCKED_TERM = 'UnlockedTerm';

    var length = 0;
    // create row with classwise values from data object
    for (keyAssessmentName in data.cumulativeUsage) {
        if (data.cumulativeUsage.hasOwnProperty(keyAssessmentName)) {
            length++;

            var row = $('<tr></tr>').attr("style", "color:black;");

            var tdName = $('<td></td>').attr("align", "center");
            var tdTopic = $('<td></td>').attr("align", "center");
            var tdType = $('<td></td>').attr("align", "center");
            var tdAttempts = $('<td></td>').attr("align", "center");
            var tdAverage = $('<td></td>').attr("align", "center");

            const assesmentNameArray = keyAssessmentName.split('_');
            // const assessmentClass = assesmentNameArray[0];
            // const assessmentSubject = assesmentNameArray[1];
            const assessmentTopic = assesmentNameArray[2];
            const assessmentName = assesmentNameArray[3];
            const average = 100 * (data.cumulativeUsage[keyAssessmentName].totalScore / data.cumulativeUsage[keyAssessmentName].totalQuestions);

            tdName.text(assessmentName);
            tdTopic.text(assessmentTopic);
            tdType.text(data.cumulativeUsage[keyAssessmentName].assessmentType);
            tdAttempts.text(data.cumulativeUsage[keyAssessmentName].count);
            tdAverage.text(average.toFixed(1) + '%');

            row.append(tdName);
            row.append(tdTopic);
            row.append(tdType);
            row.append(tdAttempts);
            row.append(tdAverage);

            this.assessmentsUsageTableBody.append(row);
        }
    }
}

App.prototype.getFormattedDate = function(date) {
    return ("0000" + date.getFullYear()).slice(-4) + "-" + ("00" + (date.getMonth() + 1)).slice(-2) + "-" + ("00" + date.getDate()).slice(-2);
}

App.prototype.getLastWeekDate = function(currentDate) {
    const last = 6;
    return new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - last)
}

App.prototype.getLastXMonthsDate = function(currentDate, last) {
    return new Date(currentDate.getFullYear(), currentDate.getMonth() - last, currentDate.getDate())
}

App.prototype.toggleRedMessage = function(id, message) {
    if (message)
        $(`#${id}`).css('display', 'block').text(message);
    else
        $(`#${id}`).css('display', 'none');
}

App.prototype.toggleLoader = function(id, show) {
    if (show)
        $(`#${id}`).css('display', 'block');
    else
        $(`#${id}`).css('display', 'none');
}

$(function() {
    const FROM_BEGINNING = 1;
    const LAST_6_MONTHS = 2;
    const LAST_1_MONTH = 3;
    const LAST_1_WEEK = 4;
    const CUSTOM_DATE = 5;

    const TYPE_ALL = 1;
    const ASSESSMENT_TYPE_PRACTICE = 2;
    const ASSESSMENT_TYPE_LOCKED_TERM = 3;
    const ASSESSMENT_TYPE_UNLOCKED_TERM = 4;

    const app = new App();

    // setting default value for "#from" and "#to" input fields
    // default value for time interval is, last one year from today
    const today = new Date();
    const endDate = app.getFormattedDate(today);
    const date = app.getLastXMonthsDate(today, 12);
    const startDate = app.getFormattedDate(date);
    $('#from').val(startDate);
    $('#to').val(endDate);

    // request for grade wise usage data for the first time when page loads
    app.init(startDate, endDate);

    $('#download')
        .css('display', 'block')
        .on('click', (e) => {
            location.href = `/reports/downloadAssessmentReportsPerStudent?type=${app.type}&did=${app.did}&studentID=${app.studentID}&grade=${app.currentGrade}&start=${$('#from').val()}&end=${$('#to').val()}`;
        })

    // on change of time interval
    $('#intervalOptions').on('change', (e) => {
        var date, startDate;

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        switch (+($(e.currentTarget).find(':selected').attr('id'))) {
            case FROM_BEGINNING:
                date = app.getLastXMonthsDate(today, 12);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_6_MONTHS:
                date = app.getLastXMonthsDate(today, 6);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_1_MONTH:
                date = app.getLastXMonthsDate(today, 1);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_1_WEEK:
                date = app.getLastWeekDate(today);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case CUSTOM_DATE:
                $('#from').prop('readonly', false).val("");
                $('#to').prop('readonly', false).val("");
                //          $('#go').prop('disabled', false);
                break;
            default:
                break;
        }
    });

    $('#go').on('click', (e) => {
        const startDate = $('#from').val();
        const endDate = $('#to').val();

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        if (!startDate || !endDate) {
            app.toggleRedMessage('alertMessage', 'Dates can not be empty');
            return;
        } else if (startDate > endDate) {
            app.toggleRedMessage('alertMessage', 'Start date can not be greater than end date');
            return;
        }

        // Per user overall assessments usage data request
        app.toggleLoader('loader1', true);
        app.getAssessmentsReportsPerUser(`/reports/getAssessmentsReportsPerUser?did=${app.did}&studentID=${app.studentID}&start=${startDate}&end=${endDate}`, 1);

        // users' all assessments taken, data request
        app.toggleLoader('loader2', true);
        app.getAssessmentsReportsPerUser(`/reports/getAttemptedAssessmentsReportsPerUser?type=${app.type}&did=${app.did}&studentID=${app.studentID}&start=${startDate}&end=${endDate}`, 2);
    });

    $('#assessmentTypeSelector').on('change', (e) => {
        switch (+($(e.currentTarget).find(':selected').attr('id'))) {
            case TYPE_ALL:
                app.type = "";
                break;
            case ASSESSMENT_TYPE_PRACTICE:
                app.type = "Practice";
                break;
            case ASSESSMENT_TYPE_LOCKED_TERM:
                app.type = "LockedTerm";
                break;
            case ASSESSMENT_TYPE_UNLOCKED_TERM:
                app.type = "UnlockedTerm";
                break;
        }

        // users' all assessments taken, data request
        app.toggleLoader('loader2', true);
        app.getAssessmentsReportsPerUser(`/reports/getAttemptedAssessmentsReportsPerUser?did=${app.did}&studentID=${app.studentID}&start=${startDate}&end=${endDate}&type=${app.type}`, 2);
    })
});