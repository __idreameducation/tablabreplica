var App = function App() {
    var tableDiv;
    var gradeUsageTableBody;
    var tableBottomInfo;
	var summaryTableBody;
	var monthlyUsageTableBody 
}

App.prototype.functions = new Functions();

App.prototype.init = function() {
    this.tableDiv = $('#tableDiv');
    this.gradeUsageTableBody = $('#gradeUsageTableBody');
    this.tableBottomInfo = $('#tableBottomInfo');
	this.summaryTableBody = $('#summaryTableBody');
	this.monthlyUsageTableBody = $('#monthlyUsageTableBody');

    // constant ids for various time periods defined in Select tag in html file
    const FROM_BEGINNING = 1;
    const LAST_6_MONTHS = 2;
    const LAST_1_MONTH = 3;
    const LAST_1_WEEK = 4;
    const CUSTOM_DATE = 5;
    const instance = this;
    const did = $('#did').text();

    // setting default value for "#from" and "#to" input fields
    // default value for time interval is, last one year from today
    const today = new Date();
    var endDate = instance.getFormattedDate(today);
    const date = instance.getLastXMonthsDate(today, 12);
    var startDate = instance.getFormattedDate(date);
    $('#from').val(startDate);
    $('#to').val(endDate);

    $('#download')
        .css('display', 'block')
        .on('click', (e) => {
            location.href = `/reports/downloadCategorywiseUsageByClasses?type=ContentUsageReports&schoolID=${did}&start=${$('#from').val()}&end=${$('#to').val()}`;
        });

    this.getGradeWiseUsageData(`/reports/getClassWiseCategoryUsageData?did=${did}&start=${startDate}&end=${endDate}&type=ClassWise Category Usage`);
	this.getSchoolDetails("/reports/getSchoolDetails?did=" + did);
	this.getSchoolMediaDetails("/reports/schoolMediaDetails?did=" + did);
	this.showMonthlyUsageData("/tablab/analytics/?did="+did);

    // listeners for time period filter on table
    //1. on change of time interval
    $('#intervalOptions').on('change', (e) => {
        var date;

        // hide red message, if any
        instance.toggleRedMessage('alertMessage');

        switch (+($(e.currentTarget).find(':selected').attr('id'))) {
            case FROM_BEGINNING:
                date = instance.getLastXMonthsDate(today, 12);
                startDate = instance.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_6_MONTHS:
                date = instance.getLastXMonthsDate(today, 6);
                startDate = instance.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_1_MONTH:
                date = instance.getLastXMonthsDate(today, 1);
                startDate = instance.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_1_WEEK:
                date = instance.getLastWeekDate(today);
                startDate = instance.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case CUSTOM_DATE:
                $('#from').prop('readonly', false).val("");
                $('#to').prop('readonly', false).val("");
                //          $('#go').prop('disabled', false);
                break;
            default:
                break;
        }
    });

    // 2. on click listener on the "Go" button for filter
    $('#go').on('click', (e) => {
        const startDate = $('#from').val();
        const endDate = $('#to').val();

        // hide red message, if any
        instance.toggleRedMessage('alertMessage');

        if (!startDate || !endDate) {
            instance.toggleRedMessage('alertMessage', 'Dates can not be empty');
            return;
        } else if (startDate > endDate) {
            instance.toggleRedMessage('alertMessage', 'Start date can not be greater than end date');
            return;
        }

        instance.toggleLoader('loader2', true);
        instance.getGradeWiseUsageData(`/reports/getClassWiseCategoryUsageData?did=${did}&start=${startDate}&end=${endDate}&type=ClassWise Category Usage`);
    });
}

App.prototype.getGradeWiseUsageData = function(url) {
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function(error, data) {
        if (data instanceof String) {
            instance.tableDiv.html(data);
        } else {
            instance.showGradeUsageDataInTable(data);
        }
    })
}

/**
 * Note: table row creation can be generic for any number of categories.
 * But for now keeping it hardcoded for 5 categories
 * 
 * @param {Object} data
 */
App.prototype.showGradeUsageDataInTable = function(data) {
    var length = 0;

    // Using only single table element in the html,
    // so need to clean old rows from the table
    $('#gradeUsageTableBody tr').css("display", "none");

    // hide spinner/loader
    $('#loader2').css('display', 'none');

    // create row with classwise values from data object
    for (key in data.cumulativeUsage) {
        if (data.cumulativeUsage.hasOwnProperty(key)) {
            length++;
            const element = data.cumulativeUsage[key];

            var row = $('<tr></tr>');
            const grade = $('<td></td>').text(key);
            var multimedia = $('<td></td>');
            var books = $('<td></td>');
            // var apps = $('<td></td>');
            var videos = $('<td></td>');
            var assessmentApp = $('<td></td>');

            if (element.multimedia)
                multimedia.text((+element.multimedia).toFixed(1));
            else
                multimedia.text('-');

            if (element.videos)
                videos.text((+element.videos).toFixed(1));
            else
                videos.text('-');

            // if (element.apps)
            // apps.text((+element.apps).toFixed(1));
            // else
            // apps.text('-');

            if (element.books)
                books.text((+element.books).toFixed(1));
            else
                books.text('-');

            if (element.assessments) // element.quiz)
                assessmentApp.text((+element.assessments).toFixed(1)); // assessmentApp.text((+element.quiz).toFixed(1));
            else
                assessmentApp.text('-');

            row.append(grade);
            // row.append(apps);
            row.append(assessmentApp);
            row.append(books);
            row.append(multimedia);
            row.append(videos);
			
            this.gradeUsageTableBody.append(row);
        }
    }
    this.tableBottomInfo.html('');
    this.tableBottomInfo.html((+length) + " out of " + (+length));
}

App.prototype.showMonthlyUsageData = function (url) {
	const instance = this;
	this.functions.sendRequest(url,"GET",null,function (error,data) {
		if(error){
			console.log(error.error.message);
		}
		else
		{
			//$('#monthlyUsageTableBody tr').css("display", "none");
			$('#monthlyUsageLoader').css('display', 'none');
		
			let monthly_usage_detials_table_object = data.monthly_usage_detials_table_object;
			
			
			for(key in monthly_usage_detials_table_object)
			{
				let  value = monthly_usage_detials_table_object[key];
				
				let row = document.createElement('tr'); 
				let month = document.createElement('td');
				let month_text = document.createTextNode(key);
				let hour_of_usage = document.createElement('td');
				let unique_days_of_use = document.createElement('td');
				
				
				month.appendChild(month_text);
				
				
		
				
				if(value.hours_of_usage)
				{
					let hour_of_usage_text = document.createTextNode(value.hours_of_usage)
					hour_of_usage.appendChild(hour_of_usage_text);
				}
				else
				{
					let hour_of_usage_text = document.createTextNode("-")
					hour_of_usage.appendChild(hour_of_usage_text);
				}
			
				if(value.unique_days_of_use)
				{
					let unique_days_of_use_text = document.createTextNode(value.unique_days_of_use)
					unique_days_of_use.appendChild(unique_days_of_use_text);
				}
				else
				{
					let unique_days_of_use_text = document.createTextNode("-")
					unique_days_of_use.appendChild(unique_days_of_use_text)
				}
				row.appendChild(month);
				row.appendChild(hour_of_usage);
				row.appendChild(unique_days_of_use); 
		
				this.monthlyUsageTableBody.appendChild(row)
			}
		}
	})
}

App.prototype.getSchoolDetails = function(url) {
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function(error, data) {
        if (error) {
            console.log(error.error.message);
        } else {

            //hide spinner/loader
            $('#loader1').css('display', 'none');
            for (key in data.schoolDetails) {
                if (data.schoolDetails.hasOwnProperty(key)) {
				
                    const value = data.schoolDetails[key];
                    var row = $('<tr></tr>');
                    var tdKey = $('<td></td>');
                    tdKey.attr("align", "center");
                    tdKey.html(`<b>${key}</b>`);

                    var tdValue = instance.functions.create_td(value); //$('<td></td>');
                    tdValue.attr("align", "center");

                    // if (value) {
                    //     tdValue.text(value);
                    // } else {
                    //     tdValue.text('-');
                    // }
                    row.append(tdKey)
                    row.append(tdValue);
                    instance.summaryTableBody.append(row);
                }
            }
        }
    });
}
App.prototype.getSchoolMediaDetails = function(url) {
    const instance = this;
	
    this.functions.sendRequest(url, "GET", null, function(error, data) {
		//console.log("adsdsda");
        if (error) {
            console.log(error.error.message);
        } else {
			//console.log(data)   
			
          if(data.imageLink){ 
             $('#linkDiv').append(' <a href="' +data.imageLink+ '" target="_blank" > <button class="red-round"> <i class="fa fa-table" area-hidden="true"></i> Gallery </button></a><br />');
		  }
		   if(data.vedioLink){ 
			$('#linkDiv').append(' <a href="' +data.vedioLink+ '" target="_blank"> <button class="red-round"><i class="fa fa-table" area-hidden="true"></i> Videos</button></a><br />');
		   }
		    if(data.docLink){ 
			$('#linkDiv').append('  <a href="' +data.docLink+ '" target="_blank" > <button class="red-round"><i class="fa fa-table" area-hidden="true"></i> Document </button></a><br />');
			}
		     //$('#summaryTableBody').append(row);
			//$('#summaryTableBody').append(row
			 $("#inputImageLink").val(data.imageLink);
			  $("#inputVideoLink").val(data.vedioLink);
			  $("#inputdocumentLink").val(data.docLink);
			 
        }
    });
}

App.prototype.getFormattedDate = function(date) {
    return ("0000" + date.getFullYear()).slice(-4) + "-" + ("00" + (date.getMonth() + 1)).slice(-2) + "-" + ("00" + date.getDate()).slice(-2);
}

App.prototype.getLastWeekDate = function(currentDate) {
    const last = 6;
    return new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - last)
}

App.prototype.getLastXMonthsDate = function(currentDate, last) {
    return new Date(currentDate.getFullYear(), currentDate.getMonth() - last, currentDate.getDate())
}

App.prototype.toggleRedMessage = function(id, message) {
    if (message)
        $(`#${id}`).css('display', 'block').text(message);
    else
        $(`#${id}`).css('display', 'none');
}

App.prototype.toggleLoader = function(id, show) {
    if (show)
        $(`#${id}`).css('display', 'block');
    else
        $(`#${id}`).css('display', 'none');
}

new App().init();

// navigation
var currentURL = window.location.href;
var navTitles = (currentURL.split("#")[1]) ? ((currentURL.split("#")[1]).replace(/%20/g, " ")).split(',') : [];

function onNextPageLinkSelected(target) {  
    var nextPageURL =  $(target).prop('id') + "#"; // `/reports/schoolPage?u=${uid}&n=${userDisplayName}&did=${key}#`;
    for (var index = 0; index < navTitles.length; index++) {
        nextPageURL += navTitles[index] + ",";
    }
    
    window.location = nextPageURL + ($('title').html().split("-")[1]).trim();
}


function addDocuments(target){
	
	var schoolId = $(target).prop('id');
	   $('#modalDiv').modal('show');
var schoolId = $('#did').html();
	console.log(schoolId);	   
}



$('#modal-form #buttonSubmit').click((event) => {
event.preventDefault();
   var functions = new Functions();


    var schoolMediaDetails = {};
	var schoolId = $('#did').html();
	console.log(schoolId);
	schoolMediaDetails.imageLink = $('#mainDiv1 #inputImageLink').val() ;
	schoolMediaDetails.vedioLink = $('#mainDiv1 #inputVideoLink').val();
	schoolMediaDetails.docLink = $('#mainDiv1 #inputdocumentLink').val();
	  //console.log(schoolMediaDetails);
	
	 functions.sendRequest("/reports/admin/updateSchoolMedia/?did="+schoolId, "POST", schoolMediaDetails, function(data) {
       if(data.status==9){
		   alert("Media Details Updated");
		    $('#modalDiv').modal('hide');
			$('#linkDiv').html("");
			if(schoolMediaDetails.imageLink){ 
             $('#linkDiv').append('<a href="' +schoolMediaDetails.imageLink+ '" target="_blank" > <button class="red-round"> <i class="fa fa-table" area-hidden="true"></i> Gallery </button></a><br />');
		  }
		   if(schoolMediaDetails.vedioLink){ 
			$('#linkDiv').append('<a href="' +schoolMediaDetails.vedioLink+ '" target="_blank"> <button class="red-round"><i class="fa fa-table" area-hidden="true"></i> Videos</button></a><br />');
		   }
		    if(schoolMediaDetails.docLink){ 
			$('#linkDiv').append('<a href="' +schoolMediaDetails.docLink+ '" target="_blank" > <button class="red-round"><i class="fa fa-table" area-hidden="true"></i> Document </button></a><br />');
			}
			
	   }else {
		   
		    const schoolId = $('#did').text();
			let flag = 0;
			functions.sendRequest(`/reports/schoolMediaDetails?did=${schoolId}`, "GET", null, function(error, schoolMedia) {
		       
			   if(schoolMediaDetails.imageLink===schoolMedia.imageLink){ 
                   flag =1;
				 } 
				 if(schoolMediaDetails.vedioLink===schoolMedia.vedioLink){ 
                   flag =1;
				 }
				  if(schoolMediaDetails.docLink===schoolMedia.docLink){ 
                   flag =1;
				 }
				 if(flag ===0){
					 alert("Please check your Network connection and Try Again");
				 }
			   
			})
	   }
    }) 

})