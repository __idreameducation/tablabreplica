// Initialize Firebase
var config = {
    apiKey: "AIzaSyDO07ubEyweIWU3NS8xJ-WqfzXDJd6EQEs",
    authDomain: "idreamlmsreport.firebaseapp.com",
    databaseURL: "https://idreamlmsreport.firebaseio.com",
    projectId: "idreamlmsreport",
    storageBucket: "idreamlmsreport.appspot.com",
    messagingSenderId: "592969595497"
};
firebase.initializeApp(config);

var databaseRef = firebase.database().ref();
var cumulativeCategoryData = {};
var cumulativeClassData = {};
var cumulativeMonthlyUsageData = {};

function DataFormatter() {
    var insatnce = this;
    var tabId = "";

    this.initializeDataFormatter = function(tabId) {
        this.tabId = tabId;

        var dbReportsRef = databaseRef.child("iReports").child(this.tabId);
        this.getDataFromFirebase(dbReportsRef.child("TCA/cumulativeCategoryData"), cumulativeCategoryData);
        this.getDataFromFirebase(dbReportsRef.child("TCA/cumulativeClassData"), cumulativeClassData);
        this.getDataFromFirebase(dbReportsRef.child("TCA/cumulativeMonthlyUsageData"), cumulativeMonthlyUsageData);

        return "Hi";
    }

    this.getDataFromFirebase = function(ref, object) {
        console.log("0\n");
        ref.once("value", snap => {
            if (snap.val()) object = snap.val();
            else object = {};
            console.log(object);
        });
    }


    this.getCumulativeAnalytics = function(imported, local) {
        if (local.isEmptyObject) {
            local = imported;
        } else {
            for (x in imported) {
                if (local.hasOwnProperty(x)) {
                    local[x] += imported[x];
                } else local[x] = imported[x];
            }
        }
        return local;
    }


    this.getTCA = function() {
        var _cumulativeCategoryData = {};
        var _cumulativeClassData = {};
        var _cumulativeMonthlyUsageData = {};

        var count = 1;
        var dbReportsRef = databaseRef.child("iReports");
        dbReportsRef.once("value", snap => {
            snap.forEach(function(tabIdSnap) {
                tca = (tabIdSnap.val())["TCA"];
                insatnce.getCumulativeAnalytics(tca.cumulativeCategoryData, _cumulativeCategoryData);
                insatnce.getCumulativeAnalytics(tca.cumulativeClassData, _cumulativeClassData);
                insatnce.getCumulativeAnalytics(tca.cumulativeMonthlyUsageData, _cumulativeMonthlyUsageData);
            });

            dbReportsRef.child("SCA/cumulativeCategoryData").set(_cumulativeCategoryData);
            dbReportsRef.child("SCA/cumulativeClassData").set(_cumulativeClassData);
            dbReportsRef.child("SCA/cumulativeMonthlyUsageData").set(_cumulativeMonthlyUsageData);
        });
    }

}

// functions 
DataFormatter.prototype.startCalculations = function() {
    var ref = databaseRef.child("iReports").child(this.tabId);
    ref.once("value", snap => {
        ref.child("TCA/cumulativeCategoryData").set(this.calCatUsage(cumulativeCategoryData, snap));
        ref.child("TCA/cumulativeClassData").set(this.calClsUsage(cumulativeClassData, snap));
        ref.child("TCA/cumulativeMonthlyUsageData").set(this.calMonUsage(cumulativeMonthlyUsageData, snap));
    });
    return "Hi Returns :p";
}

DataFormatter.prototype.calCatUsage = function(obj, snap) {
    snap.forEach(function(childSnap) {
        if (childSnap.key === "TCA") return obj;
        var report = childSnap.val();
        var total_access_time = getTimeInSeconds(report.total_access_time);

        if (obj.hasOwnProperty(report.category_accessed)) {
            obj[report.category_accessed] += total_access_time;
        } else {
            obj[report.category_accessed] = total_access_time;
        }
    });
    return obj;
}

DataFormatter.prototype.calClsUsage = function(obj, snap) {
    snap.forEach(function(childSnap) {
        if (childSnap.key === "TCA") return obj;
        var report = childSnap.val();
        var total_access_time = getTimeInSeconds(report.total_access_time);
        var clsLen = report.className.trim().length;

        console.log("length = " + clsLen)

        var classNumber = "";
        if (clsLen >= 2)
            classNumber = (report.className.split(" "))[1];
        else
            classNumber = report.className;

        console.log(report.className);
        console.log(classNumber);

        if (classNumber === "") classNumber = "0";

        if (obj.hasOwnProperty(classNumber)) {
            obj[classNumber] += total_access_time;
        } else {
            obj[classNumber] = total_access_time;
        }
    });
    return obj;
}

DataFormatter.prototype.calMonUsage = function(obj, snap) {
    snap.forEach(function(childSnap) {
        if (childSnap.key === "TCA") return obj;
        var report = childSnap.val();
        var total_access_time = getTimeInSeconds(report.total_access_time);
        var dateOfAccess = report.date_of_access;
        var month_year = getMonth_Year(dateOfAccess);

        console.log(report.date_of_access);
        console.log(month_year);
        console.log(total_access_time);

        if (obj.hasOwnProperty(month_year)) {
            obj[month_year] += total_access_time;
        } else {
            obj[month_year] = total_access_time;
        }
    });
    return cumulativeMonthlyUsageData;
}

getTimeInSeconds = function(total_access_time) {
    var time = total_access_time.split(":");
    console.log(((+time[0]) * 3600) + ((+time[1]) * 60) + (+time[2]));
    return (((+time[0]) * 3600) + ((+time[1]) * 60) + (+time[2]));
}

getMonth_Year = function(dateOfAccess) {
    var date = dateOfAccess.split("/");
    return (date[0] + "_" + date[2]);
}

DataFormatter.prototype.uploadJson = function(json) {
    databaseRef.child("MyJson").set(json);
    databaseRef.once("child_added", snapshot => {
        console.log(snapshot.key);
        snapshot.forEach(function(childSnap) {
            console.log(childSnap);
        })
    });
}

DataFormatter.prototype.getDataFromFirebase = function() {
    var dbReportsRef = databaseRef.child("iReports/SCA");
    dbReportsRef.child("reports")
        .orderByKey()
        .startAt("2017-09-05")
        .endAt("2017-09-12")
        .once("value", snap => {
            console.log(snap.key);
            console.log(snap.val());
        });
}