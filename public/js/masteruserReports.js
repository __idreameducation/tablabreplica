var App = function App() {
    this.assessmentUsageTableBody = $('#assessmentsUsageTableBody');
    this.assessmentsTableBottomInfo = $('#assessmentTableBottomInfo');
    this.startDate;
    this.endDate;
    this.did = $('#did').text();
    this.currentGrade = "";
}

App.prototype.functions = new Functions();

App.prototype.init = function (startDate, endDate) {
    this.masterReportsDetails(`/reports/usermasterReportsDetailsPage?did=${this.did}&start=${startDate}&end=${endDate}`);
}

// ---------------------------------------------------------------------------------------- //
App.prototype.masterReportsDetails = function (url) {
	
    const instance = this;
    this.functions.sendRequest(url, "GET", null, function (error, data) {

        // hide spinner/loader
        instance.toggleLoader('loader1', false);

        instance.assessmentsTableBottomInfo.html("Showing 0 out of 0");

        $('#tableHead').find('input').val('');
        $('#tableHead').find('select').val('');

        if (error || !data) {
            instance.assessmentUsageTableBody.html("");
        } else {
			
            instance.showAssessmentsDataInTable(data);
        }
    });
}

App.prototype.showAssessmentsDataInTable = function (data) {
    // Using only same table elements for all the grades in the html,
    // so need to clean old rows from the table
    $('#assessmentsUsageTableBody tr').css("display", "none");

    var length = 0;
    const ASSESSMENT_TYPE_PRACTICE = 'Practice';
    const ASSESSMENT_TYPE_LOCKED_TERM = 'LockedTerm';
    const ASSESSMENT_TYPE_UNLOCKED_TERM = 'UnlockedTerm';
   var classes =[];
   var subjects=[];
   
    // create row with classwise values from data object
    const dateObject = data.cumulativeUsage;
    for (usageKey in dateObject) {
		
		const usage = dateObject[usageKey];
		
        var row = $('<tr></tr>').attr("style", "color:black;").attr('id', '');

        var tdSerialNumber = $('<td></td>').attr("align", "center");
        var tdStudentName = $('<td></td>').attr("align", "center");
        var tdStudentClass = $('<td></td>').attr("align", "center");
		var tdDOA = $('<td></td>').attr("align", "center");
		var tdTOA = $('<td></td>').attr("align", "center");
        var tdSubject = $('<td></td>').attr("align", "center");
        var tdAssessmentType = $('<td></td>').attr("align", "center");
        var tdDOU = $('<td></td>').attr("align", "center");
        var tdScore = $('<td></td>').attr("align", "center");
        var tdCompletionStatus = $('<td></td>').attr("align", "center");

        tdSerialNumber.text(length + 1);
        tdAssessmentType.text(usage.contentCategory);
        tdCompletionStatus.text("N/A");
		tdDOA.text(usage.dateOfAccess);
		tdTOA.text(usage.timeOfAccess);
        tdDOU.text(this.getTimeFromSeconds(+(usage.durationOfUse)));
        tdScore.text(usage.resourceName);
        tdStudentName.text(usage.userName);
        tdStudentClass.text(usage.className);
        tdSubject.text(usage.subjectOfContent);
         classes.push(usage.className);
		 subjects.push(usage.subjectOfContent);
        row.append(tdSerialNumber);
        row.append(tdStudentName);
        row.append(tdStudentClass);
		row.append(tdDOA);
		row.append(tdTOA);
        row.append(tdSubject);
        row.append(tdAssessmentType);
        row.append(tdDOU);
        row.append(tdScore);
        row.append(tdCompletionStatus);

        this.assessmentUsageTableBody.append(row);
        length++;
    }
	
	var classfilterArray = classes.filter(function(item, i, ar){ return ar.indexOf(item) === i; }).sort();
	var sujectfilterArray = subjects.filter(function(item, i, ar){ return ar.indexOf(item) === i; }).sort();
    
	 addOption("subjectFilter",sujectfilterArray);
	 addOption("classFilter",classfilterArray);
    this.assessmentsTableBottomInfo.html("Showing " + (+length) + " out of " + (+length));
}

function addOption(id,array){
	array.forEach(function(element) {
   $(`#${id}`).append( "<option>" + element+"</option>")
});
	
}
// ----------------------------------------------------------------------------------------- //

App.prototype.getTimeFromSeconds = function (seconds) {
    seconds = Number(seconds);
    const ss = Math.floor(seconds % 60);
    const mm = Math.floor((seconds / 60) % 60);
    const hh = Math.floor(seconds / (60 * 60));
    return (("00" + hh.toString()).slice(-2) + ':' + ("00" + mm.toString()).slice(-2) + ':' + ("00" + ss.toString()).slice(-2));
}

App.prototype.getFormattedDate = function (date) {
    return ("0000" + date.getFullYear()).slice(-4) + "-" + ("00" + (date.getMonth() + 1)).slice(-2) + "-" + ("00" + date.getDate()).slice(-2);
}

App.prototype.getLastWeekDate = function (currentDate) {
    const last = 6;
    return new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - last)
}

App.prototype.getLastXMonthsDate = function (currentDate, last) {
    return new Date(currentDate.getFullYear(), currentDate.getMonth() - last, currentDate.getDate())
}

App.prototype.toggleRedMessage = function (id, message) {
    if (message)
        $(`#${id}`).css('display', 'block').text(message);
    else
        $(`#${id}`).css('display', 'none');
}

App.prototype.toggleLoader = function (id, show) {
    if (show)
        $(`#${id}`).css('display', 'block');
    else
        $(`#${id}`).css('display', 'none');
}

$(function () {
   
	const LAST_1_MONTH = 1;
	 const LAST_1_WEEK = 2;
	 const LAST_6_MONTHS = 3;
     const FROM_BEGINNING = 4;
     const CUSTOM_DATE = 5;
     

    const app = new App();

    // setting default value for "#from" and "#to" input fields
    // default value for time interval is, last one year from today
    const today = new Date();
    const endDate = app.getFormattedDate(today);
    const date = app.getLastXMonthsDate(today, 1);
    const startDate = app.getFormattedDate(date);
    $('#from').val(startDate);
    $('#to').val(endDate);

    // request for grade wise usage data for the first time when page loads
    app.init(startDate, endDate);

    $('#download')
        .css('display', 'block')
        .on('click', (e) => {
            location.href = `/reports/downloadUserMasterReports?did=${app.did}&start=${$('#from').val()}&end=${$('#to').val()}`;
        });

    // on change of time interval
    $('#intervalOptions').on('change', (e) => {
        var date, startDate;

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        switch (+($(e.currentTarget).find(':selected').attr('id'))) {
            case FROM_BEGINNING:
                date = app.getLastXMonthsDate(today, 12);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_6_MONTHS:
                date = app.getLastXMonthsDate(today, 6);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_1_MONTH:
                date = app.getLastXMonthsDate(today, 1);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case LAST_1_WEEK:
                date = app.getLastWeekDate(today);
                startDate = app.getFormattedDate(date);
                $('#from').prop('readonly', true).val(startDate);
                $('#to').prop('readonly', true).val(endDate);
                //          $('#go').prop('disabled', true);
                break;
            case CUSTOM_DATE:
                $('#from').prop('readonly', false).val("");
                $('#to').prop('readonly', false).val("");
                //          $('#go').prop('disabled', false);
                break;
            default:
                break;
        }
    });

    $('#go').on('click', (e) => {
        const startDate = $('#from').val();
        const endDate = $('#to').val();

        // hide red message, if any
        app.toggleRedMessage('alertMessage');

        if (!startDate || !endDate) {
            app.toggleRedMessage('alertMessage', 'Dates can not be empty');
            return;
        } else if (startDate > endDate) {
            app.toggleRedMessage('alertMessage', 'Start date can not be greater than end date');
            return;
        }

        // Grade wise assessments usage data request
		app.toggleLoader('loader1', true);
		console.log()
        app.masterReportsDetails(`/reports/usermasterReportsDetailsPage?did=${app.did}&start=${startDate}&end=${endDate}`);
    });

    // Filter on the name field
    $("#nameFilter").on('keyup', (e) => {
        var classFilterValue = $($("#classFilter").find('option:selected')[0]).val().toLowerCase();
        var subjectFilterValue = $($("#subjectFilter").find('option:selected')[0]).val().toLowerCase();
        //var assessmentTypeFilterValue = $($("#assessmentTypeFilter").find('option:selected')[0]).val().toLowerCase();
        //var completionStatusFilterValue = $($("#completionStatusFilter").find('option:selected')[0]).val().toLowerCase();
        var nameFilterValue = $(e.currentTarget).val().toLowerCase();
        var tr = $("#assessmentsUsageTableBody tr");
        var count = 0;

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            var tdNameFilter = $(tr[i]).find("td")[1];
            var tdClassFilter = $(tr[i]).find("td")[2];
            var tdSubjectFilter = $(tr[i]).find("td")[4];
            var tdAssessmentTypeFilter = $(tr[i]).find("td")[5];
            var tdCompletionStatusFilter = $(tr[i]).find("td")[8];

            // if ($(tdLanguageFilter)) {
            if ($(tdClassFilter).html().toLowerCase().indexOf(classFilterValue) > -1 &&
                $(tdNameFilter).html().toLowerCase().indexOf(nameFilterValue) > -1 &&
                (!subjectFilterValue || $(tdSubjectFilter).html().toLowerCase() === subjectFilterValue) ) {

                $(tr[i]).css("display", "");
                $($(tr[i]).find('td')[0]).html(count + 1);
                count++;
            } else {
                $(tr[i]).css("display", "none");
            }
            // }
        }
        $('#assessmentTableBottomInfo').html(`Showing ${count} out of ${tr.length}`);
    });

    // Filter on the student class
    $("#classFilter").on("change", function () {
        var classFilterValue = $($(this).find('option:selected')[0]).val().toLowerCase();
        var subjectFilterValue = $($("#subjectFilter").find('option:selected')[0]).val().toLowerCase();
       // var assessmentTypeFilterValue = $($("#assessmentTypeFilter").find('option:selected')[0]).val().toLowerCase();
       // var completionStatusFilterValue = $($("#completionStatusFilter").find('option:selected')[0]).val().toLowerCase();
        var nameFilterValue = $('#nameFilter').val().toLowerCase();

        var tr = $("#assessmentsUsageTableBody tr");
        var count = 0;

        // Loop through all table rows, and hide those who don't match the search query
        for (var i = 0; i < tr.length; i++) {
            var tdNameFilter = $(tr[i]).find("td")[1];
            var tdClassFilter = $(tr[i]).find("td")[2];
            var tdSubjectFilter = $(tr[i]).find("td")[4];
            var tdAssessmentTypeFilter = $(tr[i]).find("td")[5];
            var tdCompletionStatusFilter = $(tr[i]).find("td")[8];

            if ($(tdClassFilter)) {
                if ($(tdClassFilter).html().toLowerCase().indexOf(classFilterValue) > -1 &&
                    $(tdNameFilter).html().toLowerCase().indexOf(nameFilterValue) > -1 &&
                    (!subjectFilterValue || $(tdSubjectFilter).html().toLowerCase() === subjectFilterValue)  ) {

                    $(tr[i]).css("display", "");
                    $($(tr[i]).find('td')[0]).html(count + 1);
                    count++;
                } else {
                    $(tr[i]).css("display", "none");
                }
            }
        }
        $('#assessmentTableBottomInfo').html(`Showing ${count} out of ${tr.length}`);
    });

    // Filter on the subjects
    $("#subjectFilter").on("change", function () {
        var subjectFilterValue = $($(this).find('option:selected')[0]).val().toLowerCase();
		console.log(subjectFilterValue)
        var classFilterValue = $($('#classFilter').find('option:selected')[0]).val().toLowerCase();
        //var assessmentTypeFilterValue = $($("#assessmentTypeFilter").find('option:selected')[0]).val().toLowerCase();
       // var completionStatusFilterValue = $($("#completionStatusFilter").find('option:selected')[0]).val().toLowerCase();
        var nameFilterValue = $('#nameFilter').val().toLowerCase();

        var tr = $("#assessmentsUsageTableBody tr");
        var count = 0;

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            var tdNameFilter = $(tr[i]).find("td")[1];
            var tdClassFilter = $(tr[i]).find("td")[2];
            var tdSubjectFilter = $(tr[i]).find("td")[5];
           // var tdAssessmentTypeFilter = $(tr[i]).find("td")[5];
           // var tdCompletionStatusFilter = $(tr[i]).find("td")[8];
             console.log($(tdSubjectFilter).html());
            if ($(tdSubjectFilter)) {
                if ($(tdClassFilter).html().toLowerCase().indexOf(classFilterValue) > -1 &&
                    $(tdNameFilter).html().toLowerCase().indexOf(nameFilterValue) > -1 &&
                    (!subjectFilterValue || $(tdSubjectFilter).html().toLowerCase() === subjectFilterValue)  ) {

                    $(tr[i]).css("display", "");
                    $($(tr[i]).find('td')[0]).html(count + 1);
                    count++;
                } else {
                    $(tr[i]).css("display", "none");
                }
            }
        }
        $('#assessmentTableBottomInfo').html(`Showing ${count} out of ${tr.length}`);
    });

    // Filter on the Assessment Type
    $("#assessmentTypeFilter").on("change", function () {
        var assessmentTypeFilterValue = $($(this).find('option:selected')[0]).val().toLowerCase();
        var classFilterValue = $($('#classFilter').find('option:selected')[0]).val().toLowerCase();
        var subjectFilterValue = $($("#subjectFilter").find('option:selected')[0]).val().toLowerCase();
        var completionStatusFilterValue = $($("#completionStatusFilter").find('option:selected')[0]).val().toLowerCase();
        var nameFilterValue = $('#nameFilter').val().toLowerCase();

        var tr = $("#assessmentsUsageTableBody tr");
        var count = 0;

        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            var tdNameFilter = $(tr[i]).find("td")[1];
            var tdClassFilter = $(tr[i]).find("td")[2];
            var tdSubjectFilter = $(tr[i]).find("td")[4];
            var tdAssessmentTypeFilter = $(tr[i]).find("td")[5];
            var tdCompletionStatusFilter = $(tr[i]).find("td")[8];

            if ($(tdAssessmentTypeFilter)) {
                if ($(tdClassFilter).html().toLowerCase().indexOf(classFilterValue) > -1 &&
                    $(tdNameFilter).html().toLowerCase().indexOf(nameFilterValue) > -1 &&
                    (!subjectFilterValue || $(tdSubjectFilter).html().toLowerCase() === subjectFilterValue) &&
                    (!assessmentTypeFilterValue || $(tdAssessmentTypeFilter).html().toLowerCase() === assessmentTypeFilterValue) &&
                    (!completionStatusFilterValue || $(tdCompletionStatusFilter).html().toLowerCase() === completionStatusFilterValue)) {

                    $(tr[i]).css("display", "");
                    $($(tr[i]).find('td')[0]).html(count + 1);
                    count++;
                } else {
                    $(tr[i]).css("display", "none");
                }
            }
        }
        $('#assessmentTableBottomInfo').html(`Showing ${count} out of ${tr.length}`);
    });

    // Filter on the Completion Status
    $("#completionStatusFilter").on("change", function () {
        var completionStatusFilterValue = $($(this).find('option:selected')[0]).val().toLowerCase();
        var classFilterValue = $($('#classFilter').find('option:selected')[0]).val().toLowerCase();
        var subjectFilterValue = $($("#subjectFilter").find('option:selected')[0]).val().toLowerCase();
        var assessmentTypeFilterValue = $($("#assessmentTypeFilter").find('option:selected')[0]).val().toLowerCase();
        var nameFilterValue = $('#nameFilter').val().toLowerCase();

        var tr = $("#assessmentsUsageTableBody tr");
        var count = 0;
        // Loop through all table rows, and hide those who don't match the search query
        for (i = 0; i < tr.length; i++) {
            var tdNameFilter = $(tr[i]).find("td")[1];
            var tdClassFilter = $(tr[i]).find("td")[2];
            var tdSubjectFilter = $(tr[i]).find("td")[4];
            var tdAssessmentTypeFilter = $(tr[i]).find("td")[5];
            var tdCompletionStatusFilter = $(tr[i]).find("td")[8];

            if ($(tdCompletionStatusFilter)) {
                if ($(tdClassFilter).html().toLowerCase().indexOf(classFilterValue) > -1 &&
                    $(tdNameFilter).html().toLowerCase().indexOf(nameFilterValue) > -1 &&
                    (!subjectFilterValue || $(tdSubjectFilter).html().toLowerCase() === subjectFilterValue) &&
                    (!assessmentTypeFilterValue || $(tdAssessmentTypeFilter).html().toLowerCase() === assessmentTypeFilterValue) &&
                    (!completionStatusFilterValue || $(tdCompletionStatusFilter).html().toLowerCase() === completionStatusFilterValue)) {

                    $(tr[i]).css("display", "");
                    $($(tr[i]).find('td')[0]).html(count + 1);
                    count++;
                } else {
                    $(tr[i]).css("display", "none");
                }
            }
        }
        $('#assessmentTableBottomInfo').html(`Showing ${count} out of ${tr.length}`);
    });
});