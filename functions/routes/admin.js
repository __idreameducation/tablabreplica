var express = require("express");
var controllerAdmin = require('../controller/admin');
var router = express.Router();

router.get('/', controllerAdmin.home);

router.post("/createUser", controllerAdmin.createUser);

router.get('/projectCreationPage', controllerAdmin.serveProjectCreationPage);
router.get('/addSchoolDetailsPage', controllerAdmin.serveAddSchoolDetailsPage);
router.get('/editProjectDetailsPage', controllerAdmin.editProjectDetailsPage);

router.get('/getUsers', controllerAdmin.getUsers);
router.get('/getUserDetails', controllerAdmin.getUserDetails);
router.get('/getAssignedCoordinators', controllerAdmin.getAssignedCoordinators);
router.get('/getAssignedProjects', controllerAdmin.getAssignedProjects);
router.get('/getSchoolsUnderProject', controllerAdmin.getSchoolsUnderProject);
router.get('/getProjectDetails', controllerAdmin.getProjectDetails);
router.get('/getSchoolDetails', controllerAdmin.getSchoolDetails);
router.get('/getClassesDetails', controllerAdmin.getClassesDetails);
router.get('/getSchoolAppIDs', controllerAdmin.getSchoolAppIDs);

router.post('/mailForProjectAssignment', controllerAdmin.mailForProjectAssignment);
router.post('/mailRequest', controllerAdmin.mailRequest);

router.post('/updateSchoolMedia', controllerAdmin.updateSchoolMedia);

// Request from - addTeachers.ejs
router.post("/deleteUser", controllerAdmin.deleteUser)

// router.post("/updateUserDetails", controllerAdmin.updateUserDetail);

module.exports = router;