/** Code Documentation
 * A place to define data structures and methods to interact with your database.
 * Controls the database related admin work, like creating users
 */
const express = require('express');
var app = express();

var admin;;
var databaseRef;

const AdminAuth = function AdminAuth() {};

AdminAuth.prototype.init = function(firebaseAdmin) {
    admin = firebaseAdmin;
    databaseRef = admin.database().ref(); //.child("newDatabase");
}

/**
 * 	purpose: create user and send confirmation callback to the admin for updating database.
 * this function take input from controller , this inputs are username, email , mobile number.
 *	output:-> callback to the controller admin.js for adding new user in database.
 *	
 *	Input :- user information, send by admin.
 *	output :- return callback of successfully created user.
 *	
 *	Dependencies:- admin.auth().createUser(),
 *				   admin.database().update()
 */

AdminAuth.prototype.createUser = function(user, done) {
    console.log('Create User - Model');

    var pass = createPassword();
    admin.auth().createUser({
        email: user.userEmail,
        emailVerified: false,
        password: pass,
        displayName: user.firstName + " " + user.lastName
    }).then(function(userRecord) {
        var clientRef = databaseRef.child("users/" + userRecord.uid);
        var updatedUserRecord = {
            name: userRecord.displayName,
            email: userRecord.email,
            designation: user.designation,
            did: (user.designationId) ? user.designationId : '',
            type: "client",
            date: Date.now(),
            phoneNumber: (user.mobileNum) ? user.mobileNum : '',
            active: false
        };

        clientRef.update(updatedUserRecord).then(status => {
            done(null, userRecord, pass)
            console.log("Successfully created new user:", userRecord.uid);
        }).catch((err) => {
            console.log(err)
        });
    }).catch(function(error) {
        // console.log("Error during creating new user:", error);
        done(error)
    });
}

AdminAuth.prototype.deleteUser = function(uid, done) {
    // delete the user
    admin.auth().deleteUser(uid).then(function() {
        console.log("Successfully deleted user");
        new AdminAuth().setter(`users/${uid}`, {}, (error) => {
            done(error);
        })
    }).catch(function(error) {
        done(error);
    });
}

AdminAuth.prototype.getter = function(path, done) {
    databaseRef.child(path)
        .once('value', (snap) => {
            done(snap);
        })
}

AdminAuth.prototype.setter = function(path, data, done) {
    databaseRef.child(path).set(data).then((error) => {
        done(error);
    });
}

AdminAuth.prototype.getterWithLimit = function(path, limit, done) {
    databaseRef.child(path)
        .limitToFirst(limit)
        .once('value', (snap) => {
            done(snap);
        })
}

AdminAuth.prototype.getterWithDesignation = function(path, designation, done) {
    databaseRef.child(path)
        .orderByChild('designation')
        .equalTo(designation)
        .once('value', (snap) => {
            done(snap);
        });
}

/* Uses current system date as the value of password */
function createPassword() {
    var password = Date.now();
    return password + "";
}

//var adminObj = new AdminAuth();
module.exports = AdminAuth;