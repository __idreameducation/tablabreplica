const dirname = __dirname;
require('app-module-path').addPath(dirname);

const
    functions = require('firebase-functions'),
    firebaseAdmin = require('firebase-admin');

// Tablab  replica Config
  const serviceAccount = require("./PRIVATEFILES/tablabreplica-firebase-adminsdk-2p3dl-01d3d2a53b.json");

const adminConfig = {
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: "https://tablabreplica.firebaseio.com"
} 
//Tablab  Live Config
 /* const serviceAccount = require("./PRIVATEFILES/tablab-786cc-firebase-adminsdk-gf1u5-a339dba726.json");

const adminConfig = {
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: "https://tablab-786cc.firebaseio.com"
}  */
 
// testing project conf
//const serviceAccount = require("./PRIVATEFILES/tablab_testing.json");

/* const adminConfig = {
    credential: firebaseAdmin.credential.cert(serviceAccount),
    databaseURL: "https://tablab-testing.firebaseio.com"
} */


firebaseAdmin.initializeApp(adminConfig);

// Models
var ModelsAdmin = require('./models/admin');
var modelsAdminObj = new ModelsAdmin();
modelsAdminObj.init(firebaseAdmin);

var ModelsClient = require('./models/client');
var modelsClientObj = new ModelsClient();
modelsClientObj.init(functions, firebaseAdmin);

// Routes
const routesClient = require('./routes/client'),
    routesAdmin = require('./routes/admin');

const express = require('express'),
    path = require('path'),
    fs = require('fs'),
    expressSession = require('express-session'),
    bodyParser = require('body-parser');

const app = express();

app.set("port", (process.env.PORT || 8000));

app.set("view engine", "ejs");

app.set("views", path.join(dirname, '/pages'));

app.use(express.static(path.join(dirname, '../public')));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(expressSession({
    secret: "idream@education&&!=₹₹",
    cookie: {
        maxAge: 24 * 60 * 60 * 1000,
    },
    activeDuration: 5 * 60 * 1000,
    saveUninitialized: true,
    resave: true
}));

app.use("/", routesClient);
app.use("/reports/admin", routesAdmin);

app.listen(app.get("port"), () => {
    console.log("Server Running on port " + app.get("port"));
});

/*
 * Published on 31/08/2018
 * 
 * Cloud function to replace the "Term" with "UnlockedTerm"
 * Assessments usage reports have "Term" as asssessment type
 * This is the issue only with some old projects
 */

// const databaseRef = firebaseAdmin.database().ref();
// exports.correctAssessmentType = functions.database.ref('Tablab/analytics/{schoolID}/AssessmentUsageReports/appUsers/{dateOfUpload}/{reportID}')
//     .onWrite((change, context) => {
//         var uploadedData = change.after.val();
//         if(uploadedData.assessmentType === "Term") {
//             return databaseRef.child(`Tablab/analytics/${context.params.schoolID}/AssessmentUsageReports/appUsers/${context.params.dateOfUpload}/${context.params.reportID}/assessmentType`).set("UnlockedTerm");
//         }
//         return null;
//     });